;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (layout minimizing pict)
  #:use-module (pict)
  #:use-module (oop goops)
  #:use-module (layout minimizing layout)
  #:export (draw-arrow draw-textbox top bot))


(define (rr x) (slot-ref x 'xr))

(define (draw-rect obj)
  (let* ((l (rr (left  obj)))
         (r (rr (right obj)))
         (u (rr (up    obj)))
         (d (rr (down  obj)))
         (w (- r l))
         (h (- u d)))
  `(rect (@ (style ,((@@ (pict) style-list->string)
                     `(("fill"          "none")
                       ("stroke"        "black")
                       ("stroke-width"  "1"))))
            (x ,l)
            (y ,d)
            (width ,w)
            (height ,h)
            (rx 0)
            (ry 0)))))

(define (draw-ellipse obj)
  (let* ((l (rr (left  obj)))
         (r (rr (right obj)))
         (u (rr (up    obj)))
         (d (rr (down  obj)))
         (w (- r l))
         (h (- u d))
         (cx (+ l (/ w 2)))
         (cy (+ d (/ h 2))))

    `(ellipse (@ (style ,((@@ (pict) style-list->string)
                          `(("fill" "none")
                            ("stroke" "black")
                            ("stroke-width" "1"))))
                 (cx ,cx)
                 (cy ,cy)
                 (rx ,(/ w 2))
                 (ry ,(/ h 2))))))

(define (draw-ret obj ll)
  (catch #t
    (lambda ()
      (let* ((l (rr (left  obj)))
             (r (rr (right obj)))
             (u (rr (up    obj)))
             (d (rr (down  obj)))
             (w (- r l))
             (h (- u d)))

        ((@@ (pict) make-pict)
         `(svg (@ (width  ,w)
                  (height ,h)
                  (x      ,l)
                  (y      ,d))
               ,@ll))))
    (lambda x
      ((@@ (pict) make-pict)
       `(svg (@ (width  200)
                (height 200)
                (x      0)
                (y      0))
             ,@ll)))))


(set! *draw-ret*      draw-ret)     
(set! *draw-rect*     draw-rect)
(set! *draw-ellipse*  draw-ellipse)

(define (draw-text id text)
  `(text (textPath (@ (href ,id)) ,text)))

(define (draw-line id x1 y1 x2 y2)
  `(line (@ (id ,id) (x1 ,x1) (y1 ,y1) (x2 ,x2) (y2 ,y2)
            (style "stroke:rgb(255,0,0);stroke-width:2"))))

(define (draw-c-text p text)
  `(text (@ (x "50%")
            (y ,(format #f "~a%" p))
            (dominant-baseline "middle")
            (text-anchor "middle"))
         ,text))

(define (draw-arrow x1 y1 x2 y2 text)
  (if (< x2 x1)
      (let ((x3 x2) (y3 y2))
        (set! x2 x1)
        (set! y2 y1)
        (set! x1 x3)
        (set! y1 y3)))
  
  (let ((y1 y1)
        (y2 y2)
        (id (gensym "id")))
    `(svg (@ (x ,x1) (y ,(- y1 5)) (width ,(- x2 x1)) (height 10))
          ,(draw-line id 0 5 (- x2 x1) 5)
          ,(draw-c-text 10 text))))

(define top 0)
(define bot 0)

(define (draw-textbox obj text)
  (let* ((l (rr (left  obj)))
         (r (rr (right obj)))
         (u (rr (up    obj)))
         (d (rr (down  obj)))
         (w (- r l))
         (h (- u d)))
    `(svg (@ (x ,l) (y ,u) (width ,w) (height ,h))
          (rect (@ (style ,((@@ (pict) style-list->string)
                     `(("fill"          "none")
                       ("stroke"        "black")
                       ("stroke-width"  "1"))))
            (x 0)
            (y 0)
            (width ,w)
            (height ,h)
            (rx 0)
            (ry 0)))

          ,(draw-c-text 50 text))))
      
