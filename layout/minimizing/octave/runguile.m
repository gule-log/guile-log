function x = runguile()  
  while 1
    fin  = fopen('to-octave','r');
    fout = fopen('to-guile' ,'w');

    n  = fscanf(fin,'%d',1);
    m1 = fscanf(fin,'%d',1);
    m2 = fscanf(fin,'%d',1);

    X0 = zeros(n,1);
    for i=1:n
      X0(i,1) = fscanf(fin,'%f',1);
    end

    H = zeros(n,n);
    for i=1:n
      for j=1:n
        H(i,j) = fscanf(fin,'%f',1);
      end
    end
      
    v = zeros(n,1);
    for i=1:n
      v(i,1) = fscanf(fin,'%f',1);
    end

    A1 = zeros(m1,n);
    for i=1:m1
      for j=1:n
        A1(i,j) = fscanf(fin,'%f',1);
      end
    end

    c1 = zeros(m1,1);
    for i=1:m1
      c1(i,1) = fscanf(fin,'%f',1);
    end
    
    A2 = zeros(m2,n);
    for i=1:m2
      for j=1:n
        A2(i,j) = fscanf(fin,'%f',1);
      end
    end
    
    c2 = zeros(m2,1);
    for i=1:m2
      c2(i,1) = fscanf(fin,'%f',1);
    end

    [U,S,V] = svd(A1);
    s = S *ones(size(S)(2),1);
    I = abs(s) > 0.001;
    c11 = U'*c1;
    A1 = S*V';
    A1 = A1(I,:);
    c1 = c11(I);

    x = qp(X0,H,v,A1,c1,[],[],[],A2,c2);

    for i=1:n
      fprintf(fout,'%f ',x(i));
    end

    fclose(fin);
    fclose(fout);
  end
endfunction
