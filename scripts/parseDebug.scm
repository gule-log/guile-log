(use-modules (ice-9 eval-string))
(use-modules (ice-9 pretty-print))
(use-modules (logic guile-log))
(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log prolog myreader))
(use-modules (logic guile-log guile-prolog ops))
(<define> (library) <cc>)
(define s
  (open-file
   "/home/stis/src/guile-log/language/prolog/modules/library/clpfd.pl" "r"))


  

(define (run)
  (define mute #f)
  (define i     0)
  (define (run2)
    (define (one cont)
      (let ((x (read-one-term s #:mute mute)))
	(set! i (+ i 1))

	(if mute
	    (set! x "1")
	    (pretty-print (eval-string (string-append "'" x))))

	(pk i)
      
	(if (equal? x "")
	    (pk 'finished)
	    (cont))))

    (set! mute #f)
    (format #t "?> ")
    (let lp ((r ((@ (guile) read))))
      (cond
       ((eq? r 'm)
	(set! mute #t)
	(format #t "?> ")
	(lp ((@ (guile) read))))
       
       ((number? r)
	  (let lp ((r r))
	    (if (= r 0)
		(run2)
		(one (lambda () (lp (- r 1))))))
	
	  (one run2)))))
  (run2))

	

