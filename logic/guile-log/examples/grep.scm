;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define (f1)
  (format #t 
"
(a (b c \"x  
     \")
   (d e)
   (b (f g h)))
(q r)
(b a)
"))

