;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (kanren)
  #:use-module (logic guile-log kanren)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 pretty-print)
  #:use-module (test-suite lib))
