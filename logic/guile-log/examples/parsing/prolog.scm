;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log examples parsing prolog)
  #:use-module (logic guile-log parsing operator-parser)
  #:use-module (logic guile-log guile-log-pre)
  #:use-module (logic guile-log example parsing order)
  #:use-module (logic guile-log example parsing order)
  #:use-module ((logic guile-log) 
		#:renamer (symbol-prefix-proc 'GL:))
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log parser)
  #:export (compile-string *prolog-ops* define-goal-functor goal-eval))

(define pp
  (case-lambda
   ((s x)
    (pretty-print `(,s ,(syntax->datum x)))
    x)
   ((x)
    (pretty-print (syntax->datum x))
    x)))

(define ws-char    (f-or! (s-char #\space) (s-char #\tab) s-nl))
(define ws  (f* ws-char))
(define ws+ (f+ ws-char))

(define first-var      (f-reg! "[A-Z_]"))
(define rest-var       (f-reg! "[a-zA-Z_0-9]"))
(define variable-tok   (mk-token (f-seq first-var (f* rest-var))))
		

(define first-atom (f-reg! "[a-z]"))
(define atom-tok   (mk-token (f-seq first-atom (f* rest-var))))
(define any (f-reg "."))

(define special (f-reg "[].[(),\"'{}|]"))

;; SWI-PROLOG OPERATOR PRECEDENCE PARSER
(define expr* #f)
(define expr (Ds expr*))

(define *prolog-ops* (make-opdata))
(for-each
 (lambda (x)
   (match x
    ((a b c) (add-operator ops a c b ws))))
 `((xfx 1200 -->)
   (xfx 1200 :-)
   (fx  1200 :-)
   (fx  1200 ?-)
   (xfy 1100 ";")
   (xfy 1100 "|")
   (xfy 1050 ->)
   (xfy 1000 ",")
   (fy  900  "\\+")
   ,@(map (lambda (x) `(xfx 700 ,x))
	  '(< = =.. =@= =:= =< == "=\\=" > >= @< @=< @> @>= "\\=" "\\==" is))
   (xfy 400 :)
   ,@(map (lambda (x) `(yfx 500 ,x)) '(+ - "/\\" "\\/" xor))
   ,@(map (lambda (x) `(yfx 400 ,x)) '(* / // rdiv << >> mod rem))
   (xfx 200 **)
   (xfy 200 ^)
   ,@(map (lambda (x) `(fy 200 ,x)) '(- "\\"))))


(define symbolic (mk-token 
		  (f+ (s-not! (f-or! ws-char special rest-var )))))
  

(define quotes 
  (mk-token (f-seq (s-char #\') (f* (f-reg! "[^']")) (s-char #\'))))

(define integer  (f+ (f-reg! "[0-9]")))
(define fraction (f-seq integer  (f-or! (f-seq (s-char! #\.) integer) 
					s-true)))
(define exponent (mk-token
		   (f-seq!! fraction 
			    (f-or!
			     (f-seq
			      (s-char! #\e) 
			      (f-or! (s-char! #\+)
				     (s-char! #\-)
				     s-true)
			      integer)
			     s-true))))

(define (mk-id S c cc) cc)

(define number
  (p-freeze 'number
    (<p-lambda> (c)
      (.. (c) (ws c))
      (GL:<let> ((n N) (m M))
        (.. (c) (exponent c))
        (<p-cc> `(#:number ,(string->number c) ,n ,m))))
    mk-id))

(define variable  
  (p-freeze 'variable
    (<p-lambda> (c)
      (.. (c) (ws c))
      (GL:<let> ((n N) (m M))
        (.. (c) (variable-tok c))
        (<p-cc> `(#:variable ,(string->symbol c) ,n ,m))))
    mk-id))

(define symbolic-tok
  (p-freeze 'symbolic
    (<p-lambda> (c)
      (.. (c) (ws c))
      (GL:<let> ((n N) (m M))
        (.. (c) (symbolic c))
        (<p-cc> `(#:symbolic ,(string->symbol c) ,n ,m))))
    mk-id))

(define atom
  (p-freeze 'atom
    (<p-lambda> (c)
      (.. (c) (ws c))
      (GL:<let> ((n N) (m M))
        (.. (c) (atom-tok c))
        (.. (c) (ws c))
        (<p-cc> `(#:atom ,(string->symbol c) ,n ,m))))
    mk-id))

(define paranthesis
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'parenthesis
      (<p-lambda> (c)
	(.. (c1) (ws c))
	(.. (c2) (l    c1))
	(.. (c3) (expr c2))
	(.. (c4) (r    c3))
	(.. (c5) (ws   c4))
	(<p-cc> (GL:<scm> c3)))
      mk-id)))

(define term-tok
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'term
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(GL:<let> ((n N) (m M))
          (.. (c1) (atom c0 ))
	  (.. (c2) (l    c1))
	  (.. (c3) (expr c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws   c4))
	  (<p-cc> `(#:term ,c1 ,(GL:<scm> c3) ,n ,m))))
      mk-id)))

(define termvar-tok
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'termvar
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(GL:<let> ((n N) (m M))
          (.. (c1) (variable-tok c0))
	  (.. (c2) (l    c1))
	  (.. (c3) (expr c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws c4))
	  (<p-cc> `(#:termvar ,(string->symbol c1) ,(GL:<scm> c3) ,n ,m))))
      mk-id)))

(define list-tok 
  (let ((l (f-tag "["))
	(r (f-tag "]")))
    (p-freeze 'list
      (<p-lambda> (c)
	(.. (c1) (ws c))
	(GL:<let> ((n N) (m M))
	  (.. (c2) (l    c1))
	  (.. (c3) (expr c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws   c4))
	  (<p-cc> `(#:list ,(GL:<scm> c3) ,n ,m))))
      mk-id)))


#;(define tok (f-or! list-e term-tok termvar-tok atom symbolic variable number))
(define tok (f-or! list-tok termvar-tok term-tok paranthesis
		   number atom variable))
(define e (mk-operator-expression tok symbolic-tok ops))
(set! expr* (<p-lambda> (c) (.. (e 1200))))

(define (f-parse-1 stx m)
  (<p-lambda> (c)
    (.. (d) (m GL:_))
    (<p-cc> (cons (parse-1 stx (GL:<scm> d)) c))))

;; For now we do not do anything here but it is possible to implement
;; parser directions here
(define (parse-1 stx x)
  (define (error-num num n m)
    (format #t 
	"in op/3 (~a:~a) first argument Priority is not less then 1200, ~a~%"
	    m n num)
    (error 'parse-error))

  (define (length-error n m)
    (format #t "in op/3 (~a:~a) number of arguments is not 3~%"
	    m n)
    (error 'parse-error))

  (define (malformed-prio x n m)
    (format #t "in op/3 (~a:~a) first argument Priority malformed type ~a~%"
	    m n x)
    (error 'parse-error))

  (define (malformed-spec-atom x n m)
    (format #t "in op/3 (~a:~a) second argument Spec malformed atom ~a~%"
	    m n x)
    (error 'parse-error))

  (define (malformed-spec x n m)
    (format #t "in op/3 (~a:~a) second argument Spec malformd, ~a~%"
	    m n x)
    (error 'parse-error))

  (define (symbol-error x n m)
    (format #t "in op/3 (~a:~a) third op argument invalid atom, ~a~%"
	    m n x)
    (error 'parse-error))

  (define (malformed-op x n m)
    (format #t "in op/3 (~a:~a) third op argument invalid, ~a~%"
	    m n x)
    (error 'parse-error))
  
  (match x
    (((fx _ ':- _ ) _ (#:term (#:atom nm) l _ _) N M)
     (if (bounded-equal? (datum->syntax nm) #'op)
	 (let* ((ll       (get.. "," l))
		(n        (if (= (length ll) 3)
			      3
			      (length-error n m)))

		(priority (match (car ll)
				 ((#:number num x y)
				  (if (< num 1200)
				      num
				      (error-num num n m)))
				 ((x . _)
				  (malformed-prio x n m))))

		(spec     (match (cadr ll)
			    ((#:atom (and nm 
					  (or 'fx 'fy 'xf 'yf 'xfx 'xfy 'yfz)) 
				     . _)
			     nm)
			    ((#:atom nm n m)
			     (malformed-spec-atom nm n m))
			    ((x . _)
			     (malformed-spec x N M))))

		(op       (match (caddr ll)
			    (((or #:symbolic #:atom) nm n m)
			     (if (member (symbol->string nm) '("," "|"))
				 (symbol-error nm n m)
				 nm))
			    ((tp . _)
			     (malformed_op tp N M)))))
	   (op-directive priority spec op))
	 '()
	 x))))


;; Operator parsing
(define (op-directive priority specifier Operator)
  (add-operator *prolog-ops* specifier Operator priority ws))

(define (prolog-tokens stx)
  (let ((f (f* (f-seq ws (f-parse-1 stx expr) (<p-lambda> (c) 
					     (GL:<pp>  `(CC ,CC)) (<p-cc> c))
		      ws (s-char #\.)))))
    (<p-lambda> (c)
      (.. (d) (f '()))
      (GL:<pp> (list S P CC))
      (<p-cc> (pp 'token (reverse d))))))

(define (prolog-parse stx . l) 
  (with-fluids ((*prolog-ops* (fluid-ref *prolog-ops*)))
    (apply parse 
	   (append l (list (prolog-tokens stx))))))

(define (compile stx l)
  (define (less x y)
    (match (pp 'less-x x)
      ((#f #f _)
       #t)      
      ((f xa xb)
       (match (pp 'less-y y)
	 ((#f #f _)
	  #f)

	 ((g ya yb)
	  (let ((fstr (symbol->string f))
		(gstr (symbol->string g)))
	    (if (string< fstr gstr)
		#t
		(if (string= fstr gstr)
		    (< (length xa) (length ya))
		    #f))))))))

  (define (top x)
    (match (pp 'top x)
      (((('xfx _ ':- _) (#:term (#:atom v . _) y . _) z . _))
       (list v (get.. "," y) z))

      (((('fx _ ':- _) z . _))
       (list #f #f z))

      ((#:term (#:atom v . _) y . _)
       (list v (get.. "," y) '()))))

  (let* ((l-r (pp 'l-r (stable-sort (map top (car (pp 'compile l))) less)))
	 (com (let lp ((f #f) (xx #f) (l l-r) (r '()) (rl '()) (res '()))
		(define (next l x y)
		  (if (= (length x) (length xx))
		      (lp f x l (cons (cons x y) r) 
			  rl                    res)
		      (lp f x l (list (cons x y))   
			  (cons (reverse r) rl) res)))
		
		(match l
		 (((v x y) . l)
		  (if f
		      (if (eq? v f)
			  (next l x y)
			  (if (pair? r)
			      (lp v x l (list (cons x y)) '()
				  (cons (cons f
					      (reverse (cons (reverse r) rl)))
					res))
			      (lp v x l (list (cons x y)) '()
				  (cons (cons f (reverse rl)) 
					res))))
		      (lp v x l (list (cons x y)) rl res)))
		 (() 
		  (let* ((rl_ (if (null? r) 
				  (cons f (reverse rl))
				  (cons f (reverse 
					   (cons (reverse r) rl)))))
			 (res_ (if (null? rl_)
				   (reverse res)
				   (reverse (cons rl_ res)))))
		    
		    res_))))))
    (pp 'res #`(begin #,@(map (gen-fkn stx) (pp 'com com))))))


(define-syntax-rule (map2 f x ...)
  (let ((F f))
    (map (lambda (x ...) (map F x ...)) x ...)))


(define (gen-fkn stx)
  (lambda (com)
  (match com
    ((f . (((l . r) ...) ...))
     (let ((v-l   (pp 'v-l   (map2 get-variables l)))
	   (v-r   (pp 'v-r   (map2 (lambda (x) (get-variables (list x))) r)))
	   (v-new (pp 'v-new (map2 (difference stx) r l)))
	   (lhs   (pp 'lhs   (map2 (get-lhs stx) l)))
	   (rhs   (pp 'rhs   (map2 (get-rhs stx) r))))
       (count-variables (map (lambda (x) (cons x 0)) (union v-l v-r)) 
			(cons l r))
			
       (with-syntax (((((lhs ...) ...) ...) lhs)
		     (((rhs  ...) ...)      rhs)
		     ((((v   ...) ...) ...) v-new)
		     (f                     (datum->syntax stx f)))
       #'(define f 
	   (GL:<<case-lambda>> 
	    ((lhs ... (GL:<var> (v ...) rhs)) ...)
	    ...))))))))

(define (union v1 v2)
  (define tab  (make-hash-table))
  (define (fold k v l) (cons k l))
  (for-each (lambda (x) (hashq-set! tab x #t)) v1)
  (for-each (lambda (x) (hashq-set! tab x #t)) v2)
  (hash-fold fold '() tab))
  
(define (difference stx)
  (lambda (x y)
    (define x-tab (make-hash-table))
    (define (fold k v l)
      (if v
	  (cons (datum->syntax stx k) l)
	  l))
    (define (f x p)
      (for-each
       (lambda (x) (hash-set! x-tab x p))
       x))

    (f x #t)
    (f x #f)
    (hash-fold fold '() x-tab)))

(define (count-variables vs l)
  (define tab (make-hash-table))
  (define (coll x) (hashq-set! tab x 0))
  (for-each coll vs)
  (let lp ((l l))
    (match l
      ((#:variable x . _)
       (hashq-set! tab x (+ (hashq-ref tab x 0) 1)))
      ((x . l)
       (lp x) (lp l))
      (_ #t)))
  (let lp ((l l))
    (match l
      ((#:variable x n m)
       (when (= (hashq-ref tab x 0) 1)
	 (warn (format #f "At (~a:~a), Variable ~a only used one time"
		       m n x))))
      ((x . l)
       (lp x) (lp l))
      (_ #t))))

(define (get-variables exp)
  (define vs (make-hash-table))
  (define (add v)
    (hash-set! vs v #t))
 
  (define (loop exp)
    (match (pp 'get-variables exp)
      (((_ . _) x y _ _)
       (loop x)
       (loop y))
      (((_ . _) x _ _)
       (loop x))
      ((#:variable x . _) 
       (add x))
      ((#:termvar v l . _)
       (add v)
       (loop l))
      ((#:term v l . _)
       (loop l))
      ((#:list l . _)
       (loop l))
      (_ #t)))

  (map loop exp)
 
  (hash-fold (lambda (k v l) (cons k l)) '() vs))

(define (get.. op x)
  (match x
    (((_ _ (? (lambda (x) (eq? op x))) _) x y _ _)
     (cons x (get.. op y)))
    (x (list x))))


(define (var stx x)
  (define (fget x) (var stx x))
  (match (pp 'var x)
    ((#:variable v   . _) (datum->syntax stx v))
    ((#:list     v   . _) (map fget (get.. "," v)))
    ((#:term     (#:atom f . _) x . _) 
     (list* #:fkn 
	    (datum->syntax stx f)
	    (map fget x)))
    ((#:termvar  v x . _) (list* #:fkn
				 (datum->syntax stx v)
				 (map fget x)))
    ((#:atom v       . _) #`(quote #,(datum->syntax stx v)))
    ((#:number n     . _) n)
    ((#:symbolic s   . _) #`(quote #,(datum->syntax stx s)))
    (((_ _ op _) x y _ _)
     (list #:op (tr-binop op) (var stx x) (var stx y)))
    (((_ _ op _) x _ _)
     (list #:op (tr-unop op) (var stx x)))))

(define (get-lhs stx)
  (lambda (x)
    (map (lambda (x) (var stx x)) x)))
      
(define (get-rhs stx)
  (lambda (x) (goal stx x)))
    
(define (var-only stx x)
  (match x
    ((#:variable y . _)
     (datum->syntax stx y))))

(define guile-throw throw)
(<define> (throw x)
  (<code> (guile-throw x S)))


(define guile-catch catch)
(<define> (catch goal tag handler)
  (<let> ((fr (<newframe>)))
    (<tail-call>
     (guile-catch 'iso-prolog
      (lambda () 
	(goal S P CC))
      (lambda (key tag-in SS)
	(parse<> (SS P CC)
	  (<if> (<=> tag tag-in)
		(<and>
		 (<code> (<unwind> fr))
		 (handler))
		(throw 'iso-prolog tag-in SS))))))))
	 
(define (e-value stx x)
  (match x
    (((_ _ '+  _) x y . _)
     #'(+ #,(e-value stx x) #,(e-value stx y)))
    (((_ _ '-  _) x y . _)
     #'(- #,(e-value stx x) #,(e-value stx y)))
    (((_ _ '-  _) x   . _)
     #'(- #,(e-value stx x)))
    (((_ _ '*  _) x y . _)
     #'(* #,(e-value stx x) #,(e-value stx y)))
    (((_ _ '/  _) x y . _)
     #'(/ #,(e-value stx x) #,(e-value stx y)))
    (((_ _ '//  _) x y . _)
     #'(/ #,(e-value stx x) #,(e-value stx y)))
    (((_ _ 'rem  _) x y . _)
     #'(remainder #,(e-value stx x) #,(e-value stx y)))
    (((_ _ 'mod  _) x y . _)
     #'(modulo #,(e-value stx x) #,(e-value stx y)))
    ((#:variable X . _)
     #`(GL:<scm> #,(datum->syntax stx X)))
    ((#:number x . _) x)
    ((#:term c x . _)
     (with-syntax (((a ...) (map (lambda (x) (e-value stx x)) 
				 (get.. "," x))))
       #`(c a ...)))))

(define-syntax compile-string
  (lambda (x)
    (syntax-case x ()
      ((n str)
       (compile #'n
	 (prolog-parse (syntax->datum #'str)))))))

