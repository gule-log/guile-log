;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log dynamic-features)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log umatch)
  #:export(add-dynamic-object-features
	   ;; general way of binding a dynamic-object api
	   add-dynamic-object-features
	   
	   ;; more specific way of binding dynamic object api
	   add-fluid-dynamics             ;; e.g. a wraped functional 
	                                  ;; datastructure
	   add-vlist-dynamics
	   add-vhash-dynamics
	   add-dynamic-function-dynamics
	   add-dynamic-barrier
           add-parameter-dynamics

           mk-combine-dynamics 
           ;; this can combine many dynamics into one dynamic
           
	   ;; more specific way of binding dynamic object api
	   make-fluid-dynamics             ;; e.g. a wraped functional 
	                                  ;; datastructure
	   make-vlist-dynamics
	   make-vhash-dynamics
	   make-dynamic-function-dynamics
	   make-dynamic-barrier

	   ;; to enable a dynamic object feature from this point and up the 
	   ;; stack (h #:optional fail)
	   backtrack-dynamic-object
	   not-backtrack-dynamic-object
	   fluid-guard-dynamic-object
	   state-guard-dynamic-object
	   state-guard-dynamic-object-zip
	   always-state-guard-dynamic-object 

	   ;; to execute code with a dynamic object feature
	   ;; (h thunk #:optional fail)
	   with-fluid-guard-dynamic-object
	   with-fluid-guard-dynamic-object-once
	   with-state-guard-dynamic-object
	   with-state-guard-dynamic-object-zip
	   with-always-state-guard-dynamic-object 
	   with-backtrack-dynamic-object
	   with-backtrack-dynamic-object-once
	   with-not-backtrack-dynamic-object

           copy-dynamic-object
           set-dynamic
	   ))

;; This will enable good behavior of e.g. default of
;; * prolog_flags            - pure-dynamic
;; * operator table          - pure-dynamic
;; * char translator table   - pure-dynamic
;; * dynamic functions       - raw

#|
For functional datastructures that is typically wraped inside a fluid for thread
safeness, a special kind of mutataing datastructure can be constructed. For 
these datastructures guile-log can maintain the state eg. the functional data
structure in such a way that restorage of information is done magically. 

There is a simple constructor to use with well behaving datastructures like 
numbers lists etc, but for specially optimized datastructures care need to be 
taken to truncate e,g, reuse a datastructure only if it is safe to truncate 
and also guard a state for a mutating truncation in case a state is stored.
Because these extra functions is not generic a more general association of the
api is needed to the functional object. With this tool one can manage the state
via accessors like ref,set,copy,mk etc. if these accessors are the only one 
used it is then a safe journey.
|#

(define *dynamic-trackers* (make-weak-key-hash-table))
(define (get-dynamic-api h) (hash-ref *dynamic-trackers* h))
(define itag   0)
(define iref   1)
(define iset   2)
(define i++    3)
(define itrunc 4)
(define imk    5)
(define ibt    6)
(define itr    7)

(define (get d i)   (vector-ref d i))
(define (set d i b) (vector-set! d i b))
(define (params->id x) (x params->id))

(define (failadd s p cc h tag)
  (error (format #f "could not add dynamic features, ~a is not a ~a"
		 h tag)))

(define* (add-dynamic-object-features s p cc h tp? tp dref dset d++ dtrunc mk
				      #:key
				      (bt   #f)
				      (rd   #f)
				      (fail failadd))
  (if (tp? h)
      (begin
	(hash-set! *dynamic-trackers* h 
		   (vector tp dref dset d++ dtrunc mk bt rd))
	(cc s p))
      (fail s p cc h tp)))

(define (failadd-0 tag)
  (error (format #f "Could not add fluid dynamics on ~a" tag)))

(define* (make-dynamic-object-features h tp? tp dref dset d++ dtrunc mk
				       #:key
				       (bt   #f)
				       (rd   #f)
				       (fail failadd-0))
  (if (tp? h)
      (hash-set! *dynamic-trackers* h 
		 (vector tp dref dset d++ dtrunc mk bt rd))
      (fail h)))


(define dynamic-fluid? fluid?)
(define dynamic-vhash? (lambda (h) (and (fluid? h) (let ((h (fluid-ref h)))
						     (or
						      (not h)
						      (eq? h vlist-null)
						      (vhash? h))))))

(define* (make-dynamic-function env #:optional (name #f))
  (let* ((g    #f)
	 (name (if name
                   name
                   (gensym "dynfkn")))
         (r (mk-dyn name (lambda (f) (set! g f)))))
    (dynamic-env-set! g env)
    g))

(define (mkf str)
  (lambda h 
    (error (format #t "~a-dynamics association failed on element ~a" str h))))

(define nop (lambda x (if #f #f)))
(define* (mk-combine-dynamics s p cc . l)
  (let ((data (map (lambda (h) 	(hash-ref *dynamic-trackers* h))
                   l)))
    (add-dynamic-object-features s p cc data
               (lambda (x) #t)
               (map (lambda (d) (vector-ref d 0)) data)
               (lambda (l)
                 (map (lambda (d x) ((vector-ref d 1) x))
                      data l))
               (lambda (h v)
                 (map (lambda (d h v)
                        ((vector-ref d 2) h v))
                      data h v))
               (lambda (h)
                 (for-each 
                  (lambda (d h) ((vector-ref d 3) h))
                  data h))
               (lambda (h)
                 (for-each 
                  (lambda (d h) ((vector-ref d 4) h))
                  data h))
               (lambda (vs)
                 (map (lambda (d v)
                        ((vector-ref d 5) v))
                      data vs)))
    l))

(define* (add-fluid-dynamics s p cc h #:optional (fail (mkf 'fluid)))
  (add-dynamic-object-features s p cc h 
			       dynamic-fluid?
			       'fluid-dynamics
			       fluid-ref
			       fluid-set!
			       nop
			       nop
			       make-fluid
			       #:fail fail))

(define* (add-parameter-dynamics s p cc h #:optional  
                                 (fail (mkf 'parameter)))
  (add-dynamic-object-features s p cc h 
			       procedure?
			       'parameter-dynamics
			       (lambda (f) (f))
			       (lambda (f v) (f v))
			       nop
			       nop
			       const 
			       #:fail fail))

(define* (add-vhash-dynamics s p cc h #:optional (fail (mkf 'vhash)))
  (add-dynamic-object-features s p cc h 
			       dynamic-vhash?
			       'vhash
			       fluid-ref
			       fluid-set!
			       vlist-refcount++
			       (lambda (x y) 
				 (if (fluid? x)
				     (vhash-truncate! (fluid-ref x))
				     (vhash-truncate! x)))
			       make-fluid
			       #:fail fail))

(define* (add-vlist-dynamics s p cc h #:optional (fail (mkf 'vlist)))
  (add-dynamic-object-features s p cc h 
			       dynamic-vhash?
			       'vhash
			       fluid-ref
			       fluid-set!
			       vlist-refcount++
			       (lambda (x y) 
				 (if (fluid? x)
				     (vlist-truncate! (fluid-ref x))
				     (vlist-truncate! x)))
			       make-fluid
			       #:fail fail))

(define fdyn (mkf 'dynamic-function))
(define* (add-dynamic-function-dynamics s p cc h #:optional (fail fdyn))
  (add-dynamic-object-features s p cc h 
			       dynamic?
			       'dynamic-function
			       dynamic-env-ref
			       dynamic-env-set!
			       dynamic-refcount++
			       dynamic-truncate!
			       make-dynamic-function
			       #:fail fail))

(define* (make-fluid-dynamics h #:optional (fail (mkf 'fluid)))
  (make-dynamic-object-features h 
			       dynamic-fluid?
			       'fluid-dynamics
			       fluid-ref
			       fluid-set!
			       nop
			       nop
			       make-fluid
			       #:fail fail))

(define* (make-parameter-dynamics h const #:optional 
                                 (fail (mkf 'parameter)))
  (make-dynamic-object-features h 
			       procedure?
			       'parameter-dynamics
			       (lambda (f) (f))
			       (lambda (f v) (f v))
			       nop
			       nop
			       const 
			       #:fail fail))

(define* (make-vhash-dynamics h #:optional (fail (mkf 'vhash)))
  (make-dynamic-object-features h 
			       dynamic-vhash?
			       'vhash
			       fluid-ref
			       fluid-set!
			       vlist-refcount++
			       vhash-truncate!
			       make-fluid
			       #:fail fail))

(define* (make-vlist-dynamics h #:optional (fail (mkf 'vlist)))
  (make-dynamic-object-features h 
			       dynamic-vhash?
			       'vhash
			       fluid-ref
			       fluid-set!
			       vlist-refcount++
			       vlist-truncate!
			       make-fluid
			       #:fail fail))

(define fdyn (mkf 'dynamic-function))
(define* (make-dynamic-function-dynamics h #:optional (fail fdyn))
  (make-dynamic-object-features h 
			       dynamic?
			       'dynamic-function
			       dynamic-env-ref
			       dynamic-env-set!
			       dynamic-refcount++
			       dynamic-truncate!
			       make-dynamic-function
			       #:fail fail))
      
;;This will make sure that we will backtrack in all future
(define (dfail tag)
  (lambda (s p cc h)
    (error (format #f "could not perform ~a dynamic feature for ~a"
		   h tag))))

(define (mk api e h ref tr?)
  (let ((bt (get api (if tr? itr ibt))))
    (if bt
	bt
	(let* ((aset (get api iset))
	       (++   (get api i++))	
	       (tr   (get api itrunc))
	       (bt   (case-lambda
                       (() 
                        (let ((ret (ref h)))
                          (when (and tr? (not (eq? e ret)))
                            (++ ret))
                          ret))
		     
                       ((x)
                        (if (eq? x params->id) 
                            h
                            (let ((xold (ref h)))
                              (aset h x)
                              (when (not (eq? x e)) (tr h xold))
			      (if #f #f)))))))
          (set api (if tr? itr ibt) bt)
          bt))))

(define (backtrack-ref h s)
  (let ((l.a (fluid-ref *unwind-parameters*)))
    (if (pair? l.a)
        (let lp ((l (car l.a)))
          (if (pair? l)
              (if (eq? (params->id (car l)) h)
                  (car l)
                  (lp (cdr l)))
              #f))
        #f)))

(define (backtrack-add f s)
  (let ((l.a (fluid-ref *unwind-parameters*)))
    (gp-fluid-set! *unwind-parameters*
      (if (pair? l.a)
          (cons (cons f (car l.a)) (cdr l.a))
          (cons (cons f '()) '())))))

(define (backtrack-remove h s)
  (gp-fluid-set! *unwind-parameters*
    (let ((l.a (fluid-ref *unwind-parameters*)))
      (if (pair? l.a)
          (let ((l (let lp ((l (car l.a)))
                     (if (pair? l)
                         (if (eq? (params->id (car l)) h)
                             (cdr l)
                             (cons (car l) (lp (cdr l))))))))
            (if (pair? l)
                (cons l (cdr l.a))
                '()))
          '()))))


(define-syntax-rule (dotr) #t)	  
(define-syntax-rule (dobt) #f)	  

(define* (backtrack-dynamic-object s p cc h 
				   #:optional (fail (dfail 'backtrack)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (bt  (mk api e h ref (dobt))))
	  (when (not (backtrack-ref h s))
		(backtrack-add bt s))
	  (cc s p)))))


(define* (not-backtrack-dynamic-object s p cc h 
				       #:optional (fail (dfail 'not-backtrack)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(begin
	  (when (backtrack-ref h s)
		(backtrack-remove h s))
	  (cc s p)))))

#|
if the system backtracks over this barrier the old value will be reinstated
else if the control reenter it will install the value it had when backtracking
before. This works very much like a fluid
|#
(define* (fluid-guard-dynamic-object s p cc h 
				    #:optional (fail (dfail 'fluid-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (bt  (mk api e h ref (dobt))))
	  (gp-fluid-set bt e)
          (cc s p)))))

;; This will make sure that h is dynamic below the boundary
;; e.g. does not get store and restored after this boundary aif a store and
;; a restore is done afterwards unless another guard is issued.
;; else if if the store / restore is between it will be stored and restored
;; guarranteed.
(define* (state-guard-dynamic-object s p cc h
				     #:optional (fail (dfail 'state-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (rd  (mk api e h ref (dotr))))

          (let ((wind (gp-windlevel-ref s)))
            (gp-undo-safe-variable-lguard
             rd
             (gp-rebased-level-ref wind) 
             s)
            (cc s p))))))

(define* (state-guard-dynamic-object-zip 
          s p cc h #:optional (fail (dfail 'state-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (rd  (mk api e h ref (dotr))))

	 (let ((wind (gp-windlevel-ref s)))
           (gp-undo-safe-variable-lguard 
            rd
            (gp-rebased-level-ref (- wind 1))
            s)
           (cc s p))))))

;; This will make sure to always redo to the old value when restoring a state
;; This will override any usages of guard boundary
;; This will be active for all future execution.
(define* (always-state-guard-dynamic-object 
	  s p cc h 
	  #:optional (fail (dfail 'always-state-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (rd  (mk api e h ref (dotr))))
	  (gp-undo-safe-variable-lguard rd #t s)
	  (cc s p)))))


;;
;;
;;    -------------------- W I T H -------------------
;;
;;
(define* (with-fluid-guard-dynamic-object 
	  s p cc h code 
	  #:optional (fail (dfail 'with-fluid-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (++  (get api i++))
	       (bt  (mk api e h ref (dobt))))
	  (gp-fluid-set bt e)
	  (code s p 
	     (lambda (s p)
               (let ((d (ref h)))
                 (when (not (eq? e d))
                   (++ d))
                 (gp-fluid-set bt e)
                 (cc s p))))))))

(define* (with-fluid-guard-dynamic-object-once
	  s p cc h code 
	  #:optional (fail (dfail 'with-fluid-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (bt  (mk api e h ref (dobt))))
	  (gp-fluid-set bt e)
	  (code s p 
	     (lambda (s pp)
	       (gp-fluid-set bt e)
	       (cc s p)))))))

(define* (with-backtrack-dynamic-object 
	  s p cc h code 
	  #:optional (fail (dfail 'with-backtrack)))
  (let* ((api   (get-dynamic-api h))
         (old   (backtrack-ref h s)))
    (if (not api) 
        (fail s p cc h)	
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (bt (mk api e h ref (dobt))))
	  (backtrack-add bt s)
	  (gp-fluid-set bt e)
	  (code s p
		(lambda (s p)
		  (gp-fluid-set bt e)
		  (backtrack-remove h s)
		  (cc s p)))))))


(define* (with-backtrack-dynamic-object-once 
	  s p cc h code 
	  #:optional (fail (dfail 'with-backtrack)))
  (let* ((api   (get-dynamic-api h))
         (old   (backtrack-ref h s)))
    (if (not api) 
        (fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (bt (mk api e h ref (dobt))))
	  (backtrack-add bt s)
	  (gp-fluid-set bt e)
	  (let ((old-bt (bt)))
	    (code s p
		  (lambda (s pp)
		    (backtrack-remove h s)		       
		    (gp-fluid-set bt e)
		    (cc s p))))))))


(define* (with-not-backtrack-dynamic-object 
	  s p cc h code 
	  #:optional (fail (dfail 'with-not-backtrack)))
  (let* ((api   (get-dynamic-api h))
         (old   (backtrack-ref h s)))
    (if (not api) 
        (fail s p cc h)
        (when old
          (backtrack-remove h s)
          (code s p
		(lambda (s p)
		  (backtrack-add old s)
		  (cc s p)))))))

;; The same as above but guard it only inside code
(define* (with-state-guard-dynamic-object
	  s p cc h code
	  #:optional (fail (dfail 'with-state-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (rd  (mk api e h ref (dotr))))
	  (let ((wind (gp-windlevel-ref s)))
            (let ((w  (gp-rebased-level-ref wind)))
              (gp-undo-safe-variable-lguard rd w  s)
              (code s p 
                    (lambda (s p)
                      (gp-undo-safe-variable-rguard rd w s)
                      (cc s p)))))))))

(define* (with-state-guard-dynamic-object-zip
	  s p cc h code
	  #:optional (fail (dfail 'with-state-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (rd  (mk api e h ref (dotr))))
	  (let ((wind (gp-windlevel-ref s)))
            (let ((w  (gp-rebased-level-ref (- wind 1))))
              (gp-undo-safe-variable-lguard rd w  s)
              (code s p 
                    (lambda (s p)
                      (gp-undo-safe-variable-rguard rd w s)
                      (cc s p)))))))))

;; The limited scope to code variant
(define* (with-always-state-guard-dynamic-object 
	  s p cc h code 
	  #:optional (fail (dfail 'with-always-state-guard)))
  (let ((api (get-dynamic-api h)))
    (if (not api) 
	(fail s p cc h)
	(let* ((ref (get api iref))
	       (e   (ref h))
	       (rd  (mk api e h ref (dotr))))
	  (gp-undo-safe-variable-lguard rd #t  s)
	  (code s p 
		(lambda (s p)
		  (gp-undo-safe-variable-rguard rd #t s)
		  (cc s p)))))))

(define* (copy-dynamic-object
	  s p cc x y #:key (name #f) (fail (dfail 'copy)))
  (let ((x (gp-lookup x s))
        (y (gp-lookup y s)))
    (let ((api (get-dynamic-api x)))
      (if (not api) 
          (fail s p cc x)	
          (let* ((++  (get api i++))
                 (mk  (get api imk))
                 (ref (get api iref))
                 (h   (if name 
                          (mk (ref x) #:name name)
                          (mk (ref x)))))
            ((<lambda> () 
	       (<=> y h)
               (<code> (++ (ref x))))
             s p cc))))))

(<define> (failsetxy s p cc x y tx ty)
  (error (format #f "not similar objects (set ~a:~a ~a:~a)"
		 x tx y ty)))
  
(define* (set-dynamic  
	  s p cc x y #:key (fail (dfail 'set)) (failxy failsetxy))
  (let ((x (gp-lookup x s))
        (y (gp-lookup y s)))
    (let ((api-x (get-dynamic-api x)))
      (if (not api-x) 
          (fail s p cc x)	
          (let ((api-y (get-dynamic-api y)))
            (if (not api-y) 
                (fail s p cc y)	
                (let ((tag-x (get api-x itag))
                      (tag-y (get api-y itag)))
                  (if (eq? tag-x tag-y)
                      (let ((set  (get api-x iset))
                            (ref  (get api-y iref))
                            (++   (get api-y i++)))
                        (set x (ref y))
                        (++ (ref y))
                        (cc s p))
                      (failxy s p cc x y tag-x tag-y)))))))))
  
;; Use this to debug vhash behavior
(define (analyze pred x)
  (if (and (vlist? x) pred)
      (let ((a (struct-ref x 0))
            (b (struct-ref x 1)))
        (format #t "<vhash> offset = ~a, " b)
        (let ((block (vector-ref a 0))
              (off   (vector-ref a 2))
              (size  (vector-ref a 3))
              (free  (vector-ref a 4)))
          (format #t " size ~a, free ~a~%" size free)
          (let lp ((i b))
            (if (>= i 0)
                (let* ((next (number->string
				(logand #xffffffff 
					(vector-ref block (+ (* size 3) i)))
				16))
                       (back (ash
                              (vector-ref block (+ (* size 3) i))
                              -32))
                       (hash (vector-ref block (+ (* size 2) back)))
                       (v    (object-address (vector-ref block i)))) 
				      
                  (format #t "~a: next ~a, back ~a hashv ~a key ~a~%" 
                          i next back 
                          hash (number->string v 16))
                  (lp (- i 1))))))

        (format #t "<assoc>~%")))
  x)

      
(<wrap> add-fluid-dynamics *var-attributator*)
