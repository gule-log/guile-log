;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log vm utils)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module ((logic guile-log umatch) #:select (gp-make-var))
  #:export (reset gset gref move movex scmcall make_p make_cc j je jne
                  scmtailcall scmtailcall2 scmcall1 generate
		  label gfalse gtrue isTrue pltest lookup2 svar args
		  test sp cc p s c l vec lvec stack call? scut pp pltest_s
                  base init-proc id vconst
                  tailstub stub inlabel)
  
  #:replace (const))


(define il 0)
(<define> (ilab x)
  (<=> x ,(vector (list l il)))
  (<code> (set! il (+ il 1))))

(define base (gp-make-var))

(define i         0)
(define constants (make-hash-table))
(define init-constants
  (lambda ()
    (set! i 0)
    (set! constants (make-hash-table))))

(<define> (getconst n c l ll)
  (let ((c (<lookup> c)))
    (cond
     ((or (string? c) (number? c) (boolean? c) (symbol? c) (keyword? c))
      (<=> l (,(vector (list "get-constant" n c))
              . ll)))
     (else
      (let ((it (hashq-ref constants c)))
	(if it
	    (ssset n it l ll)
	    (let ((it i))
	      (<code>
	       (hashq-set! constants c i)
	       (set! i (+ i 1)))
	      (ssset n it l ll))))))))

(compile-prolog-string
"
   generate(Tok,[Tok|LL],LL).

   id(s        ,svar(1)).
   id(p        ,svar(0)).
   id(vec      ,svar(2)).   
   id(const    ,svar(3)).
   id(self     ,var(0)).
   id(vconst   ,var(1)).
   id(\"cut\"  ,var(2)).
   id(cc       ,var(3)). 
   id(lvec     ,cl(0)).
   id(ret      ,cl(1)).
   sp(6).

   unpack_vec(Nargs,II,L,LL) :-
     I   is Nargs + 1,
     II  is I     + 1,
     generate('scm-ref/immediate'(I,I,2),L,L2),
     generate('scm-ref/immediate'(II,I,2),L2,L3),
     generate('scm-ref/immediate'(I,I,1),L3,LL).

   pp(complex,I,II,L,LL) :- !,
      II is I + 1,
      gset(sp(I),p,II,L,L1),
      isProcedure(sp(I),Lab,II,L1,L2),
      scmcall1('gp-make-p',[self],I,I1,L2,L3),
      J is I1 - 1,
      move(base+I,base+J,L3,L4),
      label(Lab,L4,LL).

   pp(ground(Lab),I,II,L,LL) :- !,
      II is I + 1,
      gset(sp(I),l(Lab),I,L,L1),
      scmcall1('gp-make-p',[self],II,I1,L1,L2),
      gset(sp(I),sp(I1-1),II,L2,LL).

   pp([ground],I,II,L,LL) :- !,
      II is I + 1,
      gset(sp(I),p,I,L,L1),
      scmcall1('gp-make-p',[self],II,I1,L1,L2),
      gset(sp(I),sp(I1-1),II,L2,L3).

   pp(procedure,I,II,L,LL) :- !,
      II is I + 1,
      gset(sp(I),p,I,L,LL).

   pp(0,I,II,L,LL) :-
     II is I + 1,
     gset(sp(I),cc,I,L,LL).

   cc(C,I,II,L,LL) :-
      scmcall('make-cc',[l(C),self],I,I1,L,L1),
      J is I1 - 1,
      move(base+I,base+J,L1,LL),
      II is I + 1.
     

   e(cc(C),I,II,L,LL) :- cc(C,I,II,L,LL).
   e(pp(C),I,II,L,LL)    :- pp(C,I,II,L,LL).

   tailstub(I,L,LL) :-
   generate(tailcall(I),L,LL).

   stub(N,I,L,LL) :-
   generate(call(N,I),L,LL).

   reset(I,L,LL) :-   
     call(II is I),
     generate('reset-frame'(base+II),L,LL).
   
   varref(Pos,sp(I),II,L,LL) :-
     id(vec,svar(IVEC)),
     PPos is Pos + 1,
     generate('scm-ref/immediate'(base+I,IVEC,PPos),L,LL),
     II is I + 1.

   varref(Pos,svar(I),II,L,LL) :-
     id(vec,svar(IVEC)),
     PPos is Pos + 1,
     generate('scm-ref/immediate'(I,IVEC,PPos),L,LL),
     II is I + 1.

   varref(Pos,I,II,L,LL) :-
     id(vec,svar(IVEC)),
     PPos is Pos + 1,
     generate('scm-ref/immediate'(base+I,IVEC,PPos),L,LL),
     II is I + 1.

   varset(I,J,L,LL) :-     
     id(vec,svar(IVEC)),
     Pos is I + 1,
     generate('scm-set!/immediate'(IVEC,Pos,base+J),L,LL).

   varset_s(I,J,L,LL) :-
     id(vec,svar(IVEC)),
     Pos is I + 1,
     generate('scm-set!/immediate'(IVEC,Pos,J),L,LL).

   move(I,J,L,LL) :-
      generate(mov(I,J),L,LL).

   movex(I,J,L,LL) :-
      call(II is I),
      call(JJ is J),
      generate(mov(base+II,base+JJ),L,LL).

   gref(cl(X),I,L,LL) :- !,
     call(II is I),
     generate(closure(base+II,X),L,LL).
  
   gref(c(X),sp(I),L,LL) :- !,
     call(II is I),
     getconst(base+II,X,L,LL).

   gref(c(X),svar(I),L,LL) :- !,
     call(II is I),
     getconst(II,X,L,LL).

   gref(c(X),I,L,LL) :- !,
     getconst(I,X,L,LL).

   gref(const(X),svar(I),L,LL) :- !,
     call(II is I),
     id(const,svar(S)),       
     generate('scm-ref/immediate'(II,S,X),L,LL).
  
   gref(var(X),sp(I),L,LL) :- 
     varref(X,sp(I),_,L,LL).
   
   gref(var(X),svar(I),L,LL) :- 
     varref(X,svar(I),_,L,LL).
   
   gref(var(X),I,L,LL) :- 
     varref(X,sp(I),_,L,LL).

   gref(sp(X),I,L,LL) :- !,
     call(XX is X),
     call(II is I),
     move(base+II,base + XX,L,LL).

   gref(svar(X),I,L,LL) :- !,
     call(XX is X),
     call(II is I),
     move(base + II,XX,L,LL).

   gref(l(X),I,L,LL) :- !,
     generate('label-ref'(base+I,X),L,LL).

   gref(A,I,L,LL) :-
     id(A,X),!,
     gref(X,I,L,LL).

   gref(A,I,L,LL) :-
     e(A,I,_,L,LL).

   vset(K,sp(J),I,L,LL) :- !,
     call(JJ is J),
     varset(K,JJ,L,LL).

   vset(J,var(X),I,L,LL) :- !,
     gref(var(X),I,L,L2),
     varset(J,I,L2,LL).

   vset(J,svar(X),I,L,LL) :- !,
     varset_s(J,X,L,LL).

   vset(J,c(X),I,L,LL) :- !,
     gref(c(X),I,L,L2),
     varset(J,I,L2,LL).

   vset(J,X,I,L,LL) :-
     id(X,XX),
     vset(J,XX,I,L,LL).

   vset(J,X,I,L,LL) :-
     gref(X,I,L,L1),
     vset(J,sp(I),I,L1,LL).

   sset(K,l(J),I,L,LL) :- !,
      generate('get-label'(base+K,J),L,LL).

   sset(K,sp(J),I,L,LL) :- !,
     call(KK is K),
     call(JJ is J),
     move(base+KK,base+JJ,L,LL).

   sset(K,svar(J),I,L,LL) :- !,
     call(KK is K),
     move(base+KK,J,L,LL).

   sset(J,var(X),I,L,LL) :- !,
     call(JJ is J),
     gref(var(X),sp(JJ),L,LL).
     
   sset(J,c(X),I,L,LL) :- !,
     call(JJ is J),
     gref(c(X),sp(JJ),L,LL).

   ssset(J,X,L,LL) :- !,
     id(const,svar(S)),       
     generate('scm-ref/immediate'(J,S,X),L,LL).

   sset(J,X,I,L,LL) :-
     id(X,XX),
     sset(J,XX,sp(I),L,LL).

   sset(K,l(J),I,L,LL) :- !,
      generate('get-label'(K,J),L,LL).

   cset(K,sp(J),I,L,LL) :- !,
     move(K,base+J,L,LL).

   cset(K,svar(J),I,L,LL) :- !,
     call(KK is K),
     move(KK,J,L,LL).

   cset(J,var(X),I,L,LL) :- !,
     call(JJ is J),
     gref(var(X),svar(JJ),L,LL).

   cset(J,cl(X),I,L,LL) :- !,
     call(JJ is J),
     gref(cl(X),svar(JJ),L,LL).

   cset(J,c(X),I,L,LL) :- !,
     gref(c(X),I,L,L2),
     call(JJ is J),
     move(JJ,base+I,L2,LL).

   cset(J,X,I,L,LL) :-
     id(X,XX),
     cset(J,XX,I,L,LL).

   cset(K,l(J),I,L,LL) :- !,
      generate('get-label'(K,J),L,LL).

   gset(var(V),Val,I,L,LL) :- !,
     vset(V,Val,I,L,LL).

   gset(sp(V),Val,I,L,LL) :- !, 
     sset(V,Val,I,L,LL).

   gset(svar(V),Val,I,L,LL) :- !, 
     cset(V,Val,I,L,LL).

   gset(X,Val,I,L,LL) :- 
     id(X,XX),
     gset(XX,Val,I,L,LL).
   
   'init-proc'(Nargs,Nvec,L,LL) :-
     N  is Nargs + 3 + 1,
     NJ is N + 1,
     id(ret,cl(NRET)),
     id(lvec,cl(NLV)),
     NN is Nvec - N,
     generate(initialJump(NRET,NLV),L,L2),
     generate('assert-narg-ee/locals'(N,NN),L2,L3),
     generate('handle-interrupts',L3,LL).

   
   args([],I,I,L,L).

   args([X|A],I,II,L,LL) :-
      gref(X,I,L,L1),
      I1 is I + 1,
      args(A,I1,II,L1,LL).

   scmcalln(K,F,Args,I,II,L,LL) :-      
      length(Args,N),
      NN is N + 1 + K,
      NNN is I + NN - K,
      reset(NNN,L,L1),
      args(Args,I,I1,L1,L2),          
      gref(c(F),I1,L2,L3),
      I2 is I1 + 1,      
      generate(handle_interupts,L3,L4),
      generate(call(I2,NN),L4,L5),
      II is I2 + 1,
      reset(II,L5,LL).
   

   scmcall(F,Args,I,II,L,LL)  :- scmcalln(0,F,Args,I,II,L,LL).
   scmcall1(F,Args,I,II,L,LL) :- scmcalln(1,F,Args,I,II,L,LL).     
  
   scmtailcalln(K,F,Args,I,II,L,LL) :-      
      length(Args,N),
      NN is N + 1 + K,
      NNN is I + NN - K,
      reset(NNN,L,L1),
      args(Args,I,I1,L1,L2),          
      (
         F=cc->
           gref(F,sp(I1),L2,L3);
         gref(c(F),sp(I1),L2,L3)
      ),
      I2 is I1 + 1,      
      generate(handle_interupts,L3,L4),
      generate(tailcall(I2,NN),L4,LL).

   scmtailcall(F,Args,I,II,L,LL)  :- scmtailcalln(0,F,Args,I,II,L,LL).
   scmtailcall2(F,Args,I,II,L,LL) :- scmtailcalln(2,F,Args,I,II,L,LL).

   j(Label,L,LL) :-
      generate(j(Label),L,LL).

   je(Label,L,LL) :-
      generate(je(Label),L,LL).

   jne(Label,L,LL) :-
      generate(jne(Label),L,LL).

   label(Label,L,LL) :-
      ilab(Label),
      generate(label(Label),L,LL).

   label(Label,Goto,L,LL) :-
      generate(jmp(Goto),L,L1),
      ilab(Label),
      generate(label(Label),L1,LL).

   inlabel(Label,L,LL) :-
      label(Label,G,L,LL).

   inlabel(Label,G,L,LL) :-
      label(Label,G,L,LL).
   
   gfalse(I,II,L,LL) :-
      gref(p,I,L,L2),
      generate('immediate-tag?='(I,7,0),L2,L3),
      generate(je(Lab),L3,L4),
      generate(goto(I),L4,L5),
      label(Lab,L5,L6),
      generate(tailcall(I,0),L6,LL).

   gtrue(I,L,L).
   
   isTrue(sp(J),Label,L,LL) :-
      call(JJ is J),
      generate(test1(true,JJ,Label),L,LL).

   isProcedure(sp(J),Label,I,L,LL) :-
      call(JJ is J),
      generate(test1(isProcedure,JJ,Label),L,LL).
   
   pltest(F,A,I,II,L,LL) :-
     scmcall(F,A,I,II,L,L1),
     isTrue(sp(II-1),Label,L1,L2),
     gfalse(II,_,L2,L3),
     label(Label,L3,LL).
  
   pltest_s(F,A,I,L,LL) :-
     pltest(F,A,I,II,L,L2),
     gset(s,sp(II-1),II,L2,LL).

   lookup(I,L,LL) :-
     generate(lookup(base+I,base+I),L,LL).

   -trace.
   lookup2(I,L,LL) :-
      I1 is I - 2,
      I2 is I - 1,
      lookup(I1,L,L1),
      lookup(I2,L1,LL).

   test(Op,A,B,Label,I,L,LL) :-
      generate(test2(Op,A,B,Label),L,LL).
")
