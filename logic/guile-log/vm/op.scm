;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log vm op)
  #:use-module (logic guile-log vm utils)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module ((logic guile-log iso-prolog)
		#:renamer
		(lambda (s)
		  (if (eq? s 'reset)
		      'pl-reset
		      s)))
  #:replace (plus minus band bor xor modulo mul shift-l shift-r
		  divide		 
		  gt lt le eq neq))


(compile-prolog-string
"

   ggfalse(procedure,L,LL) :-
      generate(tailcall(0),L,LL).
   ggfalse(ground(P),L,LL) :-
      generate(goto(P),L,LL).
   ggfalse(complex,L,LL) :-
      gfalse(L,LL).

   ggfalse(\"\",L,LL) :-
      generate(tailcall(0),L,LL).

   cmp(Op,P,I,II,L,LL) :-
      lookup2(I,L,L1),
      II is I - 2,
      III is I - 1,
      generate(Op(base+II,base+III),L1,L2),
      (
         P=ground(NE) ->
            generate(jne(NE),L2,L5);
         (
           generate(je(Label),L2,L3);
           ggfalse(P,L3,L4),
           label(Label,L4,L5)
         )
      ),
      reset(II,L5,LL).

   gt(P,I,II,L,LL)  :- cmp(gt,P,I,II,L,LL).
   lt(P,I,II,L,LL)  :- cmp(lt,P,I,II,L,LL).
   ge(P,I,II,L,LL)  :- cmp(ge,P,I,II,L,LL).
   le(P,I,II,L,LL)  :- cmp(le,P,I,II,L,LL).
   eq(P,I,II,L,LL)  :- cmp(eq,P,I,II,L,LL).
   neq(P,I,II,L,LL) :- cmp(neq,P,I,II,L,LL).

   -trace.
   trbop(plus     , plus).
   trbop(minus    , minus).
   trbop(band     , band).
   trbop(bor      , bor).
   trbop(xor      , xor).
   trbop(modulo   , modulo).
   trbop(mul      , mul).
   trbop('shift-l', lshift).
   trbop('shift-r', rshift).
   trbop(divide   , divide).
  
   binop(Bop,I,II,L,LL) :-
      trbop(Bop,Nm),
      I1 is I - 2,
      I2 is I - 1,
      generate(Bop(base + I1, base + I1, base + I2),L,L2),
      II is I - 1,
      reset(II,L2,LL).


   plus(I,II,L,LL)      :- binop(plus,I,II,L,LL).
   minus(I,II,L,LL)     :- binop(minus,I,II,L,LL).
   band(I,II,L,LL)      :- binop(band,I,II,L,LL).
   bor(I,II,L,LL)       :- binop(bor,I,II,L,LL).
   xor(I,II,L,LL)       :- binop(xor,I,II,L,LL).
   modulo(I,II,L,LL )   :- binop(modulo,I,II,L,LL).
   mul(I,II,L,LL)       :- binop(mul,I,II,L,LL).
   'shift-l'(I,II,L,LL) :- binop('shift-l',I,II,L,LL).
   'shift-r'(I,II,L,LL) :- binop('shift-r',I,II,L,LL).
   divide(I,II,L,LL)    :- binop(divide,I,II,L,LL).

")
