;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log vm opt)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:use-module ((logic guile-log umatch) #:select (*current-stack* gp-lookup))
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log vm vm)
  #:export (opt))

;;Track Values in the stack
;;X->I   source
;;I->II
;;II->Y  (sink
;;[K,X]

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define (opt l) l)
(define (xopt l)
  (let* ((v (list->vector l))
	 (n ((@ (guile) length) l)))    
    (let lp ((i 0) (clobs vlist-null) (sources vlist-null) (sinks vlist-null) (deletes '()))
      (if (< i n)
	  (call-with-values (lambda () (clobber (vector-ref v i)))
	    (lambda (i1 i2)
	      (let ((sources
		     (cond
		      ((and i1 i2)
		       (aif it (vhash-ref sources i1 #f)
			    (aif it2 (vhash-ref clobs i2 #f)
				 (if (< it2 it)
				     (begin
				       (set! deletes (cons i l))
				       (vhash-cons i2 it sources))
				     sources)
				 (vhash-cons it i2 sources))
			    (vhash-cons i2 i sources)))
		      (i1
		       sources)
		      (i2
		       (vhash-cons i2 i sources))
		      (else
		       sources)))
		    
		    (sinks
		     (cond
		      ((and i1 i2)
		       sinks)
		      (i1
		       (vhash-cons i1 i sinks))
		      (else
		       sinks)))

		    (clobs
		     (if i1
			 (vhash-cons i1 i clobs)
			 clobs)))
		(lp (+ i 1) clobs sources sinks deletes))))
	  
	  (begin
	    (let lp ((i 0))
	      (if (< i n)
		  (call-with-values (lambda () (clobber (vector-ref v i)))
		    (lambda (i1 i2)
		      (cond
		       ((and i1 i2)
			(if ((@ (guile) member) i deletes)
			    (begin
			      (vector-set! v i #f)
			      #t)
			    (aif it (vhash-ref i1 sources #f)
				 (call-with-values (lambda () (clobber (vector-ref v it)))
				   (lambda (j1 j2)
				     (if (= j2 i1)
					 #t
					 (vector-set! v it (modify-source (vector-ref v it) i1)))))
				 #t)))
		     
		       (i1
			(aif it (vhash-ref sources i2 #f)
			     (call-with-values (lambda () (clobber (vector-ref v it)))
			       (lambda (j1 j2)
				 (if (and j1 j2)
				     (vector-set! v it (list #:k i j1)))))))
		       (else
			#t))
		    
		      (lp (+ i 1))))))
	    
	    (let lp ((i 0) (l '()))
	      (if (< i n)
		  (let ((x (vector-ref v i)))
		    (if x
			(begin
			  (if (and (pair? x) (eq? (car x) #:k))
			      (let ((j (list-ref x 1))
				    (k (list-ref x 2)))
				(set! x (modify-sink (vector-ref v k) k))))
			  (lp (+ i 1) (cons x l)))
			(lp (+ i 1) l)))
		  (reverse l))))))))
			  

(compile-prolog-string
"
  clobber0(mov(A,B)) :- !,
    call((AA is A)),
    call((BB is B)),
    cc(B,A).

  clobber0('scm-ref/immediate'(A,_,_)) :- !,
    call((AA is A)),
    cc(#f,AA).

  clobber0('get-constant'(A,_)) :- !,
    call((AA is A)),
    cc(#f,AA).

  clobber0(_) :- cc(#f,#f).

  modsource(mov(A,B),C) :- !,
    cc(mov(A,C)).

  modsource('scm-ref/immediate'(A,B,C),D) :- !,
    cc('scm-ref/immediate'(D,B,C)). 

  modsource('get-constant'(A,B),C) :- !,
    cc('get-constant'(C,B)).   

  modsource(X,I) :- throw('not a source instruction').

  modsink(X,I) :- throw('not a sink instruction').

")

(define (clobber x)
  (clobber0 (fluid-ref *current-stack*) (lambda () #f)
	    (lambda (s p x y) (values (gp-lookup x s) (gp-lookup y s))) x))

(define (modify-source x i)
  (modsource (fluid-ref *current-stack*) (lambda () #f)
	     (lambda (s p x) (gp-lookup x s)) x i))

(define (modify-sink x i)
  (modsink  (fluid-ref *current-stack*) (lambda () #f)
	    (lambda (s p x) (gp-lookup x s)) x i))
