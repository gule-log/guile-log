;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(use-modules (logic guile-log iso-prolog))
(define car #f)
(eval-when (compile eval load)
  (make-unbound-term or)
  (make-unbound-term and))
(reset-prolog)

(compile-prolog-file "vanilla.pl")

(save-operator-table)
(prolog-run 1 (local_initialization))
