;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(use-modules (logic guile-log iso-prolog))
(eval-when (compile eval load)
  (make-unbound-term or)
  (make-unbound-term and))

(compile-file "inriasuite.pl")
(save-operator-table)