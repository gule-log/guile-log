;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (inriasuite)
  #:use-module (logic guile-log iso-prolog)
  #:use-module ((guile) #:select (@ @@ define))
  #:pure
  #:re-export (prolog-run)
  #:export (run_tests  run_all_tests unexpected_ball <-- exists
		       failure success impl_def undefined))

(compile-prolog-file "inriasuite.pl")
(save-operator-table)
