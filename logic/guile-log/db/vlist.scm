;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log db vlist)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db table-vlist)
  #:use-module (logic guile-log db data)
  #:use-module (logic guile-log db objects)
  #:export
  (add-vlist lookup-vlist lookup-rvlist pull-vlists))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define vlist-base   (@@ (logic guile-log code-load) vlist-base))
(define vlist-offset (@@ (logic guile-log code-load) vlist-offset))
(define make-vlist   (@@ (logic guile-log code-load) make-vlist))

(define (add-vlist db vlist)
  (let ((vlists  (slot-ref db 'vlists))
	(rvlists (slot-ref db 'rvlists))
	(key     (slot-ref db 'vlist-key)))
    (aif it (hash-ref vlists vlist)
         it        
         (let* ((table-id (add-table db
				     (vlist-base   vlist)
				     (vlist-offset vlist))))
           (hashq-set! vlists  vlist (cons* key table-id (vlist-offset vlist)))
	   (hash-set!  rvlists key   vlist)
           (slot-set! db 'vlist-key (+ key 1))
	   key))))

(define (lookup-vlist db x)
  (let ((vlists (slot-ref db 'vlists)))
    (aif it (hashq-ref vlists x)
         (car it)
	 (error "no vlist in database"))))

(define (lookup-rvlist db x)
  (let ((vlists (slot-ref db 'rvlists)))
    (aif it (hash-ref vlists x)
         it
	 (error "no vlist in rev database"))))

(define (pull-vlists db tr)
  (let* ((vlists   (slot-ref db 'vlists))
	 (rvlists  (slot-ref db 'rvlists))
	 (nlast    (slot-ref db 'vlist-last-key))
	 (ncur     (slot-ref db 'vlist-key))
	 (rows     (db-pull-vlists db nlast))
	 (rowsp    (pair? rows))	 
	 (newlast  (if rowsp
		       (let lp ((rows rows) (n nlast))
			 (match rows
			   (((k . _) . rows)
			    (lp rows (max k n)))
			   (()
			    (+ n 1))))
		       nlast))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rvlists i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hashq-ref vlists it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))

    (if rowsp
	(let lp ((newme newme))
	  (match newme
	    (((s . (k . _)) . newme)
	     (hashq-remove! vlists  s)
	     (hash-remove! rvlists k)
	     (lp newme))
	    (() #t))))
	
    (let lp ((rows rows))
      (match rows
	(((key tid . off) . rows)
	 (let ((m (let ((t (lookup-rtable db tid)))
		    (make-vlist t off))))
			
	   (hash-set! vlists  m   (cons* key tid off))
	   (hash-set! rvlists key m)
	   (lp rows)))
	(()
	 #t)))

    (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . (k . v)) . newme)
	 (if rowsp
	     (aif it (hashq-ref vlists s)
		  (lp newme (cons (cons* s k (car it)) l)
		      w                   n)
		  (lp newme (cons (cons* s k n ) l)
		      (cons (cons* s n v) w) (+ n 1)))
	     (lp newme l
		 (cons (cons* s k v) w) ncur)))
	     
	(()
	 (let lp ((w w) (l '()))
	   (match w
	     (((s n tid . off) . w)
	      (if rowsp
		  (let ((v (cons (tr tid) off)))
		    (hashq-set! vlists  s (cons n v))
		    (hash-set! rvlists n s)
		    (lp w (cons (cons n v) l)))
		  (lp w (cons (cons* n tid off) l))))
	     (()
	      (db-write-vlists db l))))
	 
	 (slot-set! db 'vlist-key n)
	 (slot-set! db 'vlist-last-key n)


	 (let ((m (make-hash-table)))
	   (let lp ((l l))
	     (match l
	       (((s k1 . k2) . l)
		(hash-set! m k1 k2)
		(lp l))
	       (() #t)))

	   (lambda (k) (hash-ref m k k))))))))


(add-custom vlist? add-vlist lookup-rvlist lookup-vlist)

(define-values
  (type-add type-from-tp)
  (let ((tpkey (new-object-type)))
    (values
     (lambda (db w)
       (let ((key (add-vlist db w)))
	 (cons tpkey key)))
     (lambda (key) (equal? key tpkey)))))

(define (type-pull db tr-s tr-m tr-d)
  (call-with-values (lambda () (pull-tables db tr-d))
    (lambda (tr tr-n)
      (pull-vlists db tr tr-n))))

(define (type-push db)
  (push-tables db)
  (push-vlists db))

(define (type-diff* old new)
  (if (eq? old new)
      '()
      (let ((told (struct-ref old 0))
	    (nold (struct-ref old 1)))
	(let lp ((tnew (struct-ref new 0))
		 (nnew (min (vector-ref (struct-ref new 0) 4)
			    (struct-ref new 1)))
		 (l    '()))
	  (let ((v (vector-ref tnew 0))
		(s (vector-ref tnew 3)))
	    (if (eq? told tnew)
		(let lp2 ((n nnew) (l l))
		  (if (= n nold)
		      (begin
			(vector-set! tnew 4 nold)
			l)
		      (let ((k (vector-ref v (- n 1))))
			(vector-set! v (- n 1) #f)
			(vector-set! v (+ s (- n 1)) #f)
			(lp2 (- n 1) (cons k l)))))
		(let lp2 ((n nnew) (l l))
		  (if (= n 0)
		      (lp (vector-ref tnew 1) (vector-ref tnew 2) l)
		      (lp2 (- n 1) (cons (vector-ref v (- n 1))
					 l))))))))))

(define (type-append db o l)
  (let lp ((vh (o)) (l l))
    (if (pair? l)
	(lp (vlist-cons (car l) vh) (cdr l))
	(add-vlist db vh))))

(define (type-diff o old)
  (let ((ret (type-diff* (o) old)))
    (o old)
    ret))
  
(add-db-type vlist?
	     #:mk      lookup-vlist
	     #:from-tp type-from-tp
	     #:pull    type-pull
	     #:add     type-add
	     #:diff    type-diff
	     #:push    type-push
	     #:append  type-append)
