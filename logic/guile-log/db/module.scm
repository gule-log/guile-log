;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log db module)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db string)
  #:export (add-module lookup-module lookup-rmodule pull-modules push-modules))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define (add-module db x)
  (if (not (module? x)) (error "Not a string" x))

  (let ((y        (module-name x))
	(modules  (slot-ref db 'modules))
	(rmodules (slot-ref db 'rmodules)))
    (aif it (hash-ref modules x)
         (car it)
         (let* ((rep (map (lambda (x)
			    (add-string db (symbol->string x)))
			  y))
		(key (slot-ref db 'module-key)))
           (hash-set! modules  y   (cons key rep))
	   (hash-set! rmodules key y)
	   (slot-set! db 'module-key (+ key 1))
           key))))

(define (lookup-module db x)
  (let ((modules (slot-ref db 'modules)))
    (aif it (hash-ref modules (module-name x))
         (car it)
	 (error "no module in database"))))

(define (lookup-rmodule db x)
  (let ((modules (slot-ref db 'rmodules)))
    (aif it (hash-ref modules x)
         (resolve-module it)
	 (error "no module in database"))))

(define (pull-modules db trstr)
  (let* ((modules  (slot-ref db 'modules))
	 (rmodules (slot-ref db 'rmodules))
	 (nlast    (slot-ref db 'module-last-key))
	 (ncur     (slot-ref db 'module-key))
	 (rows     (db-pull-modules db nlast))
	 (rowsp    (pair? rows))	 
	 (newlast  (if rowsp
		       (let lp ((rows rows) (n nlast))
			 (match rows
			   (((k . _) . rows)
			    (lp rows (max k n)))
			   (()
			    (+ n 1))))
		       nlast))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rmodules i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hash-ref modules it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))

    (if rowsp
	(let lp ((newme newme))
	  (match newme
	    (((s . (k . _)) . newme)
	     (hash-remove! modules  s)
	     (hash-remove! rmodules k)
	     (lp newme))
	    (() #t))))
	
    (let lp ((rows rows))
      (match rows
	(((key . v) . rows)
	 (let ((m (map (lambda (x)
			 (string->symbol (lookup-rstring db x)))
		       v)))
	   (hash-set! modules  m   (cons key v))
	   (hash-set! rmodules key m)
	   (lp rows)))
	(()
	 #t)))

   (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . (k . v)) . newme)
	 (if rowsp
	     (aif it (hash-ref modules s)
		  (lp newme (cons (cons* s k (car it)) l)
		      w                   n)
		  (lp newme (cons (cons* s k n ) l)
		      (cons (cons* s n v) w) (+ n 1)))
	     (lp newme l
		 (cons (cons* s k v) w) ncur)))
	     
	(()
	 (let lp ((w w) (l '()))
	   (match w
	     (((s n . v) . w)
	      (if rowsp
		  (let ((v (map trstr v)))
		    (hash-set! modules  s (cons n v))
		    (hash-set! rmodules n s)))
	      (lp w (cons (cons n v) l)))
	     (()
	      (db-write-modules db l))))
	 
	 (slot-set! db 'module-key n)
	 (slot-set! db 'module-last-key n)


	 (let ((m (make-hash-table)))
	   (let lp ((l l))
	     (match l
	       (((s k1 . k2) . l)
		(hash-set! m k1 k2)
		(lp l))
	       (() #t)))

	   (lambda (k) (hash-ref m k k))))))))

(define (push-modules db)
  (let* ((modules  (slot-ref db 'modules))
	 (rmodules (slot-ref db 'rmodules))
	 (nlast    (slot-ref db 'module-last-key))
	 (ncur     (slot-ref db 'module-key))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rmodules i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hash-ref modules it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))


    (let lp ((newme newme) (l '()) (w '()) (n nlast))
      (match newme
	(((s . (k . v)) . newme)
	 (lp newme l
	     (cons (cons* s k v) w) ncur))
	     
	(()
	 (let lp ((w w) (l '()))
	   (match w
	     (((s n . v) . w)
	      (lp w (cons (cons n v) l)))
	     (()
	      (db-write-modules db l))))
	 
	 (slot-set! db 'module-key n)
	 (slot-set! db 'module-last-key n))))))
