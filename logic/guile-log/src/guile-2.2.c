
struct mapped_elf_image
{
  char *start;
  char *end;
  char *frame_maps;
};

SCM_DEFINE(gp_find_elf_relative_adress, "gp-bv-address", 1, 0, 0, 
	   (SCM bv),
	   "dives down in a gp variable one time")
#define FUNC_NAME s_gp_find_elf_relative_adress
{
  uint64_t ref = (uint64_t) SCM_BYTEVECTOR_CONTENTS(bv);
  return scm_from_uintptr_t(ref);
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_null_procedure, "gp-make-null-procedure", 2, 0, 0, (SCM n, SCM def), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_make_null_procedure
{
  int i,nfree;
  scm_t_bits *x;
  uintptr_t a = scm_to_uintptr_t(def);
  nfree = scm_to_int(n);
  x     = (scm_t_bits *) scm_gc_malloc (sizeof(SCM)*(nfree + 2), "program");  
  x[0] = scm_tc7_program;
  x[1] = a;
  for(i = 0; i < nfree; i++)
    {
      x[2+i] = SCM_UNPACK(SCM_UNSPECIFIED);
    }
  return GP_UNREF(x);
}
#undef FUNC_NAME

SCM_DEFINE(gp_fill_null_procedure, "gp-fill-null-procedure", 3, 0, 0, (SCM proc, SCM addr, SCM l), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_fill_null_procedure
{
  uintptr_t a = scm_to_uintptr_t(addr);
  int i = 0;
  SCM_SET_CELL_WORD_1 (proc, a);
  for(;SCM_CONSP(l);l = SCM_CDR(l),i++)
    {
      SCM_PROGRAM_FREE_VARIABLE_SET(proc,i,SCM_CAR(l));
    }
  return SCM_UNSPECIFIED;
}

#undef FUNC_NAME
SCM_DEFINE(gp_make_struct, "gp-make-struct", 2, 0, 0, 
	   (SCM vtable_data, SCM n), 
	   "")
#define FUNC_NAME s_gp_make_struct
{
  SCM ret;
  int i,nn = scm_to_int(n);
  ret = scm_words ((scm_t_bits)vtable_data | scm_tc3_struct, scm_to_int(n) + 2);
  SCM_SET_CELL_WORD_1 (ret, (scm_t_bits)SCM_CELL_OBJECT_LOC (ret, 2));
  for(i = 0; i < nn; i++)
    GP_GETREF(ret)[i+2] = SCM_UNSPECIFIED;
  return ret;
}
#undef FUNC_NAME


SCM_DEFINE(gp_set_struct, "gp-set-struct", 2, 0, 0, 
	   (SCM s, SCM l), 
	   "")
#define FUNC_NAME s_gp_set_struct  
{
  int i;
  SCM *v = GP_GETREF(s);
  SCM vtable = SCM_CAR(l);
  l = SCM_CDR(l);
  v[0] = SCM_PACK(((scm_t_bits) SCM_STRUCT_DATA(vtable)) | scm_tc3_struct);
  v    = GP_GETREF(v[1]);
  for(i=0;SCM_CONSP(l);l=SCM_CDR(l),i++)
    {
      v[i] = SCM_CAR(l);
    }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME
