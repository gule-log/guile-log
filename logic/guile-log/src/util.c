
/*
SCM_DEFINE(gp_member, "-gp-member", 3, 0, 0, 
	   (SCM x, SCM l, SCM s), 
	   "the member operation for logical variables")
{
  SCM f,a;
  
 retry:
  s = gp_pair_bang(l,s);
  if(scm_is_false(s)) goto bang_out;

  f = gp_newframe(s);

  a = gp_car(l,f);
  l = gp_gp_cdr(l,f);

  s = gp_gp_unify(x,a,f); 
  if(scm_is_false(s)) goto bang_next;

  return scm_cons(s,scm_cons(f,l));
  
 bang_out:
  return SCM_BOOL_F;
  
 bang_next:
  s = f;
  gp_unwind(f);
  goto retry;
}
*/
#define unify(x,y,s) gp_gp_unify(x,y,s);
SCM_DEFINE(gp_member, "-gp-member", 3, 0, 0, 
	   (SCM x, SCM l, SCM s), 
	   "the member operation for logical variables")
{
  SCM s0,a;
  
  s0 = s;
 retry:
  if(!SCM_CONSP(l)) goto bang_out;
  a = SCM_CAR(l);
  l = SCM_CDR(l);

  s = unify(x,a,s); 
  if(!s) goto bang_next;

  return scm_cons(s,l);
  
 bang_out:
  return SCM_BOOL_F;
  
 bang_next:
  s = s0;
  gp_unwind(s0);
  goto retry;
}

/*
SCM_DEFINE(gp_right_of, "-gp-right-of", 4, 0, 0, 
	   (SCM x, SCM y, SCM l, SCM s), 
	   "the member operation for logical variables")
{
  SCM f,a,b,ll;
  
 retry:
  s = gp_pair_bang(l,s);
  if(scm_is_false(s)) goto bang_out;

  a  = gp_car(l,s);
  ll = gp_gp_cdr(l,s);
  
  f = gp_newframe(s);
  
  s = gp_pair_bang(ll,f);
  if(scm_is_false(s)) goto bang_out;
  
  b = gp_car(ll,s);
  
  s = gp_gp_unify(a,x,s);
  if(scm_is_false(s)) goto bang_next;

  s = gp_gp_unify(b,y,s);
  if(scm_is_false(s)) goto bang_next;

  return scm_cons(s,scm_cons(f,ll));
  
 bang_out:
  return SCM_BOOL_F;
  
 bang_next:
  l = ll;
  s = f;
  gp_unwind(f);
  goto retry;
}
*/

SCM_DEFINE(gp_right_of, "-gp-right-of", 4, 0, 0, 
	   (SCM x, SCM y, SCM l, SCM s), 
	   "the member operation for logical variables")
{
  SCM s0,a,b,ll,*vv1,*vv2;
  
  s0 = s;
 retry:
  if(!SCM_CONSP(l)) goto bang_out;

  a  = SCM_CAR(l);
  ll = SCM_CDR(l);
  
  if(!SCM_CONSP(ll)) goto bang_out;
  
  b = SCM_CAR(ll);

  s = unify(a,x,s);
  if(!s) goto bang_next;

  s = unify(b,y,s);
  if(!s) goto bang_next;
    
  return scm_cons(s,ll);
  
 bang_out:
  return SCM_BOOL_F;
  
 bang_next:
  l = ll;
  s = s0;
  gp_unwind(s0);
  goto retry;
}


int memb_help(SCM **spp, int nargs, const SCM *closure)  
{

  (*spp)[-nargs + 0] = closure[0];
  (*spp)[-nargs + 1] = closure[1];
  *spp = *spp - nargs + 1;

  return 1;
}


int mb(SCM **spp, int nargs, const SCM *closure)
{
  SCM *sp,a,s,l,pp,f;

  //printf("memb\n");fflush(stdout);


  
  l = (*spp)[0];
  f = closure[0];

 retry:
  //printf("memb iter\n");fflush(stdout);
  gp_gp_unwind(f);

  if(!SCM_CONSP(l)) goto bail;
  
  a = SCM_CAR(l);
  l = SCM_CDR(l);
  
  s = gp_gp_unify(a,closure[3],f);
  if(scm_is_false(s)) goto retry;

  //printf("memb cc\n");fflush(stdout);

  SCM *p;
  pp = gp_make_closure(3,&p,s);
  p[0] = PTR2NUM(memb_help);
  p[1] = closure[4];
  p[2] = l;

  sp = *spp;
  sp[-nargs + 0] = closure[2];
  sp[-nargs + 1] = s;
  sp[-nargs + 2] = pp;
  *spp = sp - nargs + 2;
  return 2;

 bail:
  //printf("memb/fail\n");fflush(stdout);
  //gp_format1("memb fail object ~a~%",closure[1]);
  sp = *spp;
  sp[-nargs + 0] = closure[1];
  *spp = sp - nargs;
  return 0;
}



int mbr(SCM **spp, int nargs, const SCM *closure)
{
  SCM *sp,f,*pp,ppp;

  //printf("member\n");fflush(stdout);

  sp = *spp;

  ppp = gp_make_closure(6,&pp,sp[-4]);

  f = gp_newframe(sp[-4]);
  pp[0] =  PTR2NUM(mb);
  pp[1] = f;
  pp[2] = sp[-3];
  pp[3] = sp[-2];
  pp[4] = sp[-1];
  pp[5] = ppp;

  sp[-nargs + 0] = ppp;
  sp[-nargs + 1] = gp_gp_lookup(sp[0],f);

  *spp = sp - nargs + 1;
  return 1;
}


 
int rght(SCM **spp, int nargs, const SCM *closure)
{
  SCM *sp,a,b,s,l,pp,f;
  //printf("rght\n");fflush(stdout);

  l = (*spp)[0];
  f = closure[0];

 retry:
  //printf("rght iter\n");fflush(stdout);
  gp_gp_unwind(f);
  if(!SCM_CONSP(l)) goto bail;
  
  a = SCM_CAR(l);
  l = SCM_CDR(l);
  
  if(!SCM_CONSP(l)) goto bail;
  b = SCM_CAR(l);

  s = gp_gp_unify(a,closure[3],f);
  if(scm_is_false(s)) goto retry;

  s = gp_gp_unify(b,closure[4],s);
  if(scm_is_false(s)) goto retry;
  
  SCM *p;
  pp = gp_make_closure(3,&p,s);
  p[0] = PTR2NUM(memb_help);
  p[1] = closure[5];
  p[2] = l;

  sp = *spp;
  sp[-nargs + 0] = closure[2];
  sp[-nargs + 1] = s;
  sp[-nargs + 2] = pp;
  *spp = sp - nargs + 2;
  return 2;

 bail:
  //printf("rght/fail\n");fflush(stdout);
  //gp_format1("memb fail object ~a~%",closure[1]);
  sp = *spp;
  sp[-nargs + 0] = closure[1];
  *spp = sp - nargs;
  return 0;
}



int ri(SCM **spp, int nargs, const SCM *closure)
{
  
  SCM *sp,f,*pp,ppp;

  //printf("right\n");fflush(stdout);

  sp = *spp;

  
  ppp = gp_make_closure(7,&pp,sp[-5]);

  f = gp_newframe(sp[-5]);

  pp[0] = PTR2NUM(rght);
  pp[1] = f;
  pp[2] = sp[-4];
  pp[3] = sp[-3];
  pp[4] = sp[-2];
  pp[5] = sp[-1];
  pp[6] = ppp;

  sp[-nargs + 0] = ppp;
  sp[-nargs + 1] = gp_gp_lookup(sp[0],f);
  *spp = sp - nargs + 1;
  return rght(spp,1,pp + 1);
}


#define AREF(x,i) (x)[i]
#define ERROR1(x)     scm_misc_error("",x,SCM_EOL)
#define ERROR3(x,y,z) scm_misc_error(x,y,z)
#include "einstein.c"

SCM_DEFINE(gp_make_member, "gp-make-member", 0, 0, 0, 
	   (), 
	   "generate the member operation for logical variables")
{
  return member;
}
/*
{
  SCM pp;
  pp = scm_c_make_vector(1,SCM_BOOL_F);
  scm_c_vector_set_x(pp, 0, PTR2NUM(member));
  return scm_cons(closure_tag,pp);
}
*/

SCM_DEFINE(gp_make_log_api, "gp-make-log-api", 0, 0, 0, 
	   (), 
	   "generate an api of fast calls for logic code")
{
  return make__api__list();
}

SCM_DEFINE(gp_make_right, "gp-make-right-of", 0, 0, 0, 
	   (), 
	   "generate the right-of operation for logical variables")
{
  return right;
}


SCM_DEFINE(gp_make_einstein, "gp-make-einstein", 0, 0, 0, 
	   (), 
	   "generate the right-of operation for logical variables")
{
  init__einstein();
  
  return /*einstein*/ SCM_BOOL_F;
}

SCM_DEFINE(gp_set_closure_tag, "gp-set-closure-tag", 1, 0, 0, 
	   (SCM tag), 
	   "set the closure tag")
{
  closure_tag = tag;
  return SCM_UNSPECIFIED;
}
