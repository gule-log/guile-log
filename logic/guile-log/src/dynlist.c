SCM get_set_r(SCM r, SCM bits)
{
 retry:
  if(SCM_CONSP(r))
    {
      SCM cdr = SCM_CDR(r);
      if(SCM_NULLP(cdr))
	{
	  r = SCM_CAR(r);
	  goto retry;
	}
	 
      if(SCM_CONSP(cdr))
	{
	  SCM d = SCM_CAR(r);
	  SCM l = SCM_CAR(cdr);
	  SCM r = SCM_CDR(cdr);
	  if(scm_is_false(scm_equal_p(scm_logand(bits, d),
				      scm_from_int(0))))
	    {
	      return scm_logior(get_set_r(l, bits),get_set_r(r, bits));
	    }
	  else
	    return scm_from_int(0);
	}
      
      return scm_from_int(0);
    }	

  if(scm_is_vector(r))
    {
      if(scm_is_false(scm_equal_p(scm_logand(bits, scm_c_vector_ref(r,0)),
				  scm_from_int(0))))
	return scm_c_vector_ref(r, 1);
    }

  return scm_from_int(0);
}


SCM get_set(SCM r, SCM bits)
{
  if(SCM_CONSP(r))
    {
      SCM cdr = SCM_CDR(r);
      if(SCM_CONSP(cdr))
	{
	  SCM cddr = SCM_CDR(cdr);
	  if(SCM_CONSP(cddr))
	    {
	      SCM cdddr = SCM_CDR(cdr);
	      if(scm_is_true(scm_equal_p(SCM_CAR(cddr), scm_from_int(-1))))
		{
		  return scm_from_int(0);
		}
	      if(scm_is_true(scm_equal_p(SCM_CAR(cddr), scm_from_int(0))))
		{
		simple:
		  {
		    SCM l = SCM_CAR(r);
		    SCM d = SCM_CAR(cdr);
		    if(scm_is_true(scm_equal_p(scm_logand(d, bits), 
					       scm_from_int(0))))
		      return scm_from_int(0);
		    else
		      return get_set(l, bits);
		  }
		}
	      if(SCM_NULLP(cdddr)) goto simple;
	      
	      SCM l = SCM_CAR(r);
	      SCM d = SCM_CAR(cdr);
	      r = SCM_CDR(cdddr);
	      if(scm_is_true(scm_equal_p(scm_logand(d, bits),
					 scm_from_int(0))))
		return scm_from_int(0);
	      else
		return scm_logior(get_set(l, bits), get_set_r(r, bits));
	    }
	}
    }
  
  if(scm_is_vector(r))
    {
      if(scm_is_false(scm_equal_p(scm_logand(bits, scm_c_vector_ref(r,0)),
				  scm_from_int(0))))
	return scm_c_vector_ref(r, 1);
      else
	return scm_from_int(0);
    }
  
  return scm_from_int(0);
}
