/*
The need for this datastructure is that although the elements are rare,
the objects in the stack mutates and invalidates the functional nature of
the satck, therefore we will treat these datastructures specially. And because
we desided to keep a simple setup we cannot refere to the values of this objects
in a dynwind, references to the objects are fine though.

In a future version we might deside to put in a link between the functional 
structure and this tree and unwind/rewind them in a logically more correct way.
*/

//#define DB(X) X
//ID structure
#define D_VARIABLE_GUARD 2
#define D_FLUID          6
#define D_RGUARD         14
#define D_LGUARD         18

#define D_ID             0

//GUARD
#define D_GUARD_K        1
#define D_GUARD_VAR      2
#define D_GUARD_VAL      3
#define D_NGUARD         4

//RGUARD
#define D_RGUARD_VAR     1
#define D_RGUARD_K       2
#define D_OLD_K          3
#define D_NRGUARD        4

//LGUARD
#define D_LGUARD_VAR     1
#define D_LGUARD_K       2
#define D_OLD_K          3
#define D_NLGUARD        4

//FLUID 
#define D_FLUID_VAR      1
#define D_FLUID_VAL      2
#define D_OLD_K          3
#define D_NFLUID         4

static inline SCM make_rguard(SCM var, SCM val, SCM k)
{
  SCM  vnew_ = scm_c_make_vector(D_NGUARD - 1, SCM_BOOL_F);
  SCM *vnew  = SCM_I_VECTOR_WELTS(vnew_);
  vnew[D_GUARD_K   - 1] = k;
  vnew[D_GUARD_VAR - 1] = var;
  vnew[D_GUARD_VAL - 1] = val;
  
  return vnew_;
}

#define GET_ENV(h) SCM_I_VECTOR_WELTS(SCM_VARIABLE_REF(h));
#define OLD(h)     h[4];
#define NEXT(h)    h[3];
#define LEVEL(h)   h[0];

SCM_DEFINE(gp_set_tester, "gp-set-tester", 1, 0, 0,(SCM x),"")
#define FUNC_NAME s_gp_set_tester
{
  tester = x;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_rguards, "gp-rguards-ref", 0, 0, 0,(),"")
#define FUNC_NAME s_gp_get_rguards
{
  struct gp_stack *gp = get_gp();
  return gp->rguards;
}
#undef FUNC_NAME

SCM_DEFINE(undo_safe_variable_guard, "gp-undo-safe-variable-guard", 3, 0, 0,
	   (SCM var, SCM kind, SCM s),
	   "")
#define FUNC_NAME s_undo_safe_variable_guard
{
  SCM ggp;
  struct gp_stack *gp;
  UNPACK_ALL00(ggp,gp,s,"failed to unpack s in undo_safe_variable_guard");

  if(SCM_VARIABLEP(var) || SCM_FLUID_P(var) || SCM_CONSP(var) 
     || scm_is_true(scm_procedure_p (var)))
    {      
      SCM  vnew_ = scm_c_make_vector(D_NGUARD, SCM_BOOL_F);
      SCM *vnew  = SCM_I_VECTOR_WELTS(vnew_);
      vnew[D_ID]        = SCM_PACK(D_VARIABLE_GUARD);
      vnew[D_GUARD_K]   = kind;
      vnew[D_GUARD_VAR] = var;
      SCM val;
      if(SCM_CONSP(var))
	val = SCM_CDR(var);
      else if(SCM_VARIABLEP(var))
	val = SCM_VARIABLE_REF(var);
      else if(SCM_FLUID_P(var))
	val = scm_fluid_ref(var);
      else
        val = scm_call_0(var);

      vnew[D_GUARD_VAL] = val;
      
      gp->dynstack = scm_cons(vnew_, gp->dynstack);
      gp->dynstack_length += 4;

      return SCM_UNSPECIFIED;
    }
  
  scm_misc_error("undo_safe_variable_guard", 
		 "argument 1 must be a variable or fluid, got ~a", 
		 scm_list_1(var));
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM rguard_skip(SCM rguard, SCM var);

SCM get_l_k_part(SCM k, SCM guards)
{
  SCM pt;
  for(pt = guards; SCM_CONSP(pt); pt = SCM_CDR(pt))
    {
      SCM *v = SCM_I_VECTOR_WELTS(SCM_CAR(pt));
      if(v[D_LGUARD_VAR] == k)
	return v[D_OLD_K];
    }
  return SCM_BOOL_F;
}

SCM_DEFINE(undo_safe_variable_rguard, "gp-undo-safe-variable-rguard", 3, 0, 0,
	   (SCM var, SCM kind, SCM s),
	   "")
#define FUNC_NAME s_undo_safe_variable_rguard
{
  SCM ggp;
  struct gp_stack *gp;
  UNPACK_ALL00(ggp,gp,s,"failed to unpack s in undo_safe_variable_rguard");

  if(SCM_VARIABLEP(var) || SCM_FLUID_P(var) || SCM_CONSP(var) || scm_is_true(scm_procedure_p (var)))
    {      
      SCM  vnew_ = scm_c_make_vector(D_NRGUARD, SCM_BOOL_F);
      SCM* vnew  = SCM_I_VECTOR_WELTS(vnew_);
      vnew[D_ID]         = SCM_PACK(D_RGUARD);
      vnew[D_RGUARD_K]   = kind;
      vnew[D_RGUARD_VAR] = var;
      vnew[D_OLD_K]      = SCM_BOOL_F;

      gp->dynstack = scm_cons(vnew_, gp->dynstack);
      gp->dynstack_length += 4;

      if(scm_is_true(scm_procedure_p (var))) 
	{
	  SCM old_kind = get_l_k_part(var, gp->dynstack);
	  if(old_kind != SCM_BOOL_F)
	    {
	      gp->rguards = 
		scm_cons(scm_cons(var,old_kind), rguard_skip(gp->rguards, var));
	      return SCM_UNSPECIFIED;      
	    }
	}
                  
      gp->rguards  = rguard_skip(gp->rguards, var);      

      return SCM_UNSPECIFIED;
    }
  
  scm_misc_error("undo_safe_variable_guard", 
		 "argument 1 must be a variable or fluid, got ~a", 
		 scm_list_1(var));
  return SCM_BOOL_F;
}
#undef FUNC_NAME

int has_old_ref(SCM k, SCM guards)
{
  SCM pt;
  for(pt = guards; SCM_CONSP(pt); pt = SCM_CDR(pt))
    if(SCM_CAAR(pt) == k)
      return 1;
  return 0;
}
 
SCM get_oldkind(SCM k, SCM guards)
{
  SCM pt;
  for(pt = guards; SCM_CONSP(pt); pt = SCM_CDR(pt))
    {
      if(SCM_CAAR(pt) == k)
	return SCM_CDAR(pt);
    }
  return SCM_BOOL_F;
}

SCM_DEFINE(undo_safe_variable_lguard, "gp-undo-safe-variable-lguard", 3, 0, 0,
	   (SCM var, SCM kind, SCM s),
	   "")
#define FUNC_NAME s_undo_safe_variable_lguard
{
  SCM ggp;
  struct gp_stack *gp;
  //format2("lguard ~a -> ~a~%",var, SCM_FLUID_P(var)?scm_fluid_ref(var):SCM_BOOL_F);
  UNPACK_ALL00(ggp,gp,s,"failed to unpack s in undo_safe_variable_rguard");

  if(SCM_VARIABLEP(var) || SCM_FLUID_P(var) || SCM_CONSP(var)
     || scm_is_true(scm_procedure_p (var)))
    {      
      SCM  vnew_ = scm_c_make_vector(D_NLGUARD, SCM_BOOL_F);
      SCM* vnew  = SCM_I_VECTOR_WELTS(vnew_);
      
      vnew[D_ID]         = SCM_PACK(D_LGUARD);
      vnew[D_LGUARD_K]   = kind;
      vnew[D_LGUARD_VAR] = var;
      vnew[D_OLD_K]      = SCM_BOOL_F;
      if(scm_is_true(scm_procedure_p (var)) && has_old_ref(var, gp->rguards)) 
	{
	  SCM old_kind = get_oldkind(var, gp->rguards);
	  if(old_kind != kind)
	    {
	      vnew[D_OLD_K] = old_kind;
	    }
	  else
	    return SCM_UNSPECIFIED;
	}
      
      gp->dynstack = scm_cons(vnew_, gp->dynstack);
      gp->dynstack_length += 4;
 
      gp->rguards = scm_cons(scm_cons(var, kind), 
			     gp->rguards);      

      return SCM_UNSPECIFIED;
    }
  
  scm_misc_error("undo_safe_variable_guard", 
		 "argument 1 must be a variable or fluid, got ~a", 
		 scm_list_1(var));
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_with_fluid, "gp-fluid-set", 2, 0, 0,
	   (SCM var, SCM val),
	   "")
#define FUNC_NAME s_gp_fluid_set
{
  struct gp_stack *gp;
  SCM a = scm_fluid_ref(gp_current_stack);				
  if(!GP_STACKP(a))							
    scm_misc_error("unpack_s2","gp-fluid-set, unknown *gp*",SCM_EOL);
  
  gp = (struct gp_stack *) SCM_SMOB_DATA(a);			

  //format3("var ~a -> ~a  | n = ~a~%",var,SCM_FLUID_P(var)?scm_fluid_ref(var):SCM_BOOL_F,SCM_PACK(gp->dynstack_length));

  if(SCM_VARIABLEP(var) || SCM_FLUID_P(var) 
     || scm_is_true(scm_procedure_p (var)))
    {      
      SCM  vnew_ = scm_c_make_vector(D_NFLUID, SCM_BOOL_F);
      SCM* vnew  = SCM_I_VECTOR_WELTS(vnew_);
      vnew[D_ID]        = SCM_PACK(D_FLUID);
      vnew[D_FLUID_VAR] = var;
      
      SCM temp;
      if(SCM_VARIABLEP(var))
	{
	  temp = SCM_VARIABLE_REF(var);
	  SCM_VARIABLE_SET(var,val);
	}
      else if(SCM_FLUID_P(var))
	{
	  temp = scm_fluid_ref(var);
	  scm_fluid_set_x(var, val);
	}
      else
        {
	  temp = scm_call_0(var);
	  scm_call_1(var, val);
        }

      vnew[D_FLUID_VAL]   = temp;

      gp->dynstack = scm_cons(vnew_, gp->dynstack);
      gp->dynstack_length += 4;
      
      return SCM_UNSPECIFIED;
    }
  
  scm_misc_error("undo_safe_variable_guard", 
		 "argument 1 must be a variable or fluid, got ~a", 
		 scm_list_1(var));
  return SCM_BOOL_F;
}
#undef FUNC_NAME

#define LONG(x) (((long) (SCM_UNPACK((x)))) >> 2);
//#define DB(X) X

static inline long find_k(long kk, SCM hk)
{
  SCM* env;
  while (!SCM_NULLP(hk))
    {
      env = GET_ENV(hk);
      SCM x = LEVEL(env);
      kk = kk + LONG(x);
      SCM o = OLD(env);
      //gp_format2("find_k level ~a, old ~a~%",x,o);
      if(!(scm_is_eq(o, SCM_EOL) || scm_is_false(o))) goto advanced;      
      hk = NEXT(env);
    }
  return kk;


 advanced:
  {
    SCM o = OLD(env);
    SCM nexties = scm_cons(scm_cons(hk,o),SCM_EOL);
    hk = NEXT(env);
    while(1)
      {
	SCM p;
	if(scm_is_eq(hk, SCM_EOL)) return kk;

	p = scm_assq(hk, nexties);
	if(scm_is_true(p)) 
	  {
	    SCM x = SCM_CDR(p);
	    if(scm_is_eq(x, SCM_EOL)) return kk;

	    env = SCM_I_VECTOR_WELTS(SCM_CAR(p));
	    SCM_SETCDR(p, SCM_CDDR(p));
	  }
	
	SCM x = LEVEL(env);
	kk = kk + LONG(x);
	
	SCM old = OLD(env);
	if(!(scm_is_eq(old, SCM_EOL) || scm_is_false(old)))
	  {
	    if(scm_is_false(p))
	      {
		nexties = scm_cons(scm_cons(hk, old),nexties);
	      }
	  }

	hk = NEXT(env);
      }     
  }
}
#define DB(X)

static inline int dynwind_check(SCM k, SCM K)
{

  gp_format2("k(~a) >= K(~a)\n",k,K);

  if(scm_is_eq(k, SCM_BOOL_T))
    return 1;
      
  if(scm_is_false(k))
    return 0;

  if(scm_is_eq(K, SCM_BOOL_T))
    return 1;

  if(scm_is_eq(K, SCM_BOOL_F))
    return 0;
  
  
  if(SCM_CONSP(k) && SCM_CONSP(K))
    {
      long kk = LONG(SCM_CAR(k));
      long KK = LONG(SCM_CAR(K));
      SCM  hk = SCM_CDR(k);
      SCM  hK = SCM_CDR(K);
      if(!scm_is_eq(hk, hK))
	{
	  kk = find_k(kk, hk);
	  KK = find_k(KK, hK);
	  DB(printf("RES: %d >= %d\n", (int) kk, (int) KK));
	}

      return kk >= KK;
    }
  
  if(SCM_CONSP(k))
    {
      long kk = LONG(SCM_CAR(k));
      SCM  hk = SCM_CDR(k);
      long KK = LONG(K);

      kk = find_k(kk, hk);
     
      DB(printf("RES: %d >= %d\n", (int) kk, (int) KK));
      return kk >= KK;
    }

  if(SCM_CONSP(K))
    {
      long KK = LONG(SCM_CAR(K));
      SCM  hK = SCM_CDR(K);
      long kk = LONG(k);
      
      KK = find_k(KK, hK);

      //DB(printf("RES: %d >= %d\n", (int) kkk, (int) kk));
      return kk >= KK;
    }

  return SCM_UNPACK(k) >= SCM_UNPACK(K);
}
//#define DB(X) 

SCM rguard_skip(SCM rguard, SCM var)
{
  if(SCM_CONSP(rguard))
    {
      if(scm_is_eq(SCM_CAAR(rguard), var)) return SCM_CDR(rguard);
      SCM parent = rguard;
      SCM pt     = SCM_CDR(rguard);

      while(SCM_CONSP(pt))
	{
	  if(scm_is_eq(SCM_CAAR(pt), var))
	    {
	      SCM_SETCDR(parent,SCM_CDR(pt));
	      return rguard;
	    }
	  parent = SCM_CDR(parent);
	  pt     = SCM_CDR(pt);
	}
    }
  return rguard;
}

void eval_rguard(SCM guard, SCM K)
{
  SCM* v  = SCM_I_VECTOR_WELTS(guard);
  SCM k = v[D_GUARD_K - 1];

  gp_debug0("A guard\n");
  if(dynwind_check(k,K))
    {
      SCM var = v[D_GUARD_VAR - 1];
      SCM val = v[D_GUARD_VAL - 1];

      if(SCM_CONSP(var))
	SCM_SETCDR(var, val);
      else if(SCM_VARIABLEP(var))
	{
	  SCM_VARIABLE_SET(var, val);	     
	}
      else if(SCM_FLUID_P(var))
	{
	  scm_fluid_set_x(var, val);
	}
      else
        {
          scm_call_1(var, val);
        }
    }
}

void eval_rguards(SCM guards, SCM K)
{
  while(SCM_CONSP(guards))
    {
      eval_rguard(SCM_CAR(guards), K);
      guards = SCM_CDR(guards);
    }
}

SCM manage_dyn_wind(SCM obj, SCM K, SCM *rguard) {
  SCM* v  = SCM_I_VECTOR_WELTS(obj);
  scm_t_bits  id = SCM_UNPACK(v[D_ID]);
  if(id == D_VARIABLE_GUARD)
    {
      SCM k   = v[D_GUARD_K];
      SCM var = v[D_GUARD_VAR];

      if(dynwind_check(k,K))
	{	  
	  SCM val = v[D_GUARD_VAL];
	  if(SCM_CONSP(var))
	    SCM_SETCDR(var, val);
	  else if(SCM_VARIABLEP(var))
	    {
	      SCM_VARIABLE_SET(var, val);	     
	    }
	  else if(SCM_FLUID_P(var))
	    {
	      scm_fluid_set_x(var, val);
	    }
          else
            {
              scm_call_1(var, val);
            }
	}

      {
	SCM  vnew_ = scm_c_make_vector(D_NGUARD, SCM_BOOL_F);
	SCM* vnew  = SCM_I_VECTOR_WELTS(vnew_);
	vnew[D_ID]        = SCM_PACK(D_VARIABLE_GUARD);
	vnew[D_GUARD_K]   = v[D_GUARD_K];
	vnew[D_GUARD_VAR] = var;
	if(SCM_CONSP(var))
	  vnew[D_GUARD_VAL] = SCM_CDR(var);
	else if(SCM_VARIABLEP(var))
	  vnew[D_GUARD_VAL] = SCM_VARIABLE_REF(var);
	else if(SCM_FLUID_P(var))
	  vnew[D_GUARD_VAL] = scm_fluid_ref(var);
        else
          vnew[D_GUARD_VAL] = scm_call_0(var);
	return vnew_;
      }
    }

  if(id == D_LGUARD)
    {
      if(v[D_OLD_K] == SCM_BOOL_F)
	*rguard = scm_cons(scm_cons(v[D_LGUARD_VAR], v[D_LGUARD_K]), *rguard);
      else
	*rguard = scm_cons(scm_cons(v[D_LGUARD_VAR], v[D_LGUARD_K]), 
			   rguard_skip(*rguard, v[D_LGUARD_VAR]));
      return obj;
    }
  
  if(id == D_RGUARD)
    {
      SCM var = v[D_RGUARD_VAR];
      if(v[D_OLD_K] == SCM_BOOL_F)
	*rguard  = rguard_skip(*rguard, var);
      else
	*rguard = scm_cons(scm_cons(v[D_RGUARD_VAR], v[D_OLD_K]), 
			   rguard_skip(*rguard, var));
      return obj;
    }

  if(id == D_FLUID)
    {
      SCM  vnew_ = scm_c_make_vector(D_NFLUID, SCM_BOOL_F);
      SCM* vnew  = SCM_I_VECTOR_WELTS(vnew_);
      vnew[D_ID] = SCM_PACK(D_FLUID);
      SCM var = v[D_FLUID_VAR];
      vnew[D_FLUID_VAR] = var;
      SCM temp = SCM_UNDEFINED;
      SCM val = v[D_FLUID_VAL];
      if(SCM_VARIABLEP(var))
	{
	  temp = SCM_VARIABLE_REF(var);
	  SCM_VARIABLE_SET(var, val);
	}
      else if(SCM_FLUID_P(var))
	{
	  temp = scm_fluid_ref(var);
	  scm_fluid_set_x(var, val);
	}
      else
        {
	  temp = scm_call_0(var);
          scm_call_1(var, val);
        }
      vnew[D_FLUID_VAL] = temp;
      return vnew_;
    }
    
  scm_misc_error("dynwind/rewind", "tag is not one of the allowed ones ~a", 
		 scm_list_1(SCM_PACK(id)));
  
  return SCM_BOOL_F;
}
     

static inline SCM handle_dyn_wind(SCM cons, SCM pt, SCM K, SCM *rguard)
{
  return scm_cons(manage_dyn_wind(SCM_CAR(cons), K, rguard), pt);
}

SCM wind_dynstack(SCM pt, SCM dynstack, SCM K, SCM *rguard)
{
  SCM x[100];
  int i = 0;
  SCM ppt = dynstack;

  while(1)
    {            
      if(pt == ppt)
	{
	finish:
	  while(i > 0)
	    {
	      pt = handle_dyn_wind(x[i - 1], pt, K, rguard);
	      i--;
	    }
	  return pt;
	}
      
      if(i == 100)
	{
	  pt = wind_dynstack(pt, ppt, K, rguard);
	  goto finish;
	}
            
      x[i] = ppt;
      ppt = SCM_CDR(ppt);
      i++;
    }
}

SCM unwind_dynstack_it(SCM pp, SCM *rguard)
{
  gp_debug0("unwind it\n");
  
  SCM* v  = SCM_I_VECTOR_WELTS(pp);
  scm_t_bits id  = SCM_UNPACK(v[D_ID]);

  if(id == D_LGUARD)
    {
      SCM var = v[D_LGUARD_VAR];
      if(v[D_OLD_K] == SCM_BOOL_F)
	*rguard  = rguard_skip(*rguard, var);
      else
	*rguard  = scm_cons(scm_cons(var,v[D_OLD_K]),rguard_skip(*rguard, var));

      gp_debug0("  unwind DLGUARD\n");
      return pp;
    }
	 
  if(id == D_RGUARD)
    {
      gp_debug0("  unwind DRGUARD\n");

      if(v[D_OLD_K] == SCM_BOOL_F)
	*rguard = scm_cons(scm_cons(v[D_RGUARD_VAR], v[D_RGUARD_K]), 
			   *rguard);
      else
	*rguard = scm_cons(scm_cons(v[D_RGUARD_VAR], v[D_RGUARD_K]), 
			   rguard_skip(*rguard, v[D_RGUARD_VAR]));
	
      return pp;
    }

  if(id == D_VARIABLE_GUARD)
    {
      SCM var = v[D_GUARD_VAR];
      gp_debug0(" unwind DGUARD\n");
      if(SCM_CONSP(var))
	v[D_GUARD_VAL] = SCM_CDR(var);
      else if(SCM_VARIABLEP(var))
	{
	  v[D_GUARD_VAL] = SCM_VARIABLE_REF(var);
	}
      else if(SCM_FLUID_P(var))
	{
	  v[D_GUARD_VAL] = scm_fluid_ref(var);
	}      
      else
        {
          v[D_GUARD_VAL] = scm_call_0(var);
        }
      return pp;
    }

  if(id == D_FLUID)
    {
      SCM var  = v[D_FLUID_VAR];
      SCM temp = v[D_FLUID_VAL];
      gp_debug0("  unwind DFLUID\n");
      if(SCM_VARIABLEP(var))
	{
	  v[D_FLUID_VAL] = SCM_VARIABLE_REF(var);
	  SCM_VARIABLE_SET(var, temp);
	}
      else if(SCM_FLUID_P(var))
	{
	  v[D_FLUID_VAL] = scm_fluid_ref(var);
	  scm_fluid_set_x(var, temp);
	}
      else
        {
          v[D_FLUID_VAL] = scm_call_0(var);
          scm_call_1(var, temp);
        }
      return pp;
    }

  return pp;
}
  
SCM unwind_dynstack(SCM end, SCM pt, SCM rguard)
{
  while (pt != end)
    {
      SCM_SETCAR(end, unwind_dynstack_it(SCM_CAR(end), &rguard));
      end = SCM_CDR(end);
    }

  return rguard;
}

void reinstate_dynstack(struct gp_stack *gp, 
			SCM             dynstack, 
			scm_t_bits      dynstack_length, 
			SCM             K,
			SCM             rguard_in)
{
  int n         = gp->dynstack_length;
  int len       = dynstack_length;
  SCM pt        = dynstack;
  SCM gp_pt     = gp->dynstack;
  SCM rguard    = gp->rguards;
  SCM old_gp_pt = gp_pt;

  gp_debug0("0\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
				   gp_pt)) 
	 != n)
       scm_misc_error("reinstate dynstack",
		      "n and gp_pt have not the same length ~a ~a",
		      scm_list_2(n, SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
							  gp_pt))));)
   DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       pt))
	 != dynstack_length)
       scm_misc_error("reinstate dynstack",
		      "dynstack_length and pt have not the same length",
  		      SCM_EOL););
  */
   // Make equal length
  gp_debug0("re> make equal length\n");
  while(dynstack_length != n)
    {
      if(dynstack_length > n)
	{
	  dynstack_length -= 4;
	  pt = SCM_CDR(pt);
	}
      else
	{
	  n -= 4;
	  gp_pt = SCM_CDR(gp_pt);
	}
    }

  // Find common branch point
  gp_debug0("1\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
				   gp_pt)) 
	 != n)
       scm_misc_error("reinstate dynstack",
		      "n and gp_pt have not the same length",
		      SCM_EOL););
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       pt))
	 != dynstack_length)
       scm_misc_error("reinstate dynstack",
		      "dynstack_length and pt have not the same length",
		      SCM_EOL););

  gp_debug2("re> find common branch %d %d\n",n,dynstack_length);
  
  gp_debug2("re> length(gp_pt) = %d length(pt) = %d\n"
	    ,SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
				   gp_pt)) / 4
	    ,SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")),
				   pt)) / 4);
  */  
  while(!scm_is_eq(pt, gp_pt))
    {
      n -= 4;     
      gp_pt = SCM_CDR(gp_pt);
      pt    = SCM_CDR(pt);
    }
  gp_debug0("2\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
				   gp_pt)) 
	 != n)
       scm_misc_error("reinstate dynstack",
		      "2 n and gp_pt have not the same length",
		      SCM_EOL););
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       pt))
	 != n)
       scm_misc_error("reinstate dynstack",
		      "2 n and pt have not the same length",
		      SCM_EOL););
  */
  gp_debug1("re> unwind %d\n", n);

  rguard = unwind_dynstack(old_gp_pt, pt, rguard);
 
  gp_debug0("re> save mark\n");
  
  
  //Reinstate the new state
  gp_debug0("re> wind\n");
  pt = wind_dynstack(pt, dynstack, K, &rguard);

  gp_debug0("3\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       pt))
	 != len)
       scm_misc_error("reinstate dynstack",
		      "3 len and pt have not the same length",
		      SCM_EOL););
  */

  gp->dynstack_length = len;
  gp->dynstack        = pt;
  gp->rguards         = rguard;

  gp_debug0("re> eval rguard\n");
  eval_rguards(rguard_in, K);
  gp_debug0("re> exit\n");
}

//#define DB(X)


SCM make_rguards(SCM rguards, int allp)
{
  gp_debug0("make_rguards\n");
  SCM out = SCM_EOL;
  while(SCM_CONSP(rguards))
    {
      SCM guard = SCM_CAR(rguards);
      SCM k   = SCM_CDR(guard);
      SCM var = SCM_CAR(guard);
      
      SCM val;     
      if(SCM_CONSP(var))
	val = SCM_CDR(var);
      else if(SCM_VARIABLEP(var))
        val = SCM_VARIABLE_REF(var);
      else if(SCM_FLUID_P(var))
	val = scm_fluid_ref(var);
      else
        val = scm_call_0(var);

      out = scm_cons(make_rguard(var,val,(allp ? (SCM_BOOL_T) : k)), out);

      rguards = SCM_CDR(rguards);
    }
  
  gp_debug0("make_rguards end\n");

  if(SCM_CONSP(out))
    return scm_reverse_x (out, SCM_EOL);
  else
    return out;
}

void gp_unwind_dynstack(struct gp_stack *gp, scm_t_bits dyn_n)
{
  SCM        pt = gp->dynstack;
  scm_t_bits n  = gp->dynstack_length;
  
  gp_debug0("4\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       gp->dynstack))
	 != n)
       scm_misc_error("reinstate dynstack",
		      "4 n and pt have not the same length",
		      SCM_EOL););
  */
  while(dyn_n < n)
    {
      pt = SCM_CDR(pt);
      n -= 4;
    }

  SCM rguards = gp->rguards;
  rguards = unwind_dynstack(gp->dynstack, pt, rguards);

  gp->dynstack        = pt;
  gp->rguards         = rguards;
  gp->dynstack_length = n;
  
  gp_debug0("5\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       gp->dynstack))
	 != n)
       scm_misc_error("reinstate dynstack",
		      "4 n and pt have not the same length",
		      SCM_EOL););
  */
  gp_debug0("6\n");
  /*
  DB(if (SCM_UNPACK(scm_call_1(scm_variable_ref(scm_c_lookup ("length")), 
			       gp->dynstack))
	 != n)
       scm_misc_error("reinstate dynstack",
		      "4 n and pt have not the same length",
		      SCM_EOL););
  */
}
//#define DB(X)
