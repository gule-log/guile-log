/*
  TODO: make use of vlists to enable scalable solutions to these algorithms
  TODO: enable cp to use attribute hooks
  TODO: use a list of preserved variables in cp
  TODO: enable functions
  TODO: enable vectors
  TODO: enable closures  
  TODO: enable namespaces
*/

SCM canon_var;
SCM ref_var;
SCM tag_var;
SCM canon(SCM e, SCM s)
{
  SCM map     = scm_c_make_hash_table(32);
  SCM before  = scm_c_make_hash_table(256);
  SCM stack_cdr[100], stack_car[100], conses[100];
  int ncons = 0;
  int mode  = 0, modes[100], nmode = 0, ncar = 0, ncdr = 0;
  scm_t_bits n     = 2, ntag = 2;
 retry:
  e = gp_gp_lookup(e,s);

  if(scm_is_true(gp_pair(e, s)))
    {
      SCM r = scm_hashq_ref(before, e, SCM_BOOL_F);
      if(scm_is_true(r))
	{
	  if(scm_is_eq(r, SCM_BOOL_T))
	    {
	      r = SCM_PACK(ntag);
	      scm_hashq_set_x(before, e, r);
	      ntag += 4;
	    }
	  e = scm_cons(ref_var, r);
	  goto next;
	}
      else
	scm_hashq_set_x(before, e, SCM_BOOL_T);
     
      conses[ncons++] = e;

      modes[nmode++] = mode;
      stack_cdr[ncdr++] = gp_gp_cdr(e,s);      

      mode = 0;
      e = gp_car(e,s);
      goto retry;
    }
  
  if(scm_is_true(gp_varp(e,s)))
    {
      SCM r = scm_hashq_ref(map, e, SCM_BOOL_F);
      if(scm_is_true(r))
	{
	  e = r;
	}
      else
	{
	  SCM r  = scm_cons(canon_var, SCM_PACK(n));
	  n = n + 4;
	  scm_hashq_set_x(map, e, r);
	  e = r;
	}
    }

 next:
  if(mode == 0)
    {
      if(ncdr == 0)
	return e;

      SCM newdata = stack_new[ncdr-1];
      SCM olddata = stack_old[ncdr-1];

      if(SCM_CONSP(newdata))
	{
	  SCM_SETCAR(newdata,e);
	  e = SCM_CDR(olddata);
	  mode = 1;
	  goto retry;
	}
      
      if(SCM_VECTORP(newdata))
	{
	  int i = scm_c_vector_length(newdata);
	  if(i == 1)
	    {
	      scm_c_vector_set_x(newdata, 0, e);
	      ncdr--;
	      e = newdata;
	      mode = modes[ncdr];
	      goto next;
	    }
	  else
	    {
	      scm_c_vector_set_x(newdata, 0, e);
	      mode = 1;
	      e = scm_c_vector_ref(olddata, 1,);
	      goto retry;
	    }
	}
    }

  if(mode == 1)
    {
      SCM r = conses[--ncons];
      r = scm_hashq_ref(before, r, SCM_BOOL_F);
      e = scm_cons(stack_car[--ncar], e);
      
      if(scm_is_true(r) && !scm_is_eq(r, SCM_BOOL_T))
	e = scm_cons(tag_var, scm_cons(r, e));

      mode = modes[--nmode];
      goto next;
    }

  return SCM_BOOL_F;
}
//#define DB(X) X
SCM uncanon(SCM e, SCM s)
{
  SCM refs[100], variables[100], varref[100], stack_car[100], stack_cdr[100];
  int nvar = 0, nv=0, nmode=0, modes[100], mode=0, ncdr=0, ncar=0;
 retry:
  e = gp_gp_lookup(e , s);
  gp_format1("e: ~a~%", e);
  if(SCM_CONSP(e))
    {
      SCM tag = SCM_CAR(e);
      gp_format2("tag: ~a ~a~%", tag, ref_var);
      gp_debug1("%d\n",scm_is_eq(tag, ref_var));
      if(scm_is_eq(tag, tag_var))
	{	  
	  SCM cdr = SCM_CDR(e);
	  int i = SCM_UNPACK(SCM_CAR(cdr)) >> 2;
	  refs[i] = scm_make_variable(SCM_BOOL_F);	  
	  variables[nvar++] = refs[i];
	  e = SCM_CDR(cdr);
	}
      else if(scm_is_eq(tag, ref_var))
	{
	  int i = SCM_UNPACK(SCM_CDR(e)) >> 2;
	  gp_debug0("ref\n");
	  e = refs[i];
	  goto next;
	}
      else if(scm_is_eq(tag, canon_var))
	{
	  int i = SCM_UNPACK(SCM_CDR(e)) >> 2;
	  if(i >= nv)
	    {
	      int j;
	      for(j  = nv; j <= i; j++)
		varref[j] = SCM_BOOL_F;
	      nv = i + 1;
	    }
	  if(scm_is_false(varref[i]))
	    varref[i] = gp_make_variable();
	  
	  gp_format2("varref[~a] = ~a~%",SCM_CDR(e),varref[i]);

	  e = varref[i];
	  goto next;
	}
      else
	variables[nvar++] = SCM_BOOL_F;

      gp_debug0("cons\n");

      modes[nmode++] = mode;
      stack_cdr[ncdr++]  = SCM_CDR(e);

      mode = 0;
      e = SCM_CAR(e);

      goto retry;
    }
    
next:
  if(mode == 0)
    {
      if(ncdr == 0)
	return e;

      stack_car[ncar++] = e;
      e = stack_cdr[--ncdr];
      mode = 1;
      goto retry;
    }

  if(mode == 1)
    {
      e = scm_cons(stack_car[--ncar], e);
      SCM r = variables[--nvar];      
      if(scm_is_true(r))
	{
	  scm_variable_set_x(r, e);
	  e = r;
	}

      mode = modes[--nmode];
      goto next;
    }

  return SCM_BOOL_F;
}
//#define DB(X)

SCM cp(SCM e, SCM s, int isPure)
{
  SCM map     = scm_c_make_hash_table(32);
  SCM before  = scm_c_make_hash_table(256);
  SCM stack_cdr[100], stack_car[100];
  int stack_mode[100];
  int mode  = 0, nmode = 0, ncar = 0, ncdr = 0, nconses = 0;
  SCM conses[100];
 retry:
  e = gp_gp_lookup(e,s);

  if(scm_is_true(gp_pair(e, s)))
    {
      SCM r = scm_hashq_ref(before, e, SCM_BOOL_F);      
      if(scm_is_true(r))
	{
	  if(scm_is_eq(r, SCM_BOOL_T))
	    {
	      r = scm_make_variable(SCM_BOOL_F);
	      scm_hashq_set_x(before, e, r);
	    }
	  e = r;
	  goto next;
	}
      else
	scm_hashq_set_x(before, e, SCM_BOOL_T);
      
      conses[nconses++] = e;
      
      stack_mode[nmode++] = mode;
      stack_cdr[ncdr++] = gp_gp_cdr(e,s);      

      mode = 0;
      e = gp_car(e,s);
      goto retry;
    }
  
  if(scm_is_true(gp_varp(e,s)))
    {
      SCM r = scm_hashq_ref(map, e, SCM_BOOL_F);
      if(scm_is_true(r))
	{
	  e = r;
	}
      else
	{
	  r = gp_make_variable();
	  scm_hashq_set_x(map, e, r);
	  e = r;
	}
    }

 next:
  if(mode == 0)
    {
      if(ncdr == 0)
	return e;
      
      stack_car[ncar++] = e;
      e = stack_cdr[--ncdr];
      mode = 1;
      goto retry;
    }
  
  if(mode == 1)
    {
      SCM r = conses[--nconses];
      SCM car = stack_car[--ncar];
      SCM cdr = e;
      if(!isPure && scm_is_eq(car, SCM_CAR(r))  &&  scm_is_eq(cdr, SCM_CDR(r)))
	{
	  e = r;
	  mode = stack_mode[--nmode];
	  goto next;
	}

      r = scm_hashq_ref(before, r, SCM_BOOL_F);
      if(scm_is_true(r) && !scm_is_eq(r, SCM_BOOL_T))
	{
	  e = r;
	  scm_variable_set_x(r, scm_cons(car,cdr));
	  mode = stack_mode[--nmode];
	  goto next;
	}
      
      e = scm_cons(car,cdr);
      mode = stack_mode[--nmode];
      goto next;
    }

  return SCM_BOOL_F;
}

SCM_DEFINE(gp_canon, "gp-canon", 2, 0, 0, 
           (SCM e, SCM s), "")
#define FUNC_NAME s_gp_canon
{
  return canon(e,s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_uncanon, "gp-un-canon", 2, 0, 0, 
           (SCM e, SCM s), "")
#define FUNC_NAME s_gp_uncanon
{
  return uncanon(e,s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_cp, "gp-cp", 2, 0, 0, 
           (SCM e, SCM s), "")
#define FUNC_NAME s_gp_cp
{
  return cp(e,s,1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_canon_vars, "gp-get-canon-vars", 0, 0, 0, 
           (), "")
#define FUNC_NAME s_gp_get_canon_vars
{
  return scm_list_3(canon_var, ref_var, tag_var);
}
#undef FUNC_NAME


void init_canon()
{
  canon_var = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  ref_var   = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  tag_var   = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
}
