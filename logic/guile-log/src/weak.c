#define NWTHREADS 256

int nweak_thread_keys[NWTHREADS];
int Nweak_thread_keys[NWTHREADS];
SCM weak_thread_keys[NWTHREADS];


SCM inline make_pt_vector(int len)
{
  return scm_c_make_weak_vector(len, SCM_BOOL_F);
}

void clear_weak_keys()
{
  int i;

  for(i=0; i < NWTHREADS; i++)
    {
      int j;
      if(nweak_thread_keys[i] > 0)
	{
	  SCM *vt = SCM_I_VECTOR_WELTS(weak_thread_keys[i]);

	  for(j = 0; j < nweak_thread_keys[i]; j++)
	    {
	      SCM q = vt[j];
	      if(q && scm_is_true(q))
		{
		  SCM *id = GP_GETREF(SCM_CAR(q));	      
		  id[0] = SCM_PACK(GP_SETCOUNT(id[0],(scm_t_bits) 0));
		}
	    }
	}
    }
}

void prepare_weak_keys()
{
  int i;

  for(i=0; i < NWTHREADS; i++)
    {
      if(nweak_thread_keys[i] > 0)
	{
	  SCM *vt = SCM_I_VECTOR_WELTS(weak_thread_keys[i]);
	  int j;

	  for(j = 0; j < nweak_thread_keys[i]; j++)
	    {
	      SCM q = vt[j];

	      if(q && scm_is_true(q))
		{
		  SCM id = SCM_CAR(q);
		  scm_t_bits head = SCM_UNPACK(GP_GETREF(id)[0]);
		  GP_GC_CLEAR(head);
		  GP_GETREF(id)[0] = SCM_PACK(head);
		}
	    }
	}
    }
}

void register_weak_keys()
{
  int i;

  for(i=0; i < NWTHREADS; i++)
    {
      scm_t_bits j;
      if(nweak_thread_keys[i] > 0)
	{
	  SCM *vt = SCM_I_VECTOR_WELTS(weak_thread_keys[i]);

	  for(j = (scm_t_bits) 0; j < nweak_thread_keys[i]; j++)
	    {
	      SCM q = vt[j];

	      if(q && scm_is_true(q))
		{
		  SCM *id = GP_GETREF(SCM_CAR(q));	      
		  id[0] = SCM_PACK(GP_SETCOUNT(id[0], j));
		}
	    }
	}
    }
}

void register_weak_key_pair(SCM id, SCM v)
{
  scm_t_bits thid = GP_ID(id);
  if(Nweak_thread_keys[thid]==0)
    {
      weak_thread_keys[thid] = make_pt_vector(1024);
      Nweak_thread_keys[thid] = 1024;
    }

  if(Nweak_thread_keys[thid]==nweak_thread_keys[thid])
    {
      int i = Nweak_thread_keys[thid] * 2;
      SCM vold = weak_thread_keys[thid];
      weak_thread_keys[thid] = make_pt_vector(i);
      Nweak_thread_keys[thid]=i;
      {
	int j;
	SCM vt = weak_thread_keys[thid];
	for(j = nweak_thread_keys[thid] - 1; j >=0; j--)
	  scm_c_weak_vector_set_x(vt,j,scm_c_weak_vector_ref(vold,j));
      }
    }
  
  if(nweak_thread_keys[thid] == 0) nweak_thread_keys[thid]++;

  
  {
    SCM vec = weak_thread_keys[thid];
    scm_t_bits i = GP_COUNT(id);   
    if(i > 0 && scm_is_true(scm_c_weak_vector_ref(vec,i)))
      {
	SCM q = scm_c_weak_vector_ref(vec,i); 
	SCM_SETCDR(q, scm_cons(v,SCM_CDR(q)));
      }
    else
      {
	SCM pair = scm_cons(id, v);
	scm_c_weak_vector_set_x(vec, nweak_thread_keys[thid]++, pair); 
      }
  }
}

void scan_weak_lists()
{
  int i;
  for(i=0; i < NWTHREADS; i++)
    {
      int j;
      if(nweak_thread_keys[i] > 0)
	{
	  SCM vt    = weak_thread_keys[i];
	  int n     = nweak_thread_keys[i];
	  int nlive = 0;
	  for(j = 0; j < n; j++)
	    {
	      SCM q = scm_c_weak_vector_ref(vt, j);
	      if(scm_is_true(q))
		{
		  nlive++;
		}	    
	    }
	  if(nlive > 1000 && nlive*3 < n)
	    {
	      int ins = 0;
	      for(j = 0; j < n; j++)
		{
		  SCM q = scm_c_weak_vector_ref(vt, j);
		  if(scm_is_true(q))
		    {
		      ins ++;
		      if(ins < j)
			{
			  scm_c_weak_vector_set_x(vt, ins, q);
			  scm_c_weak_vector_set_x(vt, j  , SCM_BOOL_F);
			}
		    }		  		    
		}
	      nweak_thread_keys[i] = nlive;
	    }
	}
    }
}

SCM_DEFINE(gp_make_ephermal_pair, "gp-make-ephermal-pair", 2, 0, 0, 
	   (SCM id, SCM v), "")
#define FUNC_NAME s_gp_make_ephermal_pair
{
  if(GP(id))
    register_weak_key_pair(id, v);  
   else
     scm_misc_error("gp-make-ephermal-pair", "arg 1 is not a gp variable", 
		    SCM_EOL);
   return SCM_UNSPECIFIED;
}
#undef FUNC_NAME      

void init_weak()
{
  int i;
  for(i = 0; i < NWTHREADS; i++)
    {
      weak_thread_keys[i]  = SCM_BOOL_F;
      nweak_thread_keys[i] = 0;
      Nweak_thread_keys[i] = 0;
    }
}

