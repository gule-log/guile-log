 /* FIXME: In Guile 3.2, replace this union with just a "size" member.
     Digits are always allocated inline.  */
 

struct my_bignum1
{
  scm_t_bits tag;
  union {
    mpz_t mpz;
    struct {
      int zero;
      int size;
      mp_limb_t *limbs;
    } z;
  } u;
  mp_limb_t limbs[];
};


#define MY_I_BIG_MPZ0(x) (*((mpz_t *) (SCM_CELL_OBJECT_LOC((x),1))))
#define MY_I_BIG_MPZ1(x) (((struct my_bignum1 *)x)->u.mpz)

/* guile < 3.0.8, this may change when we reach 3.2 */
#define MY_I_BIG_MPZ(x)  MY_I_BIG_MPZ0(x)
