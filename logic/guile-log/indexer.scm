;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log indexer)
  #:use-module (logic guile-log code-load)
  #:re-export (get-index-set get-index-test get-index-tags))
