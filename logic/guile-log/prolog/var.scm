;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log prolog var)
  #:use-module (system base language)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog goal)
  #:use-module ((logic guile-log umatch) #:select (*current-stack*))
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog operators)
  #:use-module (logic guile-log prolog symbols)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log prolog modules)
  #:use-module ((logic guile-log slask) #:select
                (get-double-quote-flag-fkn compile-lambda))
  #:use-module ((logic guile-log umatch)
                #:select (gp-newframe gp-unwind gp-make-var))
  #:use-module (ice-9 eval-string)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module ((logic guile-log) 
		#:select ((<_> . GL:_) <define> <lambda> ->list-mac
			  <code> <cc> <lookup> CUT S))
  #:export (arg pat-match var term term-soft term-init-variables v-variables
                term-get-variables-list term-get-variables
		terms-vars terms-vars-sing))

(define scmlang (lookup-language 'scheme))
(define (eval-string0 x)
  (with-fluids ((*current-language* scmlang))
    (eval-string x)))
  
(define do-print #t)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define (metah lam  n . l)
  (if (null? l)
      (let ((f (hashq-ref (fluid-ref *closure-creations*) n #f)))
	(if f
	    f
	    (error "closure references failed could not find stored ref of already made closure")))
      (begin
	(let ((f (apply lam l)))
	  (hashq-set! (fluid-ref *closure-creations*) n f)
	  f))))

(define arg #f)
(define arg-goal #f)
(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define uq (list #'CUT))

(define v-variables (make-fluid '()))

(define (@wrapper v li ?)
  (cond
   ((eq? li #:current-module)
    (make-namespace v (map
		       (lambda (x) (if (symbol? x) (symbol->string x) x))
		       (module-name (current-module)))
		    ? #t))
   (else
    (make-namespace v li ? #t))))


(define *variables* (make-fluid (make-hash-table)))
(define *var-list*  (make-fluid '()))

(define (term-get-variables)
  (define h (make-hash-table))
  (let lp ((l (reverse (fluid-ref *var-list*))))
    (if (pair? l)
        (if (hash-ref h (car l) #f)
            (lp (cdr l))
            (begin
              (hash-set! h (car l) #t)
              (cons (car l) (lp (cdr l)))))
        '())))

(define (term-get-variables-list)
  (reverse (fluid-ref *var-list*)))

(define (term-init-variables)
  (set! terms-vars      (make-hash-table))
  (set! terms-vars-sing (make-hash-table))
  (fluid-set! *var-list*   '())
  (fluid-set! *variables* (make-hash-table)))

(define (eval-scheme f) (f))
(<define> (do-eval-scheme-str s)
  (<code> (eval-string0 s)))

(<define> (do-eval-scheme   f) (<code> (f S)))
(<define> (when-eval-scheme f) (if (f S) <cc>))
(<define> (when-eval-scheme-str s)
  (when (eval-string0 s)
    <cc>))

(define found-scm (make-fluid #f))

(define (medler . s)
  (match s
    ((f)   (f (fluid-ref *current-stack*)))
    ((s f) (f s))))

(set-procedure-property! medler 'prolog-functor-type #:scm)

(define (guile_eval . s)
  (define (f str s)
    (let ((xx (string-append
	       "(scm-lambda (ss) (scm-syntax-parameterize ((S (scm-lambda z #'ss))) " str "))")))
      ((eval-string0 xx) s)))

  (match s
    ((str)   (f str (fluid-ref *current-stack*)))
    ((s str) (f str s))))

(set-procedure-property! guile_eval 'prolog-functor-type #:scm)

(define (mk-scheme-soft goal? stx -eval- s l)
  (case s
    ((do)       
     (vector (list do-eval-scheme-str l)))

    ((when)
     (vector (list when-eval-scheme-str l)))

    ((v var)
     (let ((v (string->symbol l)))
       (fluid-set! v-variables (cons v (fluid-ref v-variables)))
       (eval-string0 l)))

    ((e eval)
     (vector (list guile_eval l)))
      
    ((s scm)
     ((eval-string0 (format #f "((@ (guile) lambda) (ss) ((@ (guile) syntax-parameterize) ((S ((@ (guile) lambda) z #'ss))) ~a))" l))
      (fluid-ref (@ (logic guile-log umatch) *current-stack*))))))
			       

(define (mk-scheme goal? stx -eval- s l)
  (let* ((e   (with-input-from-string l read))
         (w   (datum->syntax stx e))
         (lam (lambda (x) x))
         (v   #`(lambda (s)
                  (syntax-parameterize ((S (identifier-syntax s)))
                      #,w))))
    (case s
      ((do)
       (if goal?
	   #`(do-eval-scheme #,v)
	   (lam #`(vector (list (@@ (logic guile-log prolog var) do-eval-scheme)
				#,v)))))
      ((when)
       (if goal?
	   #`(when-eval-scheme #,v)
	   (lam #`(vector (list (@@ (logic guile-log prolog var)
				    when-eval-scheme)
				#,v)))))
      ((v var)
       (fluid-set! v-variables (cons e (fluid-ref v-variables)))
       (lam w))
      
      ((s scm)
       (if goal?
	   #`(do-eval-scheme #,v)
	   #`(medler
	      (lambda (ss)
		(syntax-parameterize ((S (lambda z #'ss)))
				     #,w))))))))

(define (mk-map -var- x_x -list- -eval- -term- -*term- -termvar- -*termvar-
		-vector- add-sym f->stxfkn mk-scheme compile-lambda)
  
  (let ((hmap (make-hash-table)))
    (hash-set! hmap #:group
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((x)  (fget x)))))

    (hash-set! hmap #:variable
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   (('_  . _)  (x_x))
		   ((v   . _)  (-var- stx v)))))

    (hash-set! hmap #:list
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((()  . _)   (-list- '()))
		   ((v   . _)   (-list- (get-c fget v))))))

    (hash-set! hmap #:listl
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((()  . _)   (-list- '()))
		   ((v   . _)   (-list- (map fget v))))))
    

    (hash-set! hmap #:keyword
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((str . _)  (-eval- str)))))

    (hash-set! hmap #:string
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((str . _)
		    (-eval- ((get-double-quote-flag-fkn) str))))))

    (hash-set! hmap #:fknfkn
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   (((#:group a) (#:group b))    
		    (let* ((a (fget a))
			   (b (fget `(#:term (#:atom write #f #f 0 0)
					     ,b #f 0 0))))
		      (match b
		        (#((_ . l))
			 (vector (cons a l)))
			((q (vector (li w . l)))
			 (list q (list vector (cons* li  #``#,a  l))))))))))

    (hash-set! hmap #:scm-term
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   (((#:atom s . _) l _ _)
		    (fluid-set! found-scm #t)
		    (mk-scheme goal? stx -eval- s l)))))

    (hash-set! hmap #:@
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((v type li)
		    (set! mod 1)
		    (if (eq? type '@@) 
			(set! local? #t)
			(set! local? #f))
		    (let ((li 
			   (if (keyword? li)
			       li
			       (map
				(lambda (x)
				  (if (symbol? x)
				      (symbol->string x)
				      x))
				(if (vector? li)
				    (vector->list li)
				    (map (lambda (x)
					   (match x
					     ((x) (list-ref x 1))
					     (x   (list-ref x 1))))
					 (get.. "," li)))))))
		      (case type
			((@)  (-eval- #`(@wrapper `#,(fget v) '#,li #f)))
			((@@) (-eval- #`(@wrapper `#,(fget v) '#,li #t)))))))))

    (hash-set! hmap #:lam-term
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   (((or (and s (#:atom . _)) (and #f s)) l closed? _ _)
		    (if s
			(-eval- (compile-lambda stx l s closed?))
			(if l
			    (if (eq? l 'null)
				(-vector- #:brace #:null)
				(-vector- #:brace (arg stx l)))
			    (-vector- #:brace)))))))


    (hash-set! hmap #:term
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   (((and atom (#:atom f _ _ n m)) . uu)
		    (match uu
		      ((() #f . _) 
		       (add-sym mod local? atom)
		       (-eval-
			(car (f->stxfkn #f mod f local?
					atom arg goal? stx #f n m '() '()))))

		      ((() meta . _)
		       (add-sym mod local? atom)
		       (let ((meta (map fget (get.. "," meta))))
			 (-eval- 
			  #`(metah #,@(f->stxfkn #f mod f local?
						 atom arg goal? stx #f n m '())
				   #,@meta))))

		      ((((_ _ "|" _) a b _ _) #f . _)
		       (add-sym mod local? atom)
		       (-*term-
			(f->stxfkn #f mod f local? atom arg goal? stx #f n m 
				   (get.. "," a) b)))

		      ((((_ _ "|" _) y _ _)  #f . _)
		       (add-sym mod local? atom)
		       (-*term-
			(f->stxfkn #f mod f local? atom arg goal? stx #f n m 
				   '() y)))

		      ((x  #f . _) 
		       (add-sym mod local? atom)
		       (-term-
			(f->stxfkn #f mod f local? atom arg goal? stx #f n m
				   (get.. "," x))))
				   
    
		      ((x  meta . _) 
		       (add-sym mod local? atom)
		       (let ((meta (map fget (get.. "," meta))))
			 (-term-
			  #`((metah #,@(f->stxfkn #f mod f local?
						  atom arg goal? stx #f n m '())
				    #,@meta)
			     #,@(map fget (get.. "," x)))))))))))

    (hash-set! hmap #:termvar
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((v _ . uu)
		    (match uu
		      ((() . _) 
		       (-var- stx v))

		      ((((_ _ "|" _) x y _ _)  . _)
		       (-*termvar-
			(cons*
			 (-var- stx v)
			 (map fget (get.. "," x))
			 (list (fget y)))))

		      ((((_ _ "|" _) y _ _)  . _)
		       (-*termvar-
			(list
			 (-var- stx v)
			 (fget y))))
		      
		      ((x . _)
		       (-termvar- 
			(cons (-var- stx v)
			      (map fget
				   (get.. "," x))))))))))
    
    (hash-set! hmap #:atom
	       (lambda (x stx fget arg local? mod goal?)
		 (match x
		   ((and atom (#:atom f _ _ n m))
		    (add-sym mod local? atom)
		    (-eval- (car (f->stxfkn #f mod f local?
					    atom arg goal? stx #f n m 
					    '())))))))


    (hash-set! hmap #:termstring
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)
		   ((x l . _)
		    (if goal?
			((hash-ref hmap #:term)
			 (list
			  (list (string->symbol (cadr x)) '@@ #f 0 0)
			  l #f)
			 stx fget arg local? mod goal?)
			
			(let ((ll (match l
				     (((_ _ "|" _) x y _ _)
				      (append (map fget (get.. "," x))
					      (list (fget y))))
				     (((_ _ "|" _) y _ _)
				      (list (fget y)))
				     (_
				      (append (map fget (get.. "," l))
					      (list '()))))))
			  (-term- #:str (cons (cadr x) ll))))))))

    (hash-set! hmap #:number
	       (lambda (x stx fget arg local? mod goal?)
		 (match (cdr x)	
		   ((n . _) 
		    (-eval- n)))))

    hmap))

(define (mk-mk-arg -var- x_x -list- -eval- -term- -*term-
		   -termvar- -*termvar- -vector- add-sym f->stxfkn
		   mk-scheme compile-lambda)
  (let ((hmap (mk-map -var- x_x -list- -eval- -term- -*term- -termvar-
		      -*termvar- -vector- add-sym f->stxfkn
		      mk-scheme compile-lambda)))
  (define mk-arg
    (lambda (goal?)
      (letrec
       ((arg      (lambda x (apply (mk-arg #f) x)))
	(arg-goal (lambda x (apply (mk-arg #t) x)))
	(arg0
	 (lambda* (stx x #:optional
		         (mod #f)
			 (local? #f)
			 #:key
			 (first? #t))
	   (define (fget  x . l) (apply arg      stx x mod local? l))
	   (define (fgoal x) (arg-goal stx x mod local?))

	   (match (pp 'mk-arg x)
	    (()                   '())
	    ((x) (fget x))
	    ((a . _)
	     (if (keyword? a)
		 ((hash-ref hmap a) x stx fget arg local? mod goal?)
		 (match x
		   (((_ _ "-" _) (#:number n _ _) _ _)
		    (- n))
	    
		   ((or ((tp _ op _) x y n m)	 
			(#:termstring (#:string (and tp op) _ _) (x y) n m))
		    (add-sym mod local? `(#:atom ,(string->symbol op)
						 #f #f ,n ,m))
		    (-term-
		     (f->stxfkn #f #f op #f #f arg #f stx 2 n m (list x y))))
	    
		   ((or ((tp _ op _) x n m)
			(#:termstring (#:string (and tp op) _ _) (x) n m))

		    (add-sym mod local? `(#:atom ,(string->symbol op)
						 #f #f ,n ,m))
		    (-term- 
		     (f->stxfkn #f #f op #f #f arg #f
				stx 1 n m (list x)))))))))))
	    	    
       arg0)))

  mk-arg))

;----------------------------------
(define (termer -term- -tstr-)
  (lambda (x . l)
    (if (eq? x #:str)
	(apply -tstr- l)
	(-term- x))))

(define (tstr x)
  (syntax-case x ()
    ((a b ... c) #',(vector (cons* a `b ... `c)))))

(define (var-arg  stx x)  #`,#,(datum->syntax stx x))
(define x_x           (lambda () #',GL:_))
(define (eval-arg x)  #`,#,x)
(define (list-arg x)  x)
(define (term-arg- x)
   #`(unquote (vector (list
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))

(define term-arg (termer term-arg- tstr))

(define (*term-arg x)
  (pp 'arg (match x
        ((f a ... b)
	 #`(unquote
	    (vector (cons*
		     #,f
		     #,@(map (lambda (x) #``#,x) a)
		     (->list-mac S `#,b))))))))

(define (termvar-arg x)
  (pp
   'arg
   #`(unquote (vector (list
		       `#,(car x)
		       #,@(map (lambda (x) #``#,x)
			       (cdr x)))))))

(define (*termvar-arg x)
  (pp
   'arg
   (match x
	  ((f a ... b)
	   #`(unquote (vector (cons*
			       `#,f
			       #,@(map (lambda (x) #``#,x)
				       a)
			       (->list-mac S `#,b))))))))
(define vec-arg  
  (case-lambda
   ((tag x)
    #`,(vector #,tag `#,x))
   ((tag)
    #`,(vector #,tag))))

(define (Ds f) (lambda x (apply f x)))
(define mk-arg
  (mk-mk-arg var-arg x_x list-arg eval-arg term-arg *term-arg 
	     termvar-arg *termvar-arg vec-arg add-sym f->stxfkn
	     mk-scheme compile-lambda))

(define arg      (mk-arg #f))
(define arg-goal (mk-arg #t))
; --------------------------------------------------

(define (var-match  stx x)  (datum->syntax stx x))
(define x_match         (lambda () #'_))
(define (eval-match x)    #`,#,x)
(define (list-match x)    x)
(define (term-match- x)
  #`#((,#,(car x)
       #,@(map (lambda (x) x)
	       (cdr x)))))

(define (tstr-match x)
  (syntax-case x ()
    ((a b ... c) #'#((a b ... . c)))))

(define term-match
  (lambda x
    (pp 'term-match
	 (apply
	  (termer term-match- tstr-match)
	  x))))

(define (*term-match x)
  (pp 'match
      #`#((,#,(car x)
	   #,@(map (lambda (x) x) (butlast (cdr x)))
	   . #,(last x)))))

(define (varterm-match x)
  (pp
   'match
   #`#((#,(car x)
	#,@(map (lambda (x) x)
		(cdr x))))))

(define (butlast x) (reverse (cdr (reverse x))))
(define (last    x) (car (reverse x)))

(define (*varterm-match x)
  (pp 'match
       #`#((#,(car x)
	    #,@(map (lambda (x) x)
		    (butlast (cdr x)))
	    .  #,(last x)))))

(define vec-match  
  (case-lambda
   ((tag x)
    #`#(#,tag #,x))
   ((tag)
    #`#(#,tag))))

(define mk-match
  (mk-mk-arg var-match x_match list-match eval-match 
	     term-match    *term-match
	     varterm-match *varterm-match
	     vec-match add-sym f->stxfkn
	     mk-scheme compile-lambda))

(define pat-match      (mk-match #f))
(define pat-match-goal (mk-match #t))

; ------------------------------------------------------

(define (var-var stx x)  #`,#,(datum->syntax stx x))
(define x_var         (lambda () #`,GL:_))
(define (eval-var x)    #`,#,x)
(define (list-var x)    x)
(define (term-var- x)
  #`,(vector (list #,(car x)
		   #,@(map (lambda (x) #``#,x)
			   (cdr x)))))
(define term-var (termer term-var- tstr))
(define (*term-var x)
  (pp 'var (match x
    ((f a ... b)
     #`,(vector (cons* #,f
		       #,@(map (lambda (x) #``#,x)
			       a)
		       (->list-mac S `#,b)))))))
		       
(define (termvar-var x)
  (pp 'var (match x
      ((f a ...)
       #`,(vector (list `#,(car x)
			#,@(map (lambda (x) #``#,x)
				(cdr x))))))))
(define (*termvar-var x)
  (pp 'var (match x
	     ((f a ... b)
	      #`,(vector (cons*  `#,f
				 #,@(map (lambda (x) #``#,x)
					 a)
				 (->list-mac S `#,b)))))))
(define vec-var  
  (case-lambda 
   ((tag x)
    #`,(vector #,tag `#,x))
   ((tag)
    #`,(vector #,tag))))


(define mk-var
  (mk-mk-arg var-var x_var list-var eval-var term-var *term-var 
	     termvar-var *termvar-var vec-var add-sym f->stxfkn
	     mk-scheme compile-lambda))


(define var       (mk-var #f))
(define var-goal  (mk-var #t))

; --------------------------------------------------------------
(define (var-term stx v) 
  (fluid-set! *var-list* (cons v (fluid-ref *var-list*)))
  (hashq-set! (fluid-ref *variables*) v #t)
  #`,#,(datum->syntax stx v))

(define x_term         (lambda () #',(gp-make-var)))
(define (eval-term x)  #`,#,x)
(define (list-term x)  x)
(define (term-term- x)
  (pp 'term-term
       (if (fluid-ref is-macro?)
	   (pp 'term-term-mac x)
	   #`(unquote (vector (list
			       #,(car x)
			       #,@(map (lambda (x) #``#,x)
				       (cdr x))))))))
(define term-term (termer term-term- tstr))
(define (*term-term x)
  (match x
    ((f a ... b)
     (pp '*term
	 #`(unquote
	    (vector
	     (cons*
	      #,f
	      #,@(map (lambda (x) #``#,x)
		       a)
	      (->list-mac S `#,b))))))))

(define (termvar-term x)
  (pp 'termvar
    #`(unquote (vector (list
			`#,(car x)
			#,@(map (lambda (x) #``#,x)
				(cdr x)))))))

(define (*termvar-term x)
  (pp '*termvar
    (match x
      ((f a ... b)
       #`(unquote (vector (cons*
			   `#,f
			   #,@(map (lambda (x) #``#,x)
				   a)
			   (->list-mac S `#,b))))))))

(define vec-term 
  (case-lambda
   ((tag x)
    #`,(vector #,tag `#,x))
   ((tag)
    #`,(vector #,tag))))

(define mk-term
  (mk-mk-arg var-term x_term list-term eval-term term-term  *term-term 
	     termvar-term *termvar-term vec-term add-sym f->stxfkn
	     mk-scheme compile-lambda))


(define term       (mk-term #f))
(define term-goal  (mk-term #t))


(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))
(define terms-vars      (make-hash-table))
(define terms-vars-sing (make-hash-table))
(define (mkvar v)
  (if (or (symbol? v) (string? v))      
      (aif it (hashq-ref terms-vars v)
	   (begin
	     (hashq-set! terms-vars-sing it #f)
	     it)
	   (let ((V (gp-make-var)))
	     (hashq-set! terms-vars v V)
	     (hashq-set! terms-vars-sing V #t)
	     V))
      v))

(define (var-terms stx x)
  (let* ((v x)
	 (V (mkvar v)))
    V))

(define x_terms gp-make-var)
(define (eval-terms x)
  (if (and (pair? x) (module? (car x)))
      (sym-ref x)
      x))

(define (list-terms x)  x)
(define (term-terms- x)
  (match x
    ((f a ...)
     (vector (cons (sym-ref f) a)))))

(define (tstr-terms x)
  (vector (apply cons* x)))
   
(define term-terms (termer term-terms- tstr-terms))

(define (*term-terms x)
  (match x
    ((f a ...)
     (vector
      (cons (sym-ref f)
	    (let lp ((x a))
	      (match x
	       ((x)
		x)
	       ((x . l)
		(cons x (lp l))))))))))

(define (termvar-terms x)
  (vector (cons (mkvar (car x)) (cdr x))))

(define (*termvar-terms x)
  (vector
   (cons
    (mkvar (car x))    
    (let lp ((x (cdr x)))
      (match x
        ((x)
	 x)
	((x . l)
	 (cons x (lp l))))))))
  

(define vec-terms 
  (case-lambda
   ((tag x)
    (vector tag x))
   ((tag)
    (vector tag))))

(define (compile-lambda-soft stx l s closed?)
  (let ((x (match l
		  (((_ _ "," _) . _)
		   (get.. "," l))
		  (_  (list l)))))
    
    (vector (term-soft stx s)
	    (let ((xx (map (lambda (x)
			     (term-soft stx x))
			   
			   (if (or (null? x) (equal? l ""))
			       '()
			       x))))
	      (if closed?
		  (list (vector #f xx))
		  xx)))))

(define mk-term-soft
  (mk-mk-arg var-terms x_terms list-terms eval-terms term-terms  *term-terms 
	     termvar-terms *termvar-terms vec-terms terms-add-sym
	     f->stxfkn-soft mk-scheme-soft compile-lambda-soft))


(define term-soft      (mk-term-soft #f))
(define term-soft-goal (mk-term-soft #t))

(set! (@@ (logic guile-log slask) arg) arg)
(set! (@@ (logic guile-log slask) term) term)
(set! (@@ (logic guile-log slask) term-init-variables)
      term-init-variables)
(set! (@@ (logic guile-log slask) term-get-variables)
      term-get-variables)
(set! (@@ (logic guile-log slask) found-scm)
      found-scm)

(set! (@@ (logic guile-log prolog goal) term-goal) term-goal)
(set! (@@ (logic guile-log prolog goal) term     ) term)
