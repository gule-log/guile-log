;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log prolog symbols)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog operators)
  #:export (clear-syms add-sym terms-add-sym sym-ref
		       rem-sym get-syms rem-syms add_sym with-syms))

(define (get... x)
  (match x
    (((_ _ "," _) . l)
     (get.. "," x))
    (_ x)))

(<define> (add_sym f)
   (<code> (add-sym #f #f
		    (list #:atom (procedure-name (<lookup> f)) #f #f 0 0))))

(define syms (make-fluid vlist-null))
(define (clear-syms)
  (fluid-set! syms vlist-null))

(define-syntax-rule (with-syms code ...)
  (with-fluids ((syms (fluid-ref syms)))
    code ...))

;(define logfile (open-file "log" "w"))
(define (add-sym mod local? sym)
  ;(format logfile "adding symbol ~a~%" sym)
  (when (not mod)
  (match sym
    ((#:atom f #f #f n m)
     (fluid-set! syms (vhash-consq f #t (fluid-ref syms))))
    ((#:atom f amp (and atom ((or #:string #:atom) a . _)) n m)
     (add-sym mod local? `(#:atom ,f ,amp (,atom) ,n ,m)))
    ((#:atom f amp l n m)
     (let ((l (map
               (lambda (x)
                 (match x
                   ((#:atom a _ _ n m) a)
                   ((#:string a . _)   (string->symbol a))
		   (l l)
                   (_                  
		    (error (format #f "wrong @ argument (~a) in ~a" 
				   x (get-refstr n m))))))
               (get... l))))
       (let ((mod (resolve-module l)))
         (cond
          ((eq? (current-module) (module-ref mod f))
           (fluid-set! syms (vhash-consq f #t (fluid-ref syms))))))))

    ((#:atom f amp (#:atom f . _) n m)
     (let ((mod (resolve-module `(language prolog prolog-modules ,f))))
       (cond
        ((eq? (current-module) (module-ref mod f))
         (fluid-set! syms (vhash-consq f #t (fluid-ref syms)))))))
    
    (s (if (symbol? s)
           (fluid-set! syms (vhash-consq s
					 #t (fluid-ref syms))))))))

(define make-unbound-fkn #f)
 
(define (handle x module)
  (let ((mod (if (module? module) module (current-module))))
    (unless (module-defined? mod x)
      (let ((f (make-unbound-fkn x)))
	(module-define! mod x f)
	(set-procedure-property! f 'module (module-name mod))
	(set-procedure-property! f 'shallow #t)
	(set-procedure-property! f 'name x)))))

(define (handle-ref x module)
  (let ((mod (cond
	      ((module? module) module)
	      ((pair? module)   (resolve-module module))
	      (else (current-module)))))
    (module-ref mod x)))
  
(define (terms-add-sym0 mod local? sym handle)
  (if (symbol? sym)
      (handle sym mod)
      (when (not mod)
	(match sym
         ((#:atom f #f #f n m)
	  (handle f #t))
    
	 ((#:atom f amp (and atom ((or #:string #:atom) a . _)) n m)
	  (terms-add-sym0 mod local? `(#:atom ,f ,amp (,atom) ,n ,m) handle))
    
	 ((#:atom f amp l n m)
	  (let ((l (map
		    (lambda (x)
		      (match x
			     ((#:atom a _ _ n m) a)
			     ((#:string a . _)   (string->symbol a))
			     (l l)
			     (_                  
			      (error (format #f "wrong @ argument (~a) in ~a" 
					     x (get-refstr n m))))))
		    (get... l))))
	    (let ((mod (resolve-module l)))
	      (handle f mod))))
    
	 ((#:atom f amp (#:atom f . _) n m)
	  (let ((mod (resolve-module `(language prolog prolog-modules ,f))))
	    (cond
	     ((eq? (current-module) (module-ref mod f))
	      (handle f #t)))))
    
	 (s (when (symbol? s)
	      (handle s #t)))))))

(define (terms-add-sym mod local? sym)
  (terms-add-sym0 mod local? sym handle))

(define (sym-ref data)
  (apply terms-add-sym0 (append data (list handle-ref))))

(define (rem-sym sym)
  (fluid-set! syms (vhash-delq  sym (fluid-ref syms))))
(define (get-syms)
  (vhash-fold (lambda (k v r) (cons k r)) '() (fluid-ref syms)))
