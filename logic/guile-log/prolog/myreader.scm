(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))

(define-module- (logic guile-log prolog myreader)
  #:use-module (ice-9 format)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module ((logic guile-log prolog parser) #:select
		(expand_goal do-exp-in-parse?))
  #:use-module ((logic guile-log slask) #:select (goal-expand)) 
  #:export (read-one-term))

(<define> (goal_expand0 x xx)
    (when goal-expand
      (goal-expand x xx)
      (<pp> (list 'done x xx))))

(define reg #f)

(<define> (register V N S)
  (<code>
   (set! reg ((@ (guile) list) V N S))))

(<define> (param P) (<=> P reg))
(<define> (write_string w ww x)
  (<=> w (,(format #f "~s" (<lookup> x)) . ww)))

(<define> (write_proc w ww x)
  (let* ((n  (procedure-name (<lookup> x)))
	 (nn (symbol->string n)))
    (if ((@ (guile) member) #\\ (string->list nn))
	(<=> w (,(format #f "(scm-string->symbol ~s)" nn) . ww))
	(<=> w (,(format #f "~a" n) . ww)))))

(<define> (write_rest w ww x)
  (<=> w (,(format #f "~a" (<lookup> x)) . ww)))

(define s-stack '())
(<define> (source x)
   (let* ((x (<lookup> x))
	  (x (cond
	      ((string? x)
	       x)
	      ((procedure? x)
	       (symbol->string (procedure-name x)))))
	  (s (open-file x "r")))
     (<code> (set! s-stack (cons s s-stack)))))
     
(<define> (q . x) (<pp> x))

(<define> (str2atom x y)
  (let ((s (string->symbol (<lookup> x)))
	(m (current-module)))
    (if (module-defined? s m)
	(<=> y ,(module-ref m s))
	(syntax_error "Could not find str name in current module"))))

(compile-prolog-string
"
directive_pre(N(|A),B,X) :- !,
   append(A,B,C),   
   directive(N,C,X).

directive_pre(N,C,X) :-
   directive(N,C,X).

-trace.
directive(public                  , _, _) :- !.
directive(dynamic                 , _, _) :- !.
directive(discontiguous           , _, _) :- !.
directive(multifile               , _, _) :- !.
directive(module                  , _, _) :- !.
directive(volatile                , _, _) :- !.
directive(meta_predicate          , _, _) :- !.
directive(noprofile               , _, _) :- !.
directive(create_prolog_flag      , _, _) :- !.
directive(thread_local            , _, _) :- !.
directive(add_term_expansion      , _, _) :- !.
directive(add_term_expansion_temp , _, _) :- !.
directive(add_attribute_cstr      , _, _) :- !.
directive(add_attribute_projector , _, _) :- !.
directive(add_goal_expansion      , _, _) :- !.
directive(add_goal_temp           , _, _) :- !.
directive(use_module              , _, _) :- !.
directive(include,[A], _) :- !,
   source(A).
   
directive(use_module, A, use_module(|A)) :- !,
   write(A),nl,
   use_module(|A).

directive(ensure_loaded, A, ensure_loaded(|A)) :- !,
   ensure_loaded(|A).

directive(set_prolog_flag, A, set_prolog_flag(|A)) :- !,
   set_prolog_flag(|A).

directive(op, A, op(|A)) :- !,
  op(|A).

directive(initialization,[X],X) :- !.
  
directive(F,B,F(|C)) :-
  append(A,B,C).

goal0(fail    ,  \"<fail> \")   :- !.
goal0(false   ,  \"<fail> \")   :- !.
goal0(true    ,  \"<cc> \")     :- !.
goal0(repeat  ,  \"<repeat> \") :- !.
goal0(!       ,  \"<cut>    \") :- !.

goal1((->)    ,  \"<if1> \"  ) :- !.
goal1((\\+)   ,  \"<not >\" ) :- !.

goal2((->)    ,  \"<if> \"  ) :- !.
goal2(','     ,  \"<and> \" ) :- !.
goal2(';'     ,  \"<or> \"  ) :- !.
goal2(',,'    ,  \"<qnd> \" ) :- !.
goal2(';;'    ,  \"<qor> \" ) :- !.
goal2('|'     ,  \"<or> \"  ) :- !.


-trace.
xnify(=        ,  \"<=>  \"  ) :- !.
xnify(\\=      ,  \"<!=!> \" ) :- !.
xnify('=='     ,  \"<==> \"  ) :- !.
xnify('=..'    ,  \"<=..> \" ) :- !.
xnify('\\=='   ,  \"<!==!> \") :- !.
xnify(@>       ,  \"@> \"    ) :- !.
xnify(@<       ,  \"@< \"    ) :- !.
xnify(@>=      ,  \"@>= \"   ) :- !.
xnify(@=<      ,  \"@<= \"   ) :- !.
xnify(\"=@=\"  ,  \"<=@=> \" ) :- !.
%xnify(\\=@=   ,  \"<!@!> \" ) :- !.

-trace.
comp(\">\"     ,  \"> \"     ) :- !.
comp(\"<\"     ,  \"< \"     ) :- !.
comp(\">=\"    ,  \">= \"    ) :- !.
comp(\"=<\"    ,  \"<= \"    ) :- !.
comp(\"?=\"    ,  \"?= \"    ) :- !.
comp(\"=\\\\=\",  \"<!=!> \" ) :- !.
comp(\"=:=\"   ,  \"<=:=> \" ) :- !.


-trace.
scm0(\"pi\"       , \":pi:  \")     :- !.
scm0(\"e\"        , \":e:  \")      :- !.
scm0(\"epsilon\"  , \":epsilon: \") :- !.
scm0(\"inf\"      , \":inf: \")     :- !.
scm0(\"nan\"      , \":nan: \")     :- !.
scm0(\"cputime\"  , \":cputime: \") :- !.

-trace.
scm1(\"\\\\\"         , \":lognot: \")       :- !.
scm1(\"op1-\"         , \":-: \" )          :- !.
scm1(\"op1+\"         , \":+: \" )          :- !.
scm1(\"op2-\"         , \":-: \" )          :- !.
scm1(\"op2+\"         , \":+: \" )          :- !.
scm1(\"sin\"          , \":sin: \")         :- !.
scm1(\"cos\"          , \":cos: \")         :- !.
scm1(\"tan\"          , \":tan: \")         :- !.
scm1(\"exp\"          , \":exp: \")         :- !.
scm1(\"abs\"          , \":abs: \")         :- !.
scm1(\"log\"          , \":log: \")         :- !.
scm1(\"sign\"         , \":sign: \")        :- !.
scm1(\"random\"       , \":random: \")      :- !.
scm1(\"round\"        , \":round: \")       :- !.
scm1(\"float\"        , \":float: \")       :- !.
scm1(\"integer\"      , \":round: \")       :- !.
scm1(\"rational\"     , \":rational: \")    :- !.
scm1(\"rationalize\"  , \":rationalize: \") :- !.
scm1(\"numerator\"    , \":numerator: \")   :- !.
scm1(\"denominator\"  , \":denominator: \") :- !.
scm1(\"truncate\"     , \":truncate: \")    :- !.
scm1(\"floor\"        , \":floor: \")       :- !.
scm1(\"ceiling\"      , \":ceiling: \")     :- !.
scm1(\"ceil\"         , \":ceil: \")        :- !.
scm1(\"sqrt\"         , \":sqrt: \")        :- !.
scm1(\"acos\"         , \":acos: \")        :- !.
scm1(\"asin\"         , \":asin: \")        :- !.
scm1(\"atan\"         , \":atan: \")        :- !.
scm1(\"sinh\"         , \":sinh: \")        :- !.
scm1(\"cosh\"         , \":cosh: \")        :- !.
scm1(\"tanh\"         , \":tanh: \")        :- !.
scm1(\"asinh\"        , \":asinh: \")       :- !.
scm1(\"acosh\"        , \":acosh: \")       :- !.
scm1(\"atanh\"        , \":atanh: \")       :- !.
scm1(\"rgamma\"       , \":rgamma: \")      :- !.
scm1(\"erf\"          , \":erf: \")         :- !.
scm1(\"erfc\"         , \":erfc: \")        :- !.
scm1(\"msb\"          , \":msb: \")         :- !.
scm1(\"lsb\"          , \":lsb: \")         :- !.
scm1(\"popcount\"     , \":popcount: \")    :- !.
scm1(\"float_fractional_part\"  , \":float_fractional_part: \") :- !.
scm1(\"float_integer_part\"     , \":float_integer_part: \"   ) :- !.


scm2(\"op2-\"    ,   \":-: \"   ) :- !.
scm2(\"op2+\"    ,   \":+: \"   ) :- !.
scm2(\"op2*\"    ,   \":*: \"   ) :- !.
scm2(\"op2/\"    ,   \":/: \"   ) :- !.
scm2(\"\\\\/\"   ,   \":ior: \" ) :- !.
scm2(\"/\\\\\"   ,   \":and: \" ) :- !.
scm2(\"^\"       ,   \":^: \"   ) :- !.
scm2(\"**\"      ,   \":**: \"  ) :- !.
scm2(\"<<\"      ,   \":<<: \"  ) :- !.
scm2(\">>\"      ,   \":>>: \"  ) :- !.
scm2(\"xor\"     ,   \":xor: \" ) :- !.
scm2(\"mod\"     ,   \":mod: \" ) :- !.
scm2(\"rem\"     ,   \":rem: \" ) :- !.
scm2(\"div\"     ,   \":div: \" ) :- !.
scm2(\"rdiv\"    ,   \":rdiv: \") :- !.
scm2(\"gcd\"     ,   \":gcd: \" ) :- !.
scm2(\"lcm\"     ,   \":lcm: \" ) :- !.
scm2(\"//\"      ,   \"://: \"  ) :- !.
scm2(\"cons\"    ,   \":cons: \") :- !.
scm2(\"max\"     ,   \":max: \" ) :- !.
scm2(\"min\"     ,   \":min: \" ) :- !.
scm2(\"copysign\"      ,  \"copysign \")      :- !.
scm2(\"nexttoward\"    ,  \":nexttoward: \")  :- !.
scm2(\"roundtoward\"   ,  \":roundtoward: \") :- !.
scm2(\"getbit\"        ,  \":getbit: \")      :- !.


scm3(\"powm\", \":powm: \") :- !.
")


(compile-prolog-string
 "
-trace.
fscm(W,WW,A) :-
   var(A),!,
   W=[\"(<lookup> \"|W1],
   karg(W1,W2,A),
   W2=[\")\" | WW].

fscm(W,WW,F(A)) :-
  scm1(F,FF),!,
  W = [\"(<scmcall> \", FF, \" \" | W2],
  fscm(W2,W3,A),
  W3 = [\")\" | WW].

fscm(W,WW,F(A,B)) :-
  scm2(F,FF), !,
  W = [\"(\", FF, \" \" | W2],
  fscm(W2,W3,A),
  spc(W3,W4),
  fscm(W4,W5,B),
  W5 = [\")\" | WW].

fscm(W,WW,F(A,B,C)) :-
  scm3(F,FF), !,
  W = [\"(\",FF, \" \" | W2],
  fscm(W2,W3,A),
  spc(W3,W4),
  fscm(W4,W5,B),
  spc(W5,W6),
  fscm(W6,W7,C),
  W7 = [\")\" | WW].

fscm(W,WW,\"eval\"(Code)) :- !,
  gscm(W,WW,Code).

fscm(W,WW,F(|A)) :- !,
  gscm(W,WW,F(|A)).

fscm(W,WW,F) :- !,  
  (
     scm0(F,FF) -> 
        W=[FF|WW] ; 
     atom(F) ->
        gscm0(W,WW,F);
     W=[F | WW]
  ).

allgscm(W,WW,X)    :- 
  var(X),!,
  spc(W,W2),
  karg(W2,WW,X).

allgscm(W,W,[])     :- !,
  W=[\" ()\" |WW].

allgscm(W,WW,[X|L]) :- !.
   spc(W,W2),
   fscm(W2,W3,X),
   allgscm(W3,WW).

gscm(W,WW,F) :-
  var(F),!,
  W = [\"(<scm-eval> \" | W2],
  scm(W2,W3,F),
  W3 = [\")\" | WW].

gscm(W,WW,F(|X)) :-
  (string(F) -> str2atom(F,FF) ; FF=F),
  W = [\"(<scm-eval> \" | W2],
  karg(W2,W3,FF),
  algscm(W3,W4,X),
  W4 = [\")\" | WW].

gscm(W,WW,F) :-
  (string(F) -> str2atom(F,FF) ; FF=F),
  W = [\"(<scm-eval> \" | W2],
  karg(W2,W3,FF),
  W3 = [\")\" | WW].

")


(compile-prolog-string
"
spc(W,WW) :- W = [\" \"|WW].

search_n(X,[V|VV],[N|NN],NNN) :- !,
    (X == V -> NNN=N ; search_n(X,VV,NN,NNN)).

search_n(X,[],[],\"x_x\").


search_s(X,[V|VV],N) :- !,
    (X == V -> N=true ; search_s(X,VV,N)).

search_s(X,[],false).

write_var(W,WW,X) :-
    param(P),   
    [V,N,Sing] = P,

    search_n(X,V,N,NN),
    search_s(X,Sing,PP),
    (
       PP=true ->
          W=[\"x_x\"|WW];
       write_rest(W,WW,NN)
    ).

argall(W,WW,A) :-
    var(A),!,
    karg(W,WW,A).
    
argall(W,WW,[]) :- !,
    W=[\" '()\"|WW].

argall(W,WW,[X|L]) :- !,
    W=[\" \"|W2],
    karg(W2,W3,X),
    argall(W3,WW,L).

argall(W,WW,X) :-
    W=[\" \"|W2],
    karg(W2,WW,X).

-trace.
karg(W,WW,X) :-
    var(X), !,
    write_var(W,WW,X).

karg(W,WW,[]) :- !,
    W=[\"'()\" | WW].

karg(W,WW,[X|L]) :- !,
    W=[\"(scm-cons* \" | W2],
    karg(W2,W3,X),
    argall(W3,W4,L),
    W4=[\")\" | WW].    		 

karg(W,WW,F(|A)) :- A=[_|_],!,
    W=[\"(vector (scm-cons* \" | W2],
    karg(W2,W3,F),
    W3=[\" \" | W4],
    argall(W4,W5,A),
    W5=[\"))\" | WW].

karg(W,WW,{A}) :- !,
     W=[\"(vector #:brace \" | W2],
     karg(W2,W3,A),
     W3=[\")\" | WW].

karg(W,WW,F) :-
   (  
      atom(F) ->
        write_proc(W,WW,F);
      string(X) ->
 	write_string(W,WW,F);
      write_rest(W,WW,F)
   ).
")



(compile-prolog-string
"
-trace.
fkn(W,WW,F) :-
    var(F),!,
    fkn(W,WW,call(F)).

fkn(W,WW,\"is\"(A,B)) :- !,
  W = [\"(:is: \" | W2],
  karg(W2,W3,A),
  spc(W3,W4),
  fscm(W4,W5,B),
  W5 = [\")\" | WW].

fkn(W,WW,(A -> B ; C)) :- !,
  W=[\"(<if> \" | W2],
  fkn(W2,W3,A),
  spc(W3,W4),
  fkn(W4,W5,B),
  spc(W5,W6),
  fkn(W6,W7,C),
  W7=[\")\" | WW].


fkn(W,WW,(A *-> B ; C)) :- !,
  W=[\"(<*if> \" | W2],
  fkn(W2,W3,A),
  spc(W3,W4),
  fkn(W4,W5,B),
  spc(W5,W6),
  fkn(W6,W7,C),
  W7=[\")\" | WW].

fkn(W,WW,F(A)) :-
  goal1(F,FF),!,
  W = [\"(\" | W2],
  karg(W2,W3,FF),
  spc(W3,W4),
  fkn(W4,W5,A),
  W5 = [\")\" | WW].

fkn(W,WW,F(A,B)) :- 
  goal2(F,FF), !,
  W = [\"(\",FF | W3],
  spc(W3,W4),
  fkn(W4,W5,A),
  spc(W5,W6),
  fkn(W6,W7,B),
  W7 = [\")\" | WW].

fkn(W,WW,F(A,B)) :-
  comp(F,FF),!,
  W = [\"(\",FF | W3],
  spc(W3,W4),
  fscm(W4,W5,A),
  spc(W5,W6),
  fscm(W6,W7,B),
  W7 = [\")\" | WW].

com([\",\"|W],W).

fkn(W,WW,F(A,B)) :-
  xnify(F,FF),!,
  W = [\"(\", FF | W3],
  spc(W3,W4),
  com(W4,W5),
  karg(W5,W6,A),
  spc(W6,W7),
  com(W7,W8),
  karg(W8,W9,B),
  W9 = [\")\" | WW].


fkn(W,WW,F(|A)) :- !,
    (string(F) -> str2atom(F,FF) ; FF=F),
    (
      var(F) ->
         fkn(W,WW,call(F|A));
      (
         W=[\"(<apply> \" | W2],
         karg(W2,W3,FF),    
         argall(W3,W4,A),
         W4=[\")\" | WW]
      )
    ).
 
fkn(W,WW,F) :- !,
    (
       goal0(F,FF) ->
          (
             W = [FF | WW]
          );
       (
          (string(F) -> str2atom(F,FF) ; FF=F),
          W=[\"(\"|W2],
          karg(W2,W3,FF),
          W3=[\")\" | WW]
       )
    ).

fkn(_,_,[_|_]) :-
    syntax_error(\"function must be a variable or symbol\").
")


(compile-prolog-string
"
action(W,WW,F(|A)) :-
   W=[\"(run-prolog \" | W2],
   karg(W2,W3,F),
   spc(W3,W4),
   argall(W4,W5,A),
   W5=[\")\" | WW].

-trace.
final_term(W,WW,X,P) :-
   goal_expand0(X,XX),!,
   (var(XX) -> fail ; true),
   final_term(W,WW,XX,P).

final_term(W,WW, [(:- X)|U], _) :- !,
    directive_pre(X,U,Exe),
    (
       var(Exe) ->
          W=[\"(scm-values)\" | WW];
       action(W,WW,Exe)
    ).


final_term(W,WW, (:- X), _) :- !,    
  final_term(W,WW, [(:- X)], _).

final_term(W, WW, (F:Mod)(|A), P) :- !,
    fterm(W,WW,Mod,F,A,true,P).

final_term(W, WW, (F:Mod), P) :- !,
    fterm(W,WW,Mod,F,[],true,P).

final_term(W, WW, ((F:M)(|A) :- XX), P) :- !,
    fterm(W,WW,M,F,A,XX,P).

final_term(W, WW, (F(|A) :- XX), P) :- !,
    fterm(W,WW,_,F,A,XX,P).

final_term(W, WW, ((F:M) :- XX), P) :- !,
    fterm(W,WW,M,F,[],XX,P).

final_term(W, WW, (F :- XX), P) :- !,
    fterm(W,WW,_,F,[],XX,P).

final_term(W, WW, F(|A), P) :- !,
    fterm(W,WW,_,F,A,true,P).

final_term(W, WW, F, P) :- !,
    chk_F(F),    
    fterm(W,WW,_,F,[],true,P).

chk_F(F) :-(atom(F) -> true ; syntax_error(\"a predicate must by named\")).

-trace.
fterm(W, WW, Mod, F, A, XX, P) :-
    chk_F(F),
    W=[\"(make-prolog-term \" | W2],
    karg(W2,W3,F),
    spc(W3,W4),
    karg(W4,W5,A),
    spc(W5,W6),
    fkn(W6,W7,XX),
    W7=[\"(list  \"|W8],
    (var(P) -> W8=W9 ; argall(W8,W9,P)),
    spc(W9,W10),
    (var(Mod) -> W10=[\"#f\"|W11] ; karg(W10,W11,M)),
    W11=[\"))\" | WW].

")


(compile-prolog-string
"
read_one_term(S,W,Mute) :-
  read_one_term0(S,W,Prefix,Prefix2,Mute); 
  W=[].

-trace.
read_one_term0(S,W,Prefix,Prefix2,Mute) :-
    read_term(S,X,[variables(V),variable_names(N),singletons(Sing)]),
    write(read(X)),nl,
    Mute == #f,
    ( X == end_of_file -> fail ; true),
    register(V,N,Sing),
    (
	X = -P -> 
           (
	       [P|PP] = Prefix,
	       read_one_term0(S,W,PP,Prefix2)
	   );
        (
          Prefix=[],
	  final_term(W,WW,X,Prefix2),
          WW=[]
        )
    ).
")

(define* (read-one-term S #:key (mute #f))
  (let ((SS (cond
	     ((null? s-stack)
	      (begin
		(set! s-stack (list S))
		S))
	      
	      ((eq? S (car s-stack))
	       S)

	      (else
	       (car s-stack)))))
    
    (define (untangle)
      (when (not (eq? S SS))
	(close-port SS)
	(set! s-stack (cdr s-stack))))

    (with-fluids ((do-exp-in-parse? #f))
      (call-with-output-string 
       (lambda (SSS)
	 (<run> 1 (w)
 	   (read_one_term SS w mute)
	   (let ((x (pk (<scm> w))))
	     (if (null? x)
		 (<code> (untangle))
		 (<code> (format SSS "~{~a~}" (<scm> w)))))))))))
				      
