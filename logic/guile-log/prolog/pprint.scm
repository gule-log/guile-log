;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log prolog pprint)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log umatch) #:select (gp-attvar-raw?))
  #:use-module (logic guile-log prolog dynamic)
  #:use-module (logic guile-log prolog util)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log prolog io)
  #:export (listing numbervars))

(<define> (<numbervars> x xx start end va a sing p1 p2)
  (define count (make-hash-table))

  (define (make-var i)
    (if p1
	(format #f va i)
	(format #f "~a[~a]" va i)))
	

  (define set-var
    (<lambda> (V)
      (let ((i (hashq-ref count (<lookup> V))))
	(if i
	    (<=> V ,(make-var i))
	    <cc>))))

  (define set-var0
    (<lambda> (V)
      (let ((i (hashq-ref count (<lookup> V))))
	(if i
	    (<=> V ,(if p2 "_" (make-var "_")))
	    <cc>))))

  (define new-var
    (<lambda> (i)
      (<if> (<=> sing "true")
	    (let ((V (gp-var! S)))
	      (<code> (hashq-set! count V i))
	      (<cc> V))
	    
	     (<cc> (make-var i)))))
 
  (define bulk
    (<lambda> (lp m x i end)
       (if (<var?> x)
	   (if (gp-attvar-raw? x S)
	       (<cond>
		((<=> a "skip")
		 (<cc> x i))
	       
		((<=> a "bind")
		 (<values> (v) (new-var i))
		 (<cc> v (+ i 1)))

		((<=> a "error")
		 (error "attributed var in numbervars"))

		(else
		 (error "not a known attributed choice in numbervars")))
	      
	       (if (and (not (<var?> end)) (= i end))
		   (<cc> x i)
		   (<and>
		    (<values> (v) (new-var i))
		    (<cc> v (+ i 1)))))
	   	  
	   (<<match>> () (x)
	    ((x . l)
	     (<and>
	      (<values> (x1 i1) (lp m x i))
	      (<values> (x2 i2) (lp m l i1))
	      (<cc> (cons x1 x2) i2)))

	    (#((x . l))
	     (<and>
	      (<values> (x1 i1) (lp m (cons x l) i))
	      (<cc> (vector x1) i1)))

	     (x
	      (<cc> x i))))))
  
  (define bonzai
    (<lambda> (m x i)
      (let ((x (<lookup> x)))
	(if (hashq-ref m x)
	    (<and>
	     (set-var (hashq-ref m x))
	     (<cc> (hashq-ref m x) i))
	       
	    (<and>
	     (<values> (xx ii) (bulk bonzai m x i end))
	     (<code> (hashq-set! m x xx))
	     (<cc> xx ii))))))

  (if (<var?> start)
      (<=> start 0)
      <cc>)
  
  (let ((m   (make-hash-table))
	(end (<lookup> end)))

    (<values> (x i) (bonzai m x start))

    (<recur> lp ((l (hash-fold (lambda (k v s) (cons k s)) '() count)))
      (if (pair? l)
	  (<and>
	   (if (<var?> (car l))
	       (set-var0 (car l))
	       <cc>)
	   (lp (cdr l)))
	  
	  <cc>))
    
    (<=> end i)
    (<=> xx  x)))

(define numbervars
  (<case-lambda>
   ((x y z opt)
    (define (st x)
      (if (procedure? x)
	  (symbol->string (procedure-name x))
	  x))
    
    (<var> (V A SS)
      (let ((vv (vector (list "functor_name" V)))
	    (aa (vector (list "attvar"       A)))
	    (ss (vector (list "singletons"   SS))))

	(<or>	 
	 (member vv opt)
	 (<=> ,V "$VAR[~a]"))

	(<or>
	 (member aa opt)
	 (<=> ,A "error"))
	
	(<or>
	 (member ss opt)
	 (<=> ,SS "false"))
	
	<cut>
	
	(<numbervars> x x y z (st (<lookup> V)) (<lookup> A) (<lookup> SS)
		      #f #f))))
   
   ((x y z)
    (numbervars x y z '()))

   ((x)
    (numbervars x 0 (gp-var! S) '()))))

(define listing
  (<case-lambda>
   ((f)
    (<match> () (f)
     (#(("op2/" N ARITY))
       (when (not (<var?> ARITY))
	 (<and>
	  <cut>
	  (<var> (F)
	    (functor F N ARITY)
	    (listing2 F)))))
      (F
       (listing2 (vector (cons F (gp-var! S)))))))
   
   ((f opt)
    ;; We have no implementation of the swi options
    (listing f))))

(<define> (listing2 F)
  (<var> (Out)
    (standard_output Out)
    (<or>	   
     (<var> (L xx end FF LL)
      (clause F L)
      (<numbervars> (cons F L) xx 0 end "V~a" "error" "true" #t #t)
      (<=> (FF . LL) xx)
      (nl Out)
      (pprint Out FF LL 4)
      (nl Out)
      <fail>)
     (<and>
      (nl Out)
      <cc>))))

(<define> (indent Out n)
  (<or>
   (<recur> lp ((i 0))
     (when (< i n)
       (write Out " ")
       (lp (+ i 1))))
   <cc>))

(<define> (pprint Out F L N)
  (write_term Out F '())
  (<if> (<or> (<=> L ()) (<=> L "true"))
	(write Out ".")
	(<and>
	 (write Out " :- ")
	 (nl Out)
	 (indent Out N)
	 (pprint-rhs Out L N N)
	 (write Out "."))))

(<define> (operator X)
  (let* ((x (<lookup> X))
	 (x (cond
	     ((string? x)
	      x)
	     ((procedure? x)
	      (symbol->string (procedure-name x)))
	     ((symbol? x)
	      (symbol->string x))
	     (else
	      (error (format #f "not an alowed functor ~a" x)))))
	 (x  (string->list x)))
    
    (when (or (not (char-alphabetic? (car x)))
	      (and
	       (> ((@ (guile) length) x) 3)
	       (eqv? (list-ref x 0) #\o)
	       (eqv? (list-ref x 1) #\p)
	       (eqv? (list-ref x 2) #\2)
	       (not (char-alphabetic? (list-ref x 3))))
	      (and
	       (> ((@ (guile) length) x) 3)
	       (eqv? (list-ref x 0) #\o)
	       (eqv? (list-ref x 1) #\p)
	       (eqv? (list-ref x 2) #\1)
	       (not (char-alphabetic? (list-ref x 3)))))
      <cc>)))

(<define> (foperator A)
  (when (not (<var?> A))
    (<match> () (A)
      (#((F . L))
       (<cut> (operator F)))
      (else
       (<cut> <fail>)))))

(<define> (pprint-rhs Out L I N)
  (if (<var?> L)
      (write Out L)
      (<match> () (L)
        (#((";" #(("->" A B)) C))
	 (<and>
	  <cut>
	  (write Out "(")
	  (nl Out)
	  (indent Out (+ I N))
	  (pprint-rhs Out A (+ I N) N)
	  (nl Out)
	  (indent Out (- (+ I N) 2))
	  (write Out "->")
	  (nl Out)
	  (indent Out (+ I N))
	  (pprint-rhs Out B (+ I N) N)
	  (nl Out)
	  (indent Out (- (+ I N) 2))
	  (write Out ";")
	  (nl Out)
	  (indent Out (+ I N))
	  (pprint-rhs Out C (+ I N) N)
	  (nl Out)
	  (indent Out I)
	  (write Out ")")))
	 
        (#((F A))
	 (<and>
	  (operator F)
	  <cut>
	  (<if> (<or> (<=> F "is") (foperator A))
		(<and>	    
		 (write Out "(")
		 (write Out F)
		 (nl Out)
		 (indent (+ I N))
		 (pprint-rhs Out A (+ I N) N)
		 (nl Out)
		 (indent I)
		 (write Out ")")
		 (nl))

		(<and>
		 (write Out "(")
		 (write Out F)
		 (write Out " ")
		 (write Out A)))))

	(#((F A B))
	 (<and>
	  (operator F)
	  <cut>
	  (<if> (<or> (foperator A) (foperator B))
		(<and>	    
		 (write Out "(")
		 (nl Out)
		 (indent Out (+ I N))
		 (pprint-rhs Out A (+ I N) N)
		 (nl Out)
		 (indent Out (+ I N))
		 (write Out F)
		 (nl Out)
		 (indent Out (+ I N))
		 (pprint-rhs Out B (+ I N) N)
		 (nl Out)
		 (indent Out I)	    
		 (write Out ")"))
	    
		(<and>
		 (write Out "( ")
		 (write Out A)
		 (write Out " ")
		 (write Out F)
		 (write Out " ")
		 (write Out B)
		 (write Out " )")))))
    
	(F
	 (<cut> 
	  (write Out F))))))

(set! (@@ (logic guile-log prolog io) numbervars) numbervars)
