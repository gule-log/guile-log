;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log guile-prolog memoize)
  #:use-module (logic guile-log memoize)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog goal-functors)
  #:export (with_rational_trees with_non_ratioanl_tree rational_trees
				non_rational_trees))

(<define> (with_rational_trees     code)
	  (with-rec-unifyer (<lambda> () (goal-eval code))))

(<define> (with_non_rational_trees code)
	  (with-nonrec-unifyer (<lambda> () (goal-eval code))))


(<define> (rational_trees)      (rec-unifyer))
(<define> (non_rational_trees)  (nonrec-unifyer))
	  
  
