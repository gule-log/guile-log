;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log guile-prolog paralell)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log paralell)
  #:export (paralell pzip conjeur zipper same))

(define (recur Goal)
  (<lambda> (data)
   (<values> (f Next) (goal-eval Goal))
   (<cc> (<lookup> f) (recur (<cp> Next)))))


(<define> (paralell . l)
  (<<match>> (#:mode -) (l)
    ((fail (d1 p1 x1) (d2 p2 x2))
     (<pand> n (recur fail) da ee cc 
       ((dd1 pp1 (gp-make-engine (+ n 1) 100) (goal-eval x1))
        (dd2 pp2 (gp-make-engine (+ n 1) 100) (goal-eval x2)))
       (<=> (dd1 pp1 dd2 pp2)
            (d1  p1  d2  p2))))
    
    ((fail (d1 p1 x1) (d2 p2 x2) (d3 p3 x3))
     (<pand> n (recur fail) da ee cc 
       ((dd1 pp1 (gp-make-engine (+ n 1) 100) (goal-eval x1))
        (dd2 pp2 (gp-make-engine (+ n 1) 100) (goal-eval x2))
        (dd3 pp3 (gp-make-engine (+ n 1) 100) (goal-eval x3)))
       (<=> (dd1 pp1 dd2 pp2 dd2 pp3)
            (d1  p1  d2  p2  d3  p3))))

    ((fail (d1 p1 x1) (d2 p2 x2) (d3 p3 x3) (d4 p4 x4))
     (<pand> n (recur fail) da ee cc 
       ((dd1 pp1 (gp-make-engine (+ n 1) 100) (goal-eval x1))
        (dd2 pp2 (gp-make-engine (+ n 1) 100) (goal-eval x2))
        (dd3 pp3 (gp-make-engine (+ n 1) 100) (goal-eval x3))
        (dd4 pp4 (gp-make-engine (+ n 1) 100) (goal-eval x4)))
       (<=> (dd1 pp1 dd2 pp2 dd3 pp3 dd4 pp4)
            (d1  p1  d2  p2  d3  p3  d4  p4))))))
    
(define (fail- x) x)

(define pzip
  (<case-lambda>
   (()     <fail>)
   ((x)    (goal-eval x))
   ((x y)
    (<pzip>
     ((goal-eval x))
     ((goal-eval y))))
   ((x y u)
    (<pzip>
     ((goal-eval x))
     ((goal-eval y))
     ((goal-eval u))))
   ((x y u v)
    (<pzip>
     ((goal-eval x))
     ((goal-eval y))
     ((goal-eval u))
     ((goal-eval v))))
   (l
    (let ((l-u (let lp ((l l) (u '()) (n (/ (length l) 2)))
                 (if (= n 0)
                     (cons l u)
                     (lp (cdr l) (cons (car l) u) (- n 1))))))                  
      (<pzip> 
       ((<apply> pzip (car l-u)))
       ((<apply> pzip (cdr l-u))))))))
  

(define (conjeur s p cc . l)
  (call-with-values
      (lambda ()
        (apply conjunctioneur
               (map (lambda (x) (gp-lookup s x)) l)))
    (lambda (x y)
      (cc s p x y))))

(define (zipper s p cc . l)
  (call-with-values
      (lambda ()
        (apply jack-the-zipper
               (map (lambda (x) (gp-lookup s x)) l)))
    (lambda (x y)
      (cc s p x y))))
                     

