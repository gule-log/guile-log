;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log guile-prolog postpone)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log postpone)
  #:re-export (postpone)
  #:export (postpone_frame))

(<define> (postpone_frame . x) (<apply> postpone-frame x))



