;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log guile-prolog project)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log prolog util) #:select (length_))
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log vlist)
  #:re-export (multibute)
  #:export (project_the_attributes))

(<define> (seen H N X)
  (if (hash-ref H (cons (<lookup> N) (<lookup> X)) #f)
      <cc>
      <fail>))

(<define> (add H N X)
  (<code> (hash-set! H (cons (<lookup> N) (<lookup> X)) #t)))

(compile-prolog-string
"
%k(A,B,C,H)  :- write(k(A,B,C)),nl,fail.
k(V,L,LL,H) :- (var(V),!) -> (attvar(V) -> L=[V|LL] ; L=LL).
k([],L,L,H) :- !.
k(X,L,LL,H) :- 
  X==[]    ->
    L=LL;
  X=[A|B] ->
    (
      length_(X,N),
      k(X,L,LL,H,N)
    );
  atomic(X) -> 
    L=LL; 
  X={Y} ->
    k(Y,L,LL,H);
  X=..[X|U] -> 
    k([X|U],L,LL,H) ; 
  L=LL. 

k(X,L,LL,H,N) :-
  seen(H,N,X) ->
    LL=L;            
  (
    add(H,N,X),
    X=[A|B],
    NN is N - 1,
    k(A,LX,LL,H),
    (
      NN == 0 ->
          k(B,L,LX,H);
        k(B,L,LX,H,NN)
    )
  ).

")

(<define> (project_the_attributes q)
  (<var> (l)
    (k q l '() (make-hash-table))
    (let lp ((v vlist-null) (l l))
       (<<match>> (#:mode -) (l)
         ((x . l)
	  (<let> ((x (<lookup> x)))
	    (if (gp-attvar-raw? x S)
		(<recur> lp2 ((v v) (d (gp-att-data x S)))
		  (<match> (#:mode -) (d)
		   (((a . _) . d)
		     (<let*> ((a (<lookup> a))
			      (f (ref-attribute-projector a)))
		       (if (and f (not (vhashq-ref v f #f)))
			   (lp2 (vhash-consq f #t v) d)
			   (lp2 v d))))
		    (()
		     (lp v l))))
		(lp v l))))
	 (()
	  (<recur> lp ((u  (map car (vhash->assoc v))))	    
	    (<<match>> (#:mode -) (u)
	     ((x . u)
	      (<and>
	       (<or> (<and> ((<lookup> x) q l) <cut>) <cc>)
	       (lp u)))
	     (() <cc>))))))))
