;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log guile-prolog wind)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log iso-prolog)
  #:export (dynwind))

(compile-prolog-string
"
prot(Goal) :- catch(call(Goal),Ball,(write(subcatch_in_dynwind(Ball)),nl)).
")

(define (protector s goal)
  (lambda q
    (with-fluids ((*gp*            (gp-make-engine 0 1000))
                  (*current-stack* (make-fluid '())))

      (gp-push-engine (fluid-ref *current-stack*)
                      (fluid-ref *gp*))

      (let* ((s  (fluid-ref *current-stack*))
             (fr (gp-newframe s)))
        ((<lambda> () (prot goal))
         fr (lambda x #f) (lambda x #f))
        (gp-unwind fr)))))

(define dynwind
  (<case-lambda>
   ((x)
    (<dynwind> (lambda q #t) (protector S x)))
   ((x y)
    (<dynwind> (protector S x)   (protector S y)))))  
