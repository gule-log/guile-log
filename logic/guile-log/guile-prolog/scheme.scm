;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


#|
interact with scheme expressions e.g.
scheme([+,X,Y],Out)
|#

(<define> (scheme L Out)
 (<=> Out ,(eval (<scm> L) (current-module))))

