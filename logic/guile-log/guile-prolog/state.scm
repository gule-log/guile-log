;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log guile-prolog state)
  #:use-module (logic guile-log)
  #:export (stall))
  
#|
state handling e.g. be able to store a state entering a repl, as well
as storing a state.
|#

(define stall <stall>)




