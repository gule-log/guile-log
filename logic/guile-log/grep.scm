;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log grep)
  #:use-module (logic guile-log parser)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log)
  #:use-module (ice-9 threads)
  #:export (grep sed par-grep par-sed))

(define get-stack     #f)
(define release-stack 
  (let ()
    (define (mk) (gp-make-stack 0 0 10000 10000 10000 10000))
    (let ((l (list (mk)))
	  (m (make-mutex)))
      (set! get-stack
	    (lambda ()
	      (with-mutex m
		 (if (null? l)
		     (mk)
		     (let ((ret (car l)))
		       (set! l (cdr l))
		       ret)))))
      (lambda (s) 
	(with-mutex m (set! l (cons s l)))))))
  

(define (fgrep-raw match f)
  (let ((p (pipe)))
    (begin
      (let ((stack (get-stack)))
	(call-with-new-thread 
	 (lambda () 	   
	   (fluid-set! *gp* stack)
	   (let ((c-s (@ (logic guile-log umatch) *current-stack*)))
	     (fluid-set! c-s (gp-newframe '())))
	   ((@ (logic guile-log) <clear>))

	   (with-output-to-port (cdr p) 
	     (lambda ()
	       (f) 
	       (close-port (cdr p))))
	   (release-stack stack))))
      (with-input-from-port (car p)
	(lambda ()
	  (<clear>)
	  (<run> 1 () (<values> (x xl n m out)
				(match '() (make-file-reader) 0 0 <_>)))
	  (if #f #f))))))


(define (print-line A B)
  (<p-lambda> (c)
    (<code>
     (let lp ((i (- A)))
       (when (<= i B)
	     (call-with-values (lambda () (file-next-line XL (+ M (- i 1))))
	       (lambda (found x xl)
		 (if found
		     (begin
		       (format #t "~a~%" (list->string x))
		       (lp (+ i 1)))
		     (lp (+ i 1)))))))
     (format #t "~%"))
    (<p-cc> c)))
  
(define (mk-sexp-print a b d)
  (let ((start  '())
	(bang   #f)
	(depth   0))
    (values
     (<p-lambda> (c)
       (<dynwind>
	(lambda ww
	  (set! start (cons M start))
	  (set! depth (+ depth 1)))
	(lambda ww
	  (set! start (cdr start))
	  (set! depth (- depth 1))))
       (<p-cc> c))
     (<p-lambda> (c)
       (<code>
	(if (not bang) 
	    (set! bang depth)
	    (set! bang (min depth bang))))
       (<p-cc> c))
     (<p-lambda> (c)
       (if bang	
	   (<and> 
	    (if (or (<= depth 1) (<= depth (- bang d)))
		(<and>
		 (<code> (set! bang #f))
		 (.. (q) ((print-line (+ (- M (car start)) a)
				      b) c)))
		<cc>))
	   <cc>)
       (<p-cc> c)))))


(define (line-match match A B)
  (letrec ((Qg ((s-scan-item A 10) g))
	   (f  (f-seq  Qg f))
	   (g  (f-and! (f-or f-nl
			     (f-seq  match (print-line A B) f-do-nl)
			     (f-seq  (f-reg ".") g)))))
      f))


(define (pr-line-match match)
  (letrec ((Qg ((s-scan-item 0 10) g))
	   (f  (f-seq  Qg f))
	   (g  (f-and! (f-or f-nl-pr
			     (f-seq 
			      (f-or! (f-and! match) 
				     (pr-reg "."))
			      g)))))


      f))




	
(define-syntax-rule (mk-par-match par-match f-char f-reg f-tag f-nl f-not
				  string-match line-comment 
				  body-comment char-par mk-sexp-print)
(define (par-match match str-match com-match a b c)
  (define (string-match match nl)
    (letrec ((bs    (f-tag "\\"))
	     (any   (f-reg "."))
	     (butq  (f-reg "[^\"]"))
	     (q     (f-char #\"))
	     (quote (f-seq!!  bs any))
	     (it    (f-or! match nl quote butq))
	     (str   (f-or! f-eof (f-seq!! it str) q)))
      (f-seq!! q str)))

  (define (line-comment match nl)
    (letrec ((no-nl (f-reg "[^\n]"))
	     (semi  (f-char #\;))
	     (it    (f-or! match no-nl))
	     (f     (f-or! f-eof nl (f-seq!! it f))))
      (f-seq!! semi f)))
	   

  (define (body-comment match nl)
    (letrec ((right     (f-tag "|#"))
	     (left      (f-tag "#|"))
	     (not-right (f-not right))
	     (it        (f-or! nl match f not-right))
	     (g         (f-or! f-eof (f-seq!! it g) right))
	     (f         (f-seq!! left g)))
      f))

  (define char-par 
    (let ((sharp (f-tag "#"))
	  (ch1   (f-tag "\\("))
	  (ch2   (f-tag "\\)"))
	  (ch3   (f-tag "\\\""))
	  (ch4   (f-tag "\\;")))
      (f-seq!! sharp (f-or! ch1 ch2 ch3 ch4))))

  (call-with-values (lambda () (mk-sexp-print a b c))
    (lambda (start bang end)
      (let ((nl ((s-scan-item 500 1000) f-nl)))
	(letrec* ((m      (match start bang end))
		  (mstr   (str-match start bang end))
		  (mcom   (com-match start bang end))
		  (str    (string-match mstr nl))
		  (lc     (line-comment mcom nl))
		  (bc     (body-comment mcom nl))
		  (nleft  (f-reg "[^)]"))
		  (char-l (f-char #\())
		  (char-r (f-char #\)))
		  (char-bl (f-char #\[))
		  (char-br (f-char #\]))
		  (it      (f-or!
			    str
			    lc
			    bc
			    (s-clear-body (f-seq start m end))
			    (s-clear-body (f-seq start char-l  gg char-r end))
			    (s-clear-body (f-seq start char-bl gg char-br end))
			    char-par
			    nl
			    nleft))
		  (item   (lambda (g true)
			    (f-or! (f-seq!! it (Ds (item g true)))
				  f-eof
				  true)))
		  (gg    (item (Ds gg) f-true))
		  (g     (item (Ds g)  f-false)))
	 g))))))

(mk-par-match par-match 
	      f-char f-reg f-tag f-nl f-not
	      string-match line-comment 
	      body-comment
	      char-par 
	      mk-sexp-print)

(define (mk-pr-sexp-print . l) (values f-id f-id f-id))

(mk-par-match pr-par-match 
	      pr-char pr-reg pr-tag f-nl-pr pr-not
	      pr-string-match pr-line-comment 
	      pr-body-comment
	      pr-char-par 
	      mk-pr-sexp-print)

(define* (fgrep match f #:key (a 0) (b 0) (c 0))
  (let 	((a (max c a))
	 (b (max c b)))
    (fgrep-raw (line-match match a b) f)))

(define-syntax-rule (grep match code . l) 
  (fgrep match (lambda () code) . l))

(define* (fsed match f)
  (fgrep-raw (pr-line-match match) f))

(define-syntax-rule (sed match code . l) 
  (fsed match (lambda () code) . l))

(define (q-false . l) f-false)

(define* (fpar-grep match f 
		    #:key (str q-false) (com q-false)
		    (a 0) (b 0) (c 0) (d 0))
  (let 	((a (max c a))
	 (b (max c b)))
    (fgrep-raw (par-match match str com a b d) f)))

(define-syntax-rule (par-grep match code . l) 
  (fpar-grep match (lambda () code) . l))

(define* (fpar-sed match f #:key (str q-false) (com q-false))
  (fgrep-raw (pr-par-match match str com 0 0 0) f))

(define-syntax-rule (par-sed match code . l) 
  (fpar-sed match (lambda () code) . l))




	
