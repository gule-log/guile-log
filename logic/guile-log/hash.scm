;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log hash)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log canonacalize)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log)
  #:export (<make-vhash> <vhash?>))

(define (<make-vhash>)  (make-fluid vlist-null))
(define (<vhash?> x) (and (fluid? x) (or (vhash? (fluid-ref x))
                                         (eq? (fluid-ref x)) vlist-null)))
	
