;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log racklog)
  #:use-module (logic guile-log)
  #:export (%true %fail 
		  %=:= %< %> %<= %>= %=/=
		  %which %rel %more
		  %assert! %assert-after! %empty-rel
		  %let %is %constat %compound
		  %= use-occur-check? %and %or
		  %== %/== %var %nonvar 
		  %freeze %melt %melt-new %copy !
		  %if-then-else %not))

(define use-occur-check? (make-fluid #f))
(define %true (</.> <cc>  ))
(define %fail (</.> <fail>))
(define-syntax mk-op
  (syntax-rules ()
    ((_ name op 1)
     (define (name x)
       (</.> (<when> (op (gp-lookup x))))))
    ((_ name op 2)
     (define (name x y)
       (</.> (<when> (op (gp-lookup x) (gp-lookup y))))))))


(define (neq x q) (not (= x y)))
(mk-op %=:=  =   2)
(mk-op %<    <   2)
(mk-op %>    >   2)
(mk-op %<=   <=  2)
(mk-op %>=   >=  2)
(mk-op %=/=  neq 2)

(define-syntax e
  (syntax-rules ()
    ((_ x) x)))

(define-syntax %let
  (lambda (x)    
    (syntax-case x ()
      ((_ (v ...) code ...)
       (with-syntax (((vv ...) (generate-temporaries #'(v ...))))
	  #'(let ((vv (gp-var!)) ...)
	      (let-syntax ((v (syntax-rules ()
				((_ x ...)
				 ((e vv) x ...))
				(_
				 (e vv))))
			   ,,,)
		code ...)))))))

(define-syntax state (lambda (x) #'#f))

(define-syntax E
  (syntax-rules ()
    ((_ code)
     (fluid-let-syntax ((e (syntax-rules () 
			     ((_ x) (gp-loopup state x)))))
       code))))

(define-syntax S
  (syntax-rules ()
    ((_ code)
     (fluid-let-syntax ((e (syntax-rules () 
			     ((_ x) x))))
       code))))
		       
(define-syntax %which
  (syntax-rules ()
    ((_ () code ...)
     (<ask> (code) ...))
    ((_ (a ...) (code) ...)
     (<run> 1 (a ...) (code) ...))
    ((_ n (a b ...) (code) ...)
     (<run> n (a b ...) (code) ...))))

(define (%more) (<take> 1))


(define-syntax %rel
  (syntax-rules ()
      ((_ (v ...) ((m ...) code ...) ...)
       (lambda x
	 (%let (v ...)
	    (</.> 
	     (<match> (x)
	       (,`(,m ...) 
		(<and> (code) ...))
	       ...)))))))


(define-syntax %assert!
  (syntax-rules ()
    ((_ name (a ...) clause ...)
     (set! name
	   (let ((f name))	     
	     (%rel (a ...)
		   (w (apply f w))
		   clause ...))))))

(define-syntax %asssert-after!
  (syntax-rules ()
    ((_ name (a ...) clause ...)
     (set! name
	   (let ((f name))
	     (%rel (a ...)
		   clause ...
		   (w (apply f w))))))))

(define %empty-rel (lambda x %fail))

(define (%constant x)
  (</.> (if (not (or (gp-var? x) (gp-pair? x))) <cc>)))

(define (%compound x)
  (</.> (if (or (gp-var? x) (gp-pair? x)) <cc>)))

(define (%= x y) 
  (</.> (if (fluid-ref use-occures-check?)
	    (<unify> gp-unify! x y)
	    (<unify> gp-unify-raw! x y))))


(define (%and . l)
  (</.>
   (<recur> loop ((l l))
     (if (pair? l)
	 (<and> ((car l)) (loop (cdr l)))
	 <cc>))))

(define (%or . l)
  (</.>
   (<recure> loop ((l l))
      (if (pair? l)
	  (<or> ((car l)) (loop (cdr l)))
	  <fail>))))

(define (%== x y)
  (</.> (<unify> gp-m-unify! x y)))

(define (%/== x y)
  (</.> (<not> (<unify> gp-m-unify! x y))))

(define (%var x)
  (</.>
   (<recur> loop ((x x))
     (<match> (#:mode -) (x)
       ((x . l)
	 (<or> (loop x) (loop l)))
       ((? gp-var?) <cc>)
       (_ <fail>)))))


(define (%nonvar x)
  (</.>
   (<recur> loop ((x x))
     (<match> (#:mode -) (x)
       ((x . l)
	 (<and> (loop x) (loop l)))
       ((? gp-var?) <fail>)
       (_ <cc>)))))

(define-syntax %is
  (syntax-rules ()
    ((_ x y) 
     (lambda (s p cc)
       (fluid-let-syntax ((state (lambda (x) #'s)))
	 (<unify> gp-unify! (S x) (E y) s))))))


(define (freeze x)
  (let loop ((x x))
    (umatch (#:mode -) (x)
      ((? gp-var? x)
       (vector 'freeze x))
      ((x . l)
       (cons (loop x) (loop l)))
      (x x))))

(define (%freeze s f)
  (%= (freeze s) f))

(define (melt x)
  (let loop ((x x))
    (umatch (#:mode -) (x)
      ((x . l) (cons (loop x) (loop l)))
      (#('freeze x) (gp-lookup x))
      (x            (gp-lookup x)))))

(define (melt-new x)
  (define hash (make-hash-table))
  (let loop ((x x))
    (umatch (#:mode -) (x)
     ((x . l) (cons (loop x) (loop l)))
      (#('freeze x) 
       (let* ((x (gp-lookup x)))
	 (if (gp-var? x)
	     (let ((v (hasq-ref hash x)))
	       (if v
		   v
		   (let ((u (gp-var!)))
		     (hasq-set! hash v u)
		     u)))
	     x)))
      (x            (gp-lookup x)))))

(define (copy x)
  (define hash (make-hash-table))
  (define (update x)
    (let* ((x (gp-lookup x)))
      (if (gp-var? x)
	  (let ((v (hasq-ref hash x)))
	    (if v
		v
		(let ((u (gp-var!)))
		  (hasq-set! hash v u)
		  u))))))
  (let loop ((x x))
    (umatch (#:mode -) (x)
      ((x . l) 
       (cons (loop x) (loop l)))
      (#('freeze x) 
       (update x))
      (x            
       (update x)))))

(define (%melt f s)
  (%= (melt f) s))

(define (%melt-new f s)
  (%= (melt-new f) s))

(define (%copy s c)
  (%= (copy s) c))

(define-syntax !
  (syntax-rules ()
    ((_) <cut>)
    (_   <cut>)))


(define (%if-then-else a b c)
  (</.> (<if> (a) (b) (c))))

(define (%not a)
  (</.> (<not> (a))))

;; Not supporting free variables-yet
(define (%bag-of x g res)
  (</.> (<collect> x (g) res)))

(define (%set-of x g res)
  (</.> (<collect-set> x (g) res)))

(define (free-execute g)
  (lambda (s p cc v)
    (with-guarded-state gset! ((been '()) (first #t) (next #f) (fail p))
	  (g s fail
	     (lambda (ss pp inner-next)
	       (if first
		   (let ((val (v)))
		     (if (member v been)
			 (pp))
		     (let ((n
		     (set! fail p)	   
		     (set! first #f)
		     (set! v (freeeze val))
		     (set! next 
			   (let ((s (gp-store-state)))
			     (lambda x
			       (gp-restore-state-wind s)
			       (set! been  (cons cur been))
			       (set! first #t)
			       (set! v     val)
			       (set! fail  inner-next)
			       (pp))))))
	       (cc ss pp next))))))




(define (%bag-of x g out)
  (lambda (s p cc)
    (with-guarded-vars set-next! ((next (</.> (<fail> p))))
     ((bag-of set-next! x g out)
      s p (lambda (ss pp) 
	    (cc ss (lambda () (next))))))))

(define-syntax %free-vars 
  (syntax-rules ()
    ((_ (v) g)
     (lambda (s p cc next)
       (plet ((vv v))
         (let-syntax ((v (syntax-rules ()
	  		   ((_ a ...)
			    ((vv) ...))
			   (_
			    (vv)))))
	 ((free-execute g) s p cc vv next)))))))

%append
%member

