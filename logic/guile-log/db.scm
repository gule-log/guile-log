;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (logic guile-log db)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db string)
  #:use-module (logic guile-log db module)
  #:use-module (logic guile-log db data)
  #:use-module (logic guile-log db table)
  #:use-module (logic guile-log db vhash)
  #:re-export (init-db FileDB DB)
  #:export (add-db-item synch))

(define *persistors* '())

(define (add-db-item o)
  (let ((oo (make-variable o)))
    (set! *persistors* (cons oo *persistors*))
    (case-lambda
      (()  (variable-ref oo))
      ((x) (variable-set! oo x)))))

(define (pullit db)
  (let* ((trs (pull-strings db))
         (trm (pull-modules db trs))
         (trd (pull-datas db trs trm)))
    (call-with-values (lambda () (pull-tables db trd))
      (lambda (trt trtn)
        (let ((trvh (pull-vhashs db trt trtn)))
          #t)))))

(define (synch db)
  (let lp ((os *persistors*))
    (if (pair? os)
        (let ((o (variable-ref (car os))))
          (cond
           ((vhash? o)
            (add-vhash db o)))
          (lp (cdr os)))
        (pullit db))))

