(define-module (prolog-user)
  #:pure
  #:use-module ((system base compile) #:select
		(compile compile-file))
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log prolog swi)
  #:use-module (logic guile-log guile-prolog engine)
  #:use-module (logic guile-log guile-prolog paralell)
  #:use-module (logic guile-log guile-prolog fiber)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log guile-prolog set)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log guile-prolog coroutine)
  #:use-module (logic guile-log guile-prolog gc-call)
  #:use-module (logic guile-log guile-prolog interpreter)
  #:use-module (logic guile-log guile-prolog dynamic-features)
  #:use-module (logic guile-log prolog goal-expand)
  #:use-module ((guile) #:select (@ @@ syntax))
  #:filename #f)

(compile-prolog-string
 "
     :- use_module(boot(if)).
     :- use_module(boot(dcg)).
     :- use_module(sandbox).
     :- use_module(user).
     :- use_module(swi(term_macro)).
")

