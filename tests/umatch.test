(define-module (umatch)
  :use-module (logic guile-log umatch)	       
  :use-module (test-suite lib) )


(define (f1)  (umatch () (1) (X X)))
(define (f2)  (umatch () ('(1 a)) ((X _) X)))
(define (f3)  (umatch () ('(1 1)) ((X X) X)))
(define (f4)  (umatch () ('(a b)) ((X X) X) (_ 1)))
(define (f5)  (umatch () ('(a a)) ((and X (Y Y)) X)))
(define (f6)  (umatch () ("hello") (_ #t)))
(define (f7)  (umatch () ('foo) ('foo #t)))
(define (f8)  (umatch () ("bar") ("bar" #t)))
(define (f9)  (umatch () (777) (777 #t)))
(define (f10) (umatch () (#\g) (#\g #t)))
(define (f11) (umatch () ('(a b c)) ('(a b c) #t)))

(with-test-prefix "basic mode +, 1 arg,  umatch usage +"
   (pass-if  "(X X)" 
	     (equal? 1 (f1)))

   (pass-if  "((X _) X)"
	     (equal? 1 (f2)))
   
   (pass-if  "((X X) X)"
	     (equal? 1 (f3)))
   
   (pass-if  "((X X) X) (_ 1)"
	     (equal? 1 (f4)))
   
   (pass-if  "((and X (Y Y)) X)"
	     (equal? '(a a) (f5)))
   
   (pass-if "wildcard" (f6))
	    
   
   (pass-if "symbol" (f7))

   
   (pass-if "string" (f8))

   
   (pass-if "number" (f9))

   
   (pass-if "char" (f10))

   
   (pass-if "sexp" (f11)))

(define (g1) (umatch () (3 -2) (X Y (+ X Y) )))
(define (g2) (umatch () ('(3 a) -2) ((X _) Y (+ X Y))))
(define (g3) (umatch () ('(3 3) -2) ((X X) Y (+ X Y))))

(with-test-prefix "basic mode +, 2 arg,  umatch usage +"
   (pass-if  "(X X)" 
	     (equal? 1 (g1)))
   
   (pass-if  "((X _) X)"
	     (equal? 1 (g2)))
   
   (pass-if  "((X X) X)"
	     (equal? 1 (g3))))


     
(define (f1)  (umatch (#:mode *) (1) (X X)))
(define (f2)  (umatch (#:mode *) ('(1 a)) ((X _) X)))
(define (f3)  (umatch (#:mode *) ('(1 1)) ((X X) X)))
(define (f4)  (umatch (#:mode *) ('(a b)) ((X X) X) (_ 1)))
(define (f5)  (umatch (#:mode *) ('(a a)) ((and X (Y Y)) X)))
(define (f6)  (umatch (#:mode *) ("hello") (_ #t)))
(define (f7)  (umatch (#:mode *) ('foo) ('foo #t)))
(define (f8)  (umatch (#:mode *) ("bar") ("bar" #t)))
(define (f9)  (umatch (#:mode *) (777) (777 #t)))
(define (f10) (umatch (#:mode *) (#\g) (#\g #t)))
(define (f11) (umatch (#:mode *) ('(a b c)) ('(a b c) #t)))

(with-test-prefix "basic mode * 2, 1 arg,  umatch usage *"
   (pass-if  "(X X)" 
	     (equal? 1 (f1)))
   
   (pass-if  "((X _) X)"
	     (equal? 1 (f2)))
   
   (pass-if  "((X X) X)"
	     (equal? 1 (f3)))
   
   (pass-if  "((X X) X) (_ 1)"
	     (equal? 1 (f4)))
   
   (pass-if  "((and X (Y Y)) X)"
	     (equal? '(a a) (f5)))
   
   (pass-if "wildcard" (f6))
	    
   
   (pass-if "symbol" (f7))

   
   (pass-if "string" (f8))

   
   (pass-if "number" (f9))

   
   (pass-if "char" (f10))

   
   (pass-if "sexp" (f11)))

(define (f1)  (umatch (#:mode -) (1) (X X)))
(define (f2)  (umatch (#:mode -) ('(1 a)) ((X _) X)))
(define (f3)  (umatch (#:mode -) ('(1 1)) ((X X) X)))
(define (f4)  (umatch (#:mode -) ('(a b)) ((X X) X) (_ 1)))
(define (f5)  (umatch (#:mode -) ('(a a)) ((and X (Y Y)) X)))
(define (f6)  (umatch (#:mode -) ("hello") (_ #t)))
(define (f7)  (umatch (#:mode -) ('foo) ('foo #t)))
(define (f8)  (umatch (#:mode -) ("bar") ("bar" #t)))
(define (f9)  (umatch (#:mode -) (777) (777 #t)))
(define (f10) (umatch (#:mode -) (#\g) (#\g #t)))
(define (f11) (umatch (#:mode -) ('(a b c)) ('(a b c) #t)))

(with-test-prefix "basic mode -, 1 arg,  umatch usage -"
   (pass-if  "(X X)" 
	     (equal? 1 (f1)))
   
   (pass-if  "((X _) X)"
	     (equal? 1 (f2)))
   
   (pass-if  "((X X) X)"
	     (equal? 1 (f3)))
   
   (pass-if  "((X X) X) (_ 1)"
	     (equal? 1 (f4)))
   
   (pass-if  "((and X (Y Y)) X)"
	     (equal? '(a a) (f5)))
   
   (pass-if "wildcard" (f6))
	    
   
   (pass-if "symbol" (f7))

   
   (pass-if "string" (f8))

   
   (pass-if "number" (f9))

   
   (pass-if "char" (f10))

   
   (pass-if "sexp" (f11)))


(define (f1)  (umatch (#:raw #:mode +) (1) (X X)))
(define (f2)  (umatch (#:raw #:mode +) ('(1 a)) ((X _) X)))
(define (f3)  (umatch (#:raw #:mode +) ('(1 1)) ((X X) X)))
(define (f4)  (umatch (#:raw #:mode +) ('(a b)) ((X X) X) (_ 1)))
(define (f5)  (umatch (#:raw #:mode +) ('(a a)) ((and X (Y Y)) X)))
(define (f6)  (umatch (#:raw #:mode +) ("hello") (_ #t)))
(define (f7)  (umatch (#:raw #:mode +) ('foo) ('foo #t)))
(define (f8)  (umatch (#:raw #:mode +) ("bar") ("bar" #t)))
(define (f9)  (umatch (#:raw #:mode +) (777) (777 #t)))
(define (f10) (umatch (#:raw #:mode +) (#\g) (#\g #t)))
(define (f11) (umatch (#:raw #:mode +) ('(a b c)) ('(a b c) #t)))

(with-test-prefix "basic #raw mode +, 1 arg,  umatch usage *"
   (pass-if  "(X X)" 
	     (equal? 1 (f1)))
   
   (pass-if  "((X _) X)"
	     (equal? 1 (f2)))
   
   (pass-if  "((X X) X)"
	     (equal? 1 (f3)))
   
   (pass-if  "((X X) X) (_ 1)"
	     (equal? 1 (f4)))
   
   (pass-if  "((and X (Y Y)) X)"
	     (equal? '(a a) (f5)))
   
   (pass-if "wildcard" (f6))
	    
   
   (pass-if "symbol" (f7))

   
   (pass-if "string" (f8))

   
   (pass-if "number" (f9))

   
   (pass-if "char" (f10))

   
   (pass-if "sexp" (f11)))
