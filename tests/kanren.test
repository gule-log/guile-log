(define-module (kanren)
  #:use-module (logic guile-log kanren)
  #:use-module ((logic guile-log umatch) #:select (gp-clear *current-stack*))
  #:use-module (ice-9 pretty-print)
  #:use-module (test-suite lib))

(define (time x) x)

#;
(define-syntax test-check
  (syntax-rules ()
    ((_ title tested-expression expected-result)
     (begin
       (format #t "Testing ~a~%" title)
       (let* ((expected expected-result)
              (produced tested-expression))
         (or (equal? expected produced)
             (error
              (format #f
                      "(Failed: ~a~%Expected: ~a~%Computed: ~a~%"
                      'tested-expression expected produced))))))))

(define-syntax test-check
  (syntax-rules ()
    ((_ title x y)
     (begin
       (gp-clear *current-stack*)
       (with-test-prefix "start"
         (pass-if (format #f "~a" 'x)
                  (equal? x y)))))))


(let*
  ((parents-of-scouts
    (extend-relation (a1 a2)
      (fact () 'sam 'rob)
      (fact () 'roz 'sue)
      (fact () 'rob 'sal)))
   (parents-of-athletes
    (extend-relation (a1 a2)
      (fact () 'sam 'roz)
      (fact () 'roz 'sue)
      (fact () 'rob 'sal)))

   (busy-parents
    (intersect-relation (a1 a2) parents-of-scouts parents-of-athletes))

   (conscientious-parents
    (extend-relation (a1 a2) parents-of-scouts parents-of-athletes)))
   

  (test-check 'test-conscientious-parents
     (solve 7 (x y) (conscientious-parents x y))
    '((sam rob)
      (roz sue)
      (rob sal)
      (sam roz)
      (roz sue)
      (rob sal))))


(let* ((father  
	 (extend-relation (a1 a2)
	   (relation () (to-show 'jon 'sam))
	   (relation () (to-show 'rob 'sal))
	   (relation () (to-show 'rob 'pat))
	   (relation () (to-show 'sam 'rob)))))
	 

  (let
    ((grandpa-sam
      (relation (grandchild)
        (to-show grandchild)
        (exists (parent)
          (all (father 'sam parent)
               (father parent grandchild))))))
    (test-check 'test-grandpa-sam-1
      (solve 6 (y) (grandpa-sam y))
      '(sal pat)))
   

  (let
    ((grandpa-sam
       (relation ((once grandchild))
	 (to-show grandchild)
	 (exists (parent)
	   (all (father 'sam parent)
	        (father parent grandchild))))))
    (test-check 'test-grandpa-sam-1
      (solve 6 (y) (grandpa-sam y))
      '(sal pat)))
    

  (let ((child
	  (relation ((once child) (once dad))
	    (to-show child dad)
	    (father dad child))))
    (test-check 'test-child-1
      (solve 10 (x y) (child x y))
      '((sam jon)
        (sal rob)
        (pat rob)
        (rob sam))))
    

  (let ((grandpa
	  (relation ((once grandad) (once grandchild))
	    (to-show grandad grandchild)
	    (exists (parent)
	      (all
		(father grandad parent)
		(father parent grandchild))))))
    (test-check 'test-grandpa-1
      (solve 4 (x) (grandpa 'sam x))
      '(sal pat)))

  (let ((grandpa-maker
	  (lambda (guide* grandad*)
	    (relation (grandchild)
	      (to-show grandchild)
	      (exists (parent)
		(all
		  (guide* grandad* parent)
		  (guide* parent grandchild)))))))
    (test-check 'test-grandpa-maker-2
      (solve 4 (x) ((grandpa-maker father 'sam) x))
      '(sal pat))))



(let*
    ((father
      (extend-relation (a1 a2)
        (fact () 'jon 'sam)
        (extend-relation (a1 a2)
	  (fact () 'sam 'rob)
          (extend-relation (a1 a2)
	    (fact () 'sam 'roz)
            (extend-relation (a1 a2)
	      (fact () 'rob 'sal)
              (fact () 'rob 'pat))))))
     (mother
      (extend-relation (a1 a2)
	(fact () 'roz 'sue)
	(fact () 'roz 'sid))))
    

  (let*
    ((grandpa/father
       (relation (grandad grandchild)
	 (to-show grandad grandchild)
	 (exists (parent)
	   (all
	     (father grandad parent)
	     (father parent grandchild)))))
     (grandpa/mother
       (relation (grandad grandchild)
	 (to-show grandad grandchild)
	 (exists (parent)
	   (all
	     (father grandad parent)
	     (mother parent grandchild)))))
     (grandpa
      (extend-relation (a1 a2) grandpa/father grandpa/mother)))

    (test-check 'test-grandpa-5
      (solve 10 (y) (grandpa 'sam y))
      '(sal pat sue sid)))
    

  ; A relation is just a function
  (let
    ((grandpa-sam
       (let ((r (relation (child)
		  (to-show child)
		  (exists (parent)
		    (all
		      (father 'sam parent)
		      (father parent child))))))
	 (relation (child)
	   (to-show child)
	   (r child)))))

    (test-check 'test-grandpa-55
      (solve 6 (y) (grandpa-sam y))
      '(sal pat)))

; Now we don't need it
  (let*
     ((grandpa/father
       (relation (grandad grandchild)
	 (to-show grandad grandchild)
	 (exists (parent)
	   (all!
	     (father grandad parent)
	     (father parent grandchild)))))

      (grandpa/mother
	(relation (grandad grandchild)
	  (to-show grandad grandchild)
	  (exists (parent)
	    (all
	      (father grandad parent)
	      (mother parent grandchild)))))

      (grandpa
	(lift-to-relations (a1 a2)
	  (all!
	    (extend-relation (a1 a2) grandpa/father grandpa/mother)))))
      
    (test-check 'test-grandpa-8
      (solve 10 (x y) (grandpa x y))
      '((jon rob))))
    
  
; The solution that used to require cuts
; (define grandpa/father
;   (relation/cut cut (grandad grandchild)
;     (to-show grandad grandchild)
;     (exists (parent)
;       (all cut (father grandad parent) (father parent grandchild)))))

  (let
    ((grandpa/father
       (relation (grandad grandchild)
	 (to-show grandad grandchild)
	 (exists (parent)
	   (all
	     (father grandad parent) (father parent grandchild)))))

      (grandpa/mother
	(relation (grandad grandchild)
	  (to-show grandad grandchild)
	  (exists (parent)
	    (all
	      (father grandad parent) (mother parent grandchild)))))
      )

; Properly, this requires soft cuts, aka *->, or Mercury's
; if-then-else. But we emulate it...

    (let
      ((grandpa
	 (let-gls (a1 a2) ((grandpa/father grandpa/father)
			   (grandpa/mother grandpa/mother))
	   (if-only (succeeds grandpa/father) grandpa/father grandpa/mother))))
	
      (test-check 'test-grandpa-10
	(solve 10 (x y) (grandpa x y))
	'((jon rob)
	  (jon roz)
          (sam sal)
          (sam pat)))
      (test-check 'test-grandpa-10-1
	(solve 10 (x) (grandpa x 'sue))
	'(sam)))
      
  
; The same as above, with if-all! -- just to test the latter.
    (let
      ((grandpa
	 (let-gls (a1 a2) ((grandpa/father grandpa/father)
			    (grandpa/mother grandpa/mother))
	   (if-only (all! (succeeds grandpa/father) (succeeds grandpa/father))
	     grandpa/father grandpa/mother))))

      (test-check 'test-grandpa-10
	(solve 10 (x y) (grandpa x y))
	'((jon rob)
          (jon roz)
          (sam sal)
          (sam pat)))
      
      (test-check 'test-grandpa-10-1
	(solve 10 (x) (grandpa x 'sue))
	'(sam)))
     


; Now do it with soft-cuts
  
    (let
      ((grandpa
	 (let-gls (a1 a2) ((grandpa/father grandpa/father)
			    (grandpa/mother grandpa/mother))
            (if-some grandpa/father succeed grandpa/mother))))
	
      (test-check 'test-grandpa-10-soft-cut
	(solve 10 (x y) (grandpa x y))
	'((jon rob)
	  (jon roz)
	  (sam sal)
	  (sam pat))))
  
  
    (let*
       ((a-grandma
	 (relation (grandad grandchild)
	   (to-show grandad grandchild)
	   (exists (parent)
	     (all! (mother grandad parent)))))
	(no-grandma-grandpa
	  (let-gls (a1 a2) ((a-grandma a-grandma)
			     (grandpa (lift-to-relations (a1 a2)
					(all!
					  (extend-relation (a1 a2) 
					    grandpa/father grandpa/mother)))))
	    (if-only a-grandma fail grandpa))))
	
      (test-check 'test-no-grandma-grandpa-1
	(solve 10 (x) (no-grandma-grandpa 'roz x))
	'()))))


(test-check 'test-pred1
  (let ((test1
	  (lambda (x)
	    (any (predicate (< 4 5))
	      (== x (< 6 7))))))
    (solution (x) (test1 x)))
  'v.0)

(test-check 'test-pred2
    (let ((test2
           (lambda (x)
             (any (predicate (< 5 4))
                  (== x (< 6 7))))))
    (solution (x) (test2 x)))
  '#t)

(test-check 'test-pred3
  (let ((test3
	  (lambda (x y)
	    (any
	      (== x (< 5 4))
	      (== y (< 6 7))))))
    (solution (x y) (test3 x y)))
  `(#f v.0))

(test-check 'test-Seres-Spivey
  (let ((father
         (lambda (dad child)
           (any
            (all (== dad 'jon) (== child 'sam))
            (all (== dad 'sam) (== child 'rob))
            (all (== dad 'sam) (== child 'roz))
            (all (== dad 'rob) (== child 'sal))
            (all (== dad 'rob) (== child 'pat))
            (all (== dad 'jon) (== child 'hal))
            (all (== dad 'hal) (== child 'ted))
            (all (== dad 'sam) (== child 'jay))))))
    (letrec
        ((ancestor
          (lambda (old young)
            (any
             (father old young)
             (exists (not-so-old)
               (all (father old not-so-old)
                 (ancestor not-so-old young)))))))
      (pk (solve 20 (x) (ancestor 'jon x)))))
  '(sam
    hal 
    rob
    roz
    jay
    sal
    pat
    ted))


(define towers-of-hanoi
  (letrec
      ((move
         (extend-relation (a1 a2 a3 a4)
           (fact () 0 _ _ _)
           (relation (n a b c)
             (to-show n a b c)
             (project (n)
               (if-only (predicate (positive? n))
                 (let ((m (- n 1)))
                   (all 
                     (move m a c b)
                     (project (a b)
                       (begin
                         (format #t "Move a disk from ~a to ~a~%" a b)
                         (move m c b a)))))))))))
    (relation (n)
      (to-show n)
      (move n 'left 'middle 'right))))

(format #t "~%test-towers-of-hanoi with 3 disks: ~a~%~%"
  (solution () (towers-of-hanoi 3)))


(test-check 'test-fun-resubst
  (reify
    (let ((j (relation (x w z)
	       (to-show z)
	       (let ((x 4)
                     (w 3))
                 (== z (cons x w))))))
      (solve 4 (q) (j q))))
  '((4 . 3)))

(define towers-of-hanoi-path
  (let ((steps '()))
    (let ((push-step (lambda (x y) (set! steps (cons `(,x ,y) steps)))))
      (letrec
          ((move
             (extend-relation (a1 a2 a3 a4)
               (fact () 0 _ _ _)
               (relation (n a b c)
                 (to-show n a b c)
                 (project (n)
                   (if-only (predicate (positive? n))
                     (let ((m (- n 1)))
                       (all
                         (move m a c b)
                         (project (a b)
                           (begin
                             (push-step a b)
                             (move m c b a)))))))))))
        (relation (n path)
          (to-show n path)
          (begin
            (set! steps '())
            (any
              (fails (move n 'l 'm 'r))
              (== path (reverse steps)))))))))

(test-check 'test-towers-of-hanoi-path
  (solution (path) (towers-of-hanoi-path 3 path))
  '((l m) (l r) (m r) (l m) (r l) (r m) (l m)))

; This is not supporeted technology
#;
(let
  ((parents-of-scouts
     (extend-relation (a1 a2)
       (fact () 'sam 'rob)
       (fact () 'roz 'sue)
       (fact () 'rob 'sal)))
    (fathers-of-cubscouts
      (extend-relation (a1 a2)
	(fact () 'sam 'bob)
	(fact () 'tom 'adam)
	(fact () 'tad 'carl)))
    )

  (test-check 'test-partially-eval-sgl
   (let-lv (p1 p2)
      (let* ((parents-of-scouts-sgl
              (parents-of-scouts p1 p2))

             (cons@ (lambda (x y) (cons x y)))
             (split1 (partially-eval-sgl parents-of-scouts-sgl
                                         cons@ 
                                         (lambda () '())))
             (a1 (car split1))
             (split2 (partially-eval-sgl (cdr split1) 
                                         cons@
                                         (lambda () '())))
             (a2 (car split2))
             (split3 (partially-eval-sgl (cdr split2) 
                                         cons@
                                         (lambda () '())))
             (a3 (car split3)))
	(list a1 a2 a3))))
  '(((p1.0 sam) (p2.0 rob)) ((p1.0 roz) (p2.0 sue)) ((p1.0 rob) (p2.0 sal))))



;------------------------------------------------------------------------

          
(test-check 'unification-of-free-vars-1
  (solve 1 (x)
    (let-lv (y)
      (all!! (== x y) (== y 5))))
  '(5))

(test-check 'unification-of-free-vars-2
  (solve 1 (x)
    (let-lv (y)
      (all!! (== y 5) (== x y))))
  '(5))

(test-check 'unification-of-free-vars-3
  (solve 1 (x)
    (let-lv (y)
      (all!! (== y x) (== y 5))))
  '(5))

(test-check 'unification-of-free-vars-3
  (solve 1 (x)
    (let-lv (y)
      (all!! (== x y) (== y 5) (== x y))))
  '(5))

(test-check 'unification-of-free-vars-4
  (solve 1 (x)
    (exists (y)
      (all! (== y x) (== y 5) (== x y))))
  '(5))


(letrec
  ((concat
     (lambda (xs ys)
       (cond
	 ((null? xs) ys)
	 (else (cons (car xs) (concat (cdr xs) ys)))))))

  (test-check 'test-concat-as-function
    (concat '(a b c) '(u v))
    '(a b c u v))

  (test-check 'test-fun-concat
    (solve 1 (q)
      (== q (concat '(a b c) '(u v))))
    '((a b c u v))))



; Now the same with the relation
(letrec
  ((concat
     (extend-relation (a1 a2 a3)
       (fact (xs) '() xs xs)
       (relation (x xs (once ys) zs)
	 (to-show `(,x . ,xs) ys `(,x . ,zs))
	 (concat xs ys zs)))))
  (test-check 'test-concat
    (time
     (and
      (equal?
       (solve 6 (q) (concat '(a b c) '(u v) q))
       '((a b c u v)))

      (equal?
       (solve 6 (q) (concat '(a b c) q '(a b c u v)))
       '((u v)))
      (equal?
       (solve 6 (q) (concat q '(u v) '(a b c u v)))
        '((a b c)))
      (equal?
       (solve 6 (q r) (concat q r '(a b c u v)))
       '((() (a b c u v))
         ((a) (b c u v))
          ((a b) (c u v))
          ((a b c) (u v))
          ((a b c u) (v))
          ((a b c u v) ())))

      (equal?
       (solve 6 (q r s) (concat q r s))
	'((()                 v.0  v.0)
	  ((v.0)              v.1 (v.0         . v.1))
	  ((v.0 v.1)          v.2 (v.0 v.1     . v.2))
	  ((v.0 v.1 v.2)      v.3 (v.0 v.1 v.2 . v.3))
	  ((v.0 v.1 v.2 v.3)  v.4 (v.0 v.1 v.2 v.3 . v.4))
	  ((v.0 v.1 v.2 v.3 v.4) v.5
           (v.0 v.1 v.2 v.3 v.4 . v.5))))


      (equal?
       (solve 6 (q r) (concat q '(u v) `(a b c . ,r)))
       '(((a b c) (u v))
         ((a b c v.0) (v.0 u v))
         ((a b c v.0 v.1) (v.0 v.1 u v))
         ((a b c v.0 v.1 v.2) (v.0 v.1 v.2 u v))
         ((a b c v.0 v.1 v.2 v.3) (v.0 v.1 v.2 v.3 u v))
         ((a b c v.0 v.1 v.2 v.3 v.4)
          (v.0 v.1 v.2 v.3 v.4 u v))))

      (equal?
       (solve 6 (q) (concat q '() q))
       '(()
         (v.0)
         (v.0 v.1)
         (v.0 v.1 v.2)
         (v.0 v.1 v.2 v.3)
         (v.0 v.1 v.2 v.3 v.4)))))
    #t))

; Extending relations in truly mathematical sense.
; First, why do we need this.
(let*
  ((fact1 (fact () 'x1 'y1))
   (fact2 (fact () 'x2 'y2))
   (fact3 (fact () 'x3 'y3))
   (fact4 (fact () 'x4 'y4))

   ; R1 and R2 are overlapping
   (R1 (extend-relation (a1 a2) fact1 fact2))
   (R2 (extend-relation (a1 a2) fact1 fact3))
  )
   ; Infinitary relation
   ; r(z,z).
   ; r(s(X),s(Y)) :- r(X,Y).
  (letrec
   ((Rinf
     (extend-relation (a1 a2)
       (fact () 'z 'z)
       (relation (x y t1 t2)
	 (to-show t1 t2)
	 (all
	   (== t1 `(s ,x))
	   (== t2 `(s ,y))
	   (Rinf x y)))))
    )

  (format #t "~% R1:~%")
  (pretty-print (solve 10 (x y) (R1 x y)))
  (format #t "~% R2:~%")
  (pretty-print (solve 10 (x y) (R2 x y)))
  (format #t "~% R1 + R2:~%")
  (pretty-print 
   (solve 10 (x y)
          ((extend-relation (a1 a2) R1 R2) x y)))
  (format #t "~% Rinf:~%")
  (time (pretty-print (solve 5 (x y) (Rinf x y))))
  (format #t "~%Rinf+R1: Rinf starves R1:~%")
  (time
   (pretty-print 
    (solve 5 (x y)
           ((extend-relation (a1 a2) Rinf R1) x y))))

; Solving the starvation problem: extend R1 and R2 so that they
; are interleaved
; ((sf-extend R1 R2) sk fk)
; (R1 sk fk)
; If R1 fails, we try the rest of R2
; If R1 succeeds, it executes (sk fk)
; with fk to re-prove R1. Thus fk is the "rest" of R1
; So we pass sk (lambda () (run-rest-of-r2 interleave-with-rest-of-r1))
; There is a fixpoint in the following algorithm!
; Or a second-level shift/reset!

  (test-check "Rinf+R1"
    (time 
      (solve 7 (x y)
	(any-interleave (Rinf x y) (R1 x y))))
    '((z z) (x1 y1) ((s z) (s z)) (x2 y2) ((s (s z)) (s (s z))) 
      ((s (s (s z))) (s (s (s z)))) ((s (s (s (s z)))) (s (s (s (s z)))))))

  (test-check "R1+Rinf"
    (time 
      (solve 7 (x y)
	(any-interleave (R1 x y) (Rinf x y))))
    '((x1 y1) (z z) (x2 y2) ((s z) (s z)) ((s (s z)) (s (s z))) 
      ((s (s (s z))) (s (s (s z)))) ((s (s (s (s z)))) (s (s (s (s z)))))))

  (test-check "R2+R1"
    (solve 7 (x y)
      (any-interleave (R2 x y) (R1 x y)))
    '((x1 y1) (x1 y1) (x3 y3) (x2 y2)))

  (test-check "R1+fact3"
    (solve 7 (x y)
      (any-interleave (R1 x y) (fact3 x y)))
    '((x1 y1) (x3 y3) (x2 y2)))

  (test-check "fact3+R1"
    (solve 7 (x y)
      (any-interleave (fact3 x y) (R1 x y)))
    '((x3 y3) (x1 y1) (x2 y2)))

; testing all-interleave
  (test-check 'all-interleave-1
    (solve 100 (x y z)
      (all-interleave
	(any (== x 1) (== x 2))
	(any (== y 3) (== y 4))
	(any (== z 5) (== z 6) (== z 7))))
    '((1 3 5) (2 3 5) (1 4 5) (1 3 6) (2 4 5) (2 3 6) 
      (1 4 6) (1 3 7) (2 4 6) (2 3 7) (1 4 7) (2 4 7)))

  (test-check "R1 * Rinf: clearly starvation"
    (solve 5 (x y u v)
      (all (R1 x y) (Rinf u v)))
  ; indeed, only the first choice of R1 is apparent
    '((x1 y1 z z) 
      (x1 y1 (s z) (s z)) 
      (x1 y1 (s (s z)) (s (s z))) 
      (x1 y1 (s (s (s z))) (s (s (s z)))) 
      (x1 y1 (s (s (s (s z)))) (s (s (s (s z)))))))



  (test-check "R1 * Rinf: interleaving"
    (solve 5 (x y u v)
      (all-interleave (R1 x y) (Rinf u v)))
  ; both choices of R1 are apparent 
    '((x1 y1 z z) 
      (x2 y2 z z) 
      (x1 y1 (s z) (s z)) 
      (x2 y2 (s z) (s z)) 
      (x1 y1 (s (s z)) (s (s z)))))

  ;; Test for nonoverlapping.

  
  (format #t "~%any-union~%")
  (test-check "R1+R2"
    (solve 10 (x y)
      (any-union (R1 x y) (R2 x y)))
    '((x1 y1) (x2 y2) (x3 y3)))

  (test-check "R2+R1"
    (solve 10 (x y)
      (any-union (R2 x y) (R1 x y)))
    '((x1 y1) (x3 y3) (x2 y2)))
  
  (test-check "R1+R1"
    (solve 10 (x y)
      (any-union (R1 x y) (R1 x y)))
    '((x1 y1) (x2 y2)))

  (test-check "Rinf+R1"
    (solve 7 (x y)
      (any-union (Rinf x y) (R1 x y)))
    '((z z) (x1 y1) ((s z) (s z)) (x2 y2) 
      ((s (s z)) (s (s z))) ((s (s (s z))) (s (s (s z)))) 
      ((s (s (s (s z)))) (s (s (s (s z)))))))

  
  (test-check "R1+RInf"
    (solve 7 (x y)
      (any-union (R1 x y) (Rinf x y)))
    '((x1 y1) (z z) (x2 y2) ((s z) (s z)) ((s (s z)) (s (s z))) 
      ((s (s (s z))) (s (s (s z)))) ((s (s (s (s z)))) (s (s (s (s z)))))))


; Infinitary relation Rinf2
; r(z,z).
; r(s(s(X)),s(s(Y))) :- r(X,Y).
; Rinf2 overlaps with Rinf in the infinite number of points
  (letrec
    ((Rinf2
       (extend-relation (a1 a2)
	 (fact () 'z 'z)
	 (relation (x y t1 t2)
	   (to-show t1 t2)
	   (all
	     (== t1 `(s (s ,x)))
	     (== t2 `(s (s ,y)))
	     (Rinf2 x y)))))
      )
    (test-check "Rinf2"
      (solve 5 (x y) (Rinf2 x y))
      '((z z) 
        ((s (s z)) (s (s z))) 
        ((s (s (s (s z)))) (s (s (s (s z))))) 
        ((s (s (s (s (s (s z)))))) (s (s (s (s (s (s z))))))) 
        ((s (s (s (s (s (s (s (s z)))))))) (s (s (s (s (s (s (s (s z)))))))))))

    (test-check "Rinf+Rinf2"
      (solve 9 (x y)
	(any-union (Rinf x y) (Rinf2 x y)))
      '((z z)
	 ((s z) (s z))
	 ((s (s z)) (s (s z)))
	 ((s (s (s (s z)))) (s (s (s (s z)))))
	 ((s (s (s z))) (s (s (s z))))
	 ((s (s (s (s (s (s z))))))
	   (s (s (s (s (s (s z)))))))
	 ((s (s (s (s (s (s (s (s z))))))))
	   (s (s (s (s (s (s (s (s z)))))))))
	 ((s (s (s (s (s z))))) (s (s (s (s (s z))))))
	 ((s (s (s (s (s (s (s (s (s (s z))))))))))
	   (s (s (s (s (s (s (s (s (s (s z)))))))))))))
    
    (test-check "Rinf2+Rinf"
      (solve 9 (x y)
	(any-union (Rinf2 x y) (Rinf x y)))
      '((z z)
	 ((s z) (s z))
	 ((s (s z)) (s (s z)))
	 ((s (s (s z))) (s (s (s z))))
	 ((s (s (s (s z)))) (s (s (s (s z)))))
	 ((s (s (s (s (s z))))) (s (s (s (s (s z))))))
	 ((s (s (s (s (s (s z))))))
	   (s (s (s (s (s (s z)))))))
	 ((s (s (s (s (s (s (s z)))))))
	   (s (s (s (s (s (s (s z))))))))
	 ((s (s (s (s (s (s (s (s z))))))))
	   (s (s (s (s (s (s (s (s z))))))))))))))



(format #t "Append with limited depth~%")

; In Prolog, we normally write:
; append([],L,L).
; append([X|L1],L2,[X|L3]) :- append(L1,L2,L3).
;
; If we switch the clauses, we get non-termination.
; In our system, it doesn't matter!

(letrec
  ((extend-clause-1
     (relation (l)
       (to-show '() l l)
       succeed))
   (extend-clause-2
     (relation (x l1 l2 l3)
       (to-show `(,x . ,l1) l2 `(,x . ,l3))
       (extend-rel l1 l2 l3)))
   (extend-rel
       (extend-relation-with-recur-limit 5 (a b c)
	 extend-clause-1
	 extend-clause-2))
    )

  ; Note (solve 100 ...)
  ; Here 100 is just a large number: we want to print all solutions
    (format #t "~%Extend: clause1 first: ~a~%"
            (solve 100 (a b c) (extend-rel a b c))))
   

(letrec
  ((extend-clause-1
     (relation (l)
       (to-show '() l l)
       succeed))
   (extend-clause-2
     (relation (x l1 l2 l3)
       (to-show `(,x . ,l1) l2 `(,x . ,l3))
       (extend-rel l1 l2 l3)))
   (extend-rel
     (extend-relation-with-recur-limit 3 (a b c)
       extend-clause-2
       extend-clause-1)))

  (format #t "~%Extend: clause2 first. In Prolog, it would diverge!: ~a~%" 
    (solve 100 (a b c) (extend-rel a b c))))


(letrec
  ((base-+-as-relation
     (fact (n) 'zero n n))
   (recursive-+-as-relation
     (relation (n1 n2 n3)
       (to-show `(succ ,n1) n2 `(succ ,n3))
       (+-as-relation n1 n2 n3)))
   ; Needed eta-expansion here: otherwise, SCM correctly reports
   ; an error (but Petite doesn't, alas)
   ; This is a peculiarity of extend-relation as a macro
   ; Potentially, we need the same approach as in minikanren
   (+-as-relation
     (extend-relation (a1 a2 a3)
       (lambda (a1 a2 a3) (base-+-as-relation a1 a2 a3))
       (lambda (a1 a2 a3) (recursive-+-as-relation a1 a2 a3))
       ))
    )

  (test-check "Addition"
    (solve 20 (x y)
      (+-as-relation x y '(succ (succ (succ (succ (succ zero)))))))
    '((zero (succ (succ (succ (succ (succ zero))))))
      ((succ zero) (succ (succ (succ (succ zero))))) 
      ((succ (succ zero)) (succ (succ (succ zero)))) 
      ((succ (succ (succ zero))) (succ (succ zero))) 
      ((succ (succ (succ (succ zero)))) (succ zero)) 
      ((succ (succ (succ (succ (succ zero))))) zero))))


