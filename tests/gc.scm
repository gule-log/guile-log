;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log umatch))
(use-modules (logic guile-log))

(compile-prolog-string "
inc(X,Y) :- Y is X + 1.

test(N)   :- test(N,0).
test(N,X) :- X < N, inc(X,Y), test(N,Y).

test3(N)   :- test3(N,0).
test3(N,X) :- X < N, Y is X + 1, test3(N,Y).

test2(N)   :- test2(N,0).
test2(N,X) :- X < N -> (inc(X,Y), test2(N,Y)) ; (write('goaaaaaaaal'),nl).
")


(<define> (gl-test m)
  (<recur> lp ((n 0))
    (when (< n m)
      (<var> (x)
        (<=> x n)
        (lp (+ n 1))))))

(define store '())
(<define> (gl-test2 m)
  (<code> (set! store '()))
  (<recur> lp ((n 0))
    (when (< n m)
      (<var> (x)
        (<code> (set! store (cons x store)))

        (<=> x n)
        (lp (+ n 1))))))

(<define> (gl-test3 m)
  (<recur> lp ((n 0))
    (<if> (when (< n m))
          (<var> (x)
            (<=> x n)
            (lp (+ n 1)))
          (<pp> 'finish))))
