:- module(if, 
	  [fast_compile/1,
           eval/1,
	   term(compile),
           if/1,
           else/0,
	   elseif/1,
	   endif/0,
	   term(ifmacro)
	 ]).

%%
:- dynamic(fast_compile/1).

nocolon(:) :- !, fail.
nocolon(_).

compile((:- eval(X)        ), []) :- X.
compile((:- fast_compile(X)), []) :- 
   X=="true"  ->
      asserta((fast_compile(true) :- !));
   X=="false" ->
      asserta((fast_compile(true) :- !, fail));
   asserta((fast_compile(X) :- !)).

compile((:- X), _) :- !,fail.
compile((?  X), _) :- !,fail.
compile((-  X), _) :- !,fail.
compile((X :- Y),[X :- Y]) :- !,
    X =.. [F|_], nocolon(F), (fast_compile(true) ; fast_compile(F)),!,write(did1(F)),nl.
compile(X,[X]) :-
    X =.. [F|_], nocolon(F), (fast_compile(true) ; fast_compile(F)),!,write(did2(F)),nl.
  

%% This is the dynamic.
:- dynamic(doif/1).
doif(1).
setif(X) :- asserta((doif(Y) :- !, Y=X)).

ifmacro((:- if(Code)),[]) :- !,
    (
        doif(1) ->
           (Code -> true ; setif(0));
        doif(I) ->
            (
                II is I - 1,
                setif(II)
            )
    ).
            
ifmacro((:- endif), []) :- !,
    doif(I),
    (I = 1 -> J is 1 ; J is I + 1),
    setif(J).

ifmacro((:- elseif(Code)), []) :- !,
    (doif(0) -> (Code -> setif(1) ; true) ; true).

ifmacro((:- else), []) :- !,
    (doif(0) -> setif(1) ; true).

ifmacro(X,Y) :- !,
    (doif(1) -> fail ; Y=[]).
