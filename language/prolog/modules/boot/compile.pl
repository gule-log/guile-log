module_dynamic(glob/2)
glob(#f).


termcomp(:- all_fast(X), [])  :- asserta(glob(X)).
termcomp(:- fast(F,X)  , [])  :- asserta(glob(F,X)).
termcomp(:- Z, _) :- !,fail.  
termcomp( - Z, _) :- !,fail.  
termcomp(Q, Z) :-
    (F(|X) :- B) = Q ->
    (    
      glob(F,A)  -> (A=#t -> Z=Q ; fail)                                    ;
      glob(#t)   -> asserta(glob(F,#t)), term_comp(Q,Z)
    ) ;

    F(|X) = Q ->
      glob(F,A)  -> (A=#t -> Z=Q ; fail).
