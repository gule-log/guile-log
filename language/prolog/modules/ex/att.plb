:- module(att,[domain/2]).

:- use_module(library(ordsets)).

domain(X, Dom) :-
        var(Dom), !,
        get_attr(X, domainI, Dom).
domain(X, List) :-
        list_to_ord_set(List, Domain),
        put_attr(Y, domainI, Domain),
        X = Y.

%       An attributed variable with attribute value Domain has been
%       assigned the value Y
attribute_goals(X) -->
        { get_attr(X, domainI, List) },
        [domain(X, List)].

-attach_attribute_cstor(attribute_goals).
domainI(Domain, Y, #f).
domainI(Domain, Y, #t) :-
        (   get_attr(Y, domainI, Dom2)
        ->  ord_intersection(Domain, Dom2, NewDomain),
            (   NewDomain == []
            ->  fail
            ;   NewDomain = [Value]
            ->  Y = Value
            ;   put_attr(Y, domainI, NewDomain)
            )
        ;   var(Y)
        ->  put_attr( Y, domainI, Domain )
        ;   ord_memberchk(Y, Domain)
        ).

%       Translate attributes from this module to residual goals
