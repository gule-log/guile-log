:- module(clpfd3,[parse_clpfd/2,make_parse_reified/1,make_matches/1]).

:- use_module(library(apply)).
%:- use_module(library(apply_macros)).
%:- use_module(library(assoc))
:- use_module(library(vhash)).
:- use_module(library(error)).
:- use_module(library(lists)).
:- use_module(library(pairs)).
:- use_module(library(clpfd1)).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Given expression E, we obtain the finite domain variable R by
   interpreting a simple committed-choice language that is a list of
   conditions and bodies. In conditions, g(Goal) means literally Goal,
   and m(Match) means that E can be decomposed as stated. The
   variables are to be understood as the result of parsing the
   subexpressions recursively. In the body, g(Goal) means again Goal,
   and p(Propagator) means to attach and trigger once a propagator.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

:- op(800, xfx, =>).

parse_clpfd(E, R,
            [g(cyclic_term(E)) => [g(domain_error(clpfd_expression, E))],
             g(var(E))         => [g(non_monotonic(E)),
                                   g(constrain_to_integer(E)), g(E = R)],
             g(integer(E))     => [g(R = E)],
             ?(E)              => [g(must_be_fd_integer(E)), g(R = E)],
             m(A+B)            => [p(pplus(A, B, R))],
             % power_var_num/3 must occur before */2 to be useful
             g(power_var_num(E, V, N)) => [p(pexp(V, N, R))],
             m(A*B)            => [p(ptimes(A, B, R))],
             m(A-B)            => [p(pplus(R,B,A))],
             m(-A)             => [p(ptimes(-1,A,R))],
             m(max(A,B))       => [g(A #=< ?(R)), g(B #=< R), p(pmax(A, B, R))],
             m(min(A,B))       => [g(A #>= ?(R)), g(B #>= R), p(pmin(A, B, R))],
             m(A mod B)        => [g(B #\= 0), p(pmod(A, B, R))],
             m(A rem B)        => [g(B #\= 0), p(prem(A, B, R))],
             m(abs(A))         => [g(?(R) #>= 0), p(pabs(A, R))],
             m(A/B)            => [g(B #\= 0), p(ptzdiv(A, B, R))],
             m(A//B)           => [g(B #\= 0), p(ptzdiv(A, B, R))],
             m(A div B)        => [g(?(R) #= (A - (A mod B)) // B)],
             m(A rdiv B)       => [g(B #\= 0), p(prdiv(A, B, R))],
             m(A^B)            => [p(pexp(A, B, R))],
             g(true)           => [g(domain_error(clpfd_expression, E))]
            ]).

non_monotonic(X) :-
        (   \+ fd_var(X), current_prolog_flag(clpfd_monotonic, true) ->
            instantiation_error(X)
        ;   true
        ).

% Here, we compile the committed choice language to a single
% predicate, parse_clpfd/2.

make_parse_clpfd(Clauses) :-
        parse_clpfd_clauses(Clauses0),
        maplist(goals_goal, Clauses0, Clauses).

goals_goal((Head :- Goals), (Head :- Body)) :-
        list_goal(Goals, Body).

parse_clpfd_clauses(Clauses) :-
        parse_clpfd(E, R, Matchers),
        maplist(parse_matcher(E, R), Matchers, Clauses).

parse_matcher(E, R, Matcher, Clause) :-
        Matcher = (Condition0 => Goals0),
        phrase((parse_condition(Condition0, E, Head),
                parse_goals(Goals0)), Goals),
        Clause = (parse_clpfd(Head, R) :- Goals).

parse_condition(g(Goal), E, E)       --> [Goal, !].
parse_condition(?(E), _, ?(E))       --> [!].
parse_condition(m(Match), _, Match0) -->
        [!],
        { copy_term(Match, Match0),
          term_variables(Match0, Vs0),
          term_variables(Match, Vs)
        },
        parse_match_variables(Vs0, Vs).

parse_match_variables([], []) --> [].
parse_match_variables([V0|Vs0], [V|Vs]) -->
        [parse_clpfd(V0, V)],
        parse_match_variables(Vs0, Vs).

parse_goals([]) --> [].
parse_goals([G|Gs]) --> parse_goal(G), parse_goals(Gs).

parse_goal(g(Goal)) --> [Goal].
parse_goal(p(Prop)) -->
        [make_propagator(Prop, P)],
        { term_variables(Prop, Vs) },
        parse_init(Vs, P),
        [trigger_once(P)].

parse_init([], _)     --> [].
parse_init([V|Vs], P) --> [init_propagator(V, P)], parse_init(Vs, P).

%?- set_prolog_flag(answer_write_options, [portray(true)]),
%   clpfd:parse_clpfd_clauses(Clauses), maplist(portray_clause, Clauses).




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

trigger_once(Prop) :- trigger_prop(Prop), do_queue.

neq(A, B) :- propagator_init_trigger(pneq(A, B)).

propagator_init_trigger(P) -->
        { term_variables(P, Vs) },
        propagator_init_trigger(Vs, P).

propagator_init_trigger(Vs, P) -->
        [p(Prop)],
        { make_propagator(P, Prop),
          maplist(prop_init(Prop), Vs),
          trigger_once(Prop) }.

propagator_init_trigger(P) :-
        phrase(propagator_init_trigger(P), _).

propagator_init_trigger(Vs, P) :-
        phrase(propagator_init_trigger(Vs, P), _).

prop_init(Prop, V) :- init_propagator(V, Prop).

geq(A, B) :-
        (   fd_get(A, AD, APs) ->
            domain_infimum(AD, AI),
            (   fd_get(B, BD, _) ->
                domain_supremum(BD, BS),
                (   AI cis_geq BS -> true
                ;   propagator_init_trigger(pgeq(A,B))
                )
            ;   (   AI cis_geq n(B) -> true
                ;   domain_remove_smaller_than(AD, B, AD1),
                    fd_put(A, AD1, APs),
                    do_queue
                )
            )
        ;   fd_get(B, BD, BPs) ->
            domain_remove_greater_than(BD, A, BD1),
            fd_put(B, BD1, BPs),
            do_queue
        ;   A >= B
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Naive parsing of inequalities and disequalities can result in a lot
   of unnecessary work if expressions of non-trivial depth are
   involved: Auxiliary variables are introduced for sub-expressions,
   and propagation proceeds on them as if they were involved in a
   tighter constraint (like equality), whereas eventually only very
   little of the propagated information is actually used. For example,
   only extremal values are of interest in inequalities. Introducing
   auxiliary variables should be avoided when possible, and
   specialised propagators should be used for common constraints.

   We again use a simple committed-choice language for matching
   special cases of constraints. m_c(M,C) means that M matches and C
   holds. d(X, Y) means decomposition, i.e., it is short for
   g(parse_clpfd(X, Y)). r(X, Y) means to rematch with X and Y.

   Two things are important: First, although the actual constraint
   functors (#\=2, #=/2 etc.) are used in the description, they must
   expand to the respective auxiliary predicates (match_expand/2)
   because the actual constraints are subject to goal expansion.
   Second, when specialised constraints (like scalar product) post
   simpler constraints on their own, these simpler versions must be
   handled separately and must occur before.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

match_expand(#>=, clpfd_geq_).
match_expand(#=, clpfd_equal_).
match_expand(#\=, clpfd_neq).

symmetric(#=).
symmetric(#\=).

matches([
         m_c(any(X) #>= any(Y), left_right_linsum_const(X, Y, Cs, Vs, Const)) =>
            [g((   Cs = [1], Vs = [A] -> geq(A, Const)
               ;   Cs = [-1], Vs = [A] -> Const1 is -Const, geq(Const1, A)
               ;   Cs = [1,1], Vs = [A,B] -> ?(A) + ?(B) #= ?(S), geq(S, Const)
               ;   Cs = [1,-1], Vs = [A,B] ->
                   (   Const =:= 0 -> geq(A, B)
                   ;   C1 is -Const,
                       propagator_init_trigger(x_leq_y_plus_c(B, A, C1))
                   )
               ;   Cs = [-1,1], Vs = [A,B] ->
                   (   Const =:= 0 -> geq(B, A)
                   ;   C1 is -Const,
                       propagator_init_trigger(x_leq_y_plus_c(A, B, C1))
                   )
               ;   Cs = [-1,-1], Vs = [A,B] ->
                   ?(A) + ?(B) #= ?(S), Const1 is -Const, geq(Const1, S)
               ;   scalar_product_(#>=, Cs, Vs, Const)
               ))],
         m(any(X) - any(Y) #>= integer(C))     => [d(X, X1), d(Y, Y1), g(C1 is -C), p(x_leq_y_plus_c(Y1, X1, C1))],
         m(integer(X) #>= any(Z) + integer(A)) => [g(C is X - A), r(C, Z)],
         m(abs(any(X)-any(Y)) #>= integer(I))  => [d(X, X1), d(Y, Y1), p(absdiff_geq(X1, Y1, I))],
         m(abs(any(X)) #>= integer(I))         => [d(X, RX), g((I>0 -> I1 is -I, RX in inf..I1 \/ I..sup; true))],
         m(integer(I) #>= abs(any(X)))         => [d(X, RX), g(I>=0), g(I1 is -I), g(RX in I1..I)],
         m(any(X) #>= any(Y))                  => [d(X, RX), d(Y, RY), g(geq(RX, RY))],

         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

         m(var(X) #= var(Y))        => [g(constrain_to_integer(X)), g(X=Y)],
         m(var(X) #= var(Y)+var(Z)) => [p(pplus(Y,Z,X))],
         m(var(X) #= var(Y)-var(Z)) => [p(pplus(X,Z,Y))],
         m(var(X) #= var(Y)*var(Z)) => [p(ptimes(Y,Z,X))],
         m(var(X) #= -var(Z))       => [p(ptimes(-1, Z, X))],
         m_c(any(X) #= any(Y), left_right_linsum_const(X, Y, Cs, Vs, S)) =>
            [g(scalar_product_(#=, Cs, Vs, S))],
         m(var(X) #= any(Y))       => [d(Y,X)],
         m(any(X) #= any(Y))       => [d(X, RX), d(Y, RX)],

         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

         m(var(X) #\= integer(Y))             => [g(neq_num(X, Y))],
         m(var(X) #\= var(Y))                 => [g(neq(X,Y))],
         m(var(X) #\= var(Y) + var(Z))        => [p(x_neq_y_plus_z(X, Y, Z))],
         m(var(X) #\= var(Y) - var(Z))        => [p(x_neq_y_plus_z(Y, X, Z))],
         m(var(X) #\= var(Y)*var(Z))          => [p(ptimes(Y,Z,P)), g(neq(X,P))],
         m(integer(X) #\= abs(any(Y)-any(Z))) => [d(Y, Y1), d(Z, Z1), p(absdiff_neq(Y1, Z1, X))],
         m_c(any(X) #\= any(Y), left_right_linsum_const(X, Y, Cs, Vs, S)) =>
            [g(scalar_product_(#\=, Cs, Vs, S))],
         m(any(X) #\= any(Y) + any(Z))        => [d(X, X1), d(Y, Y1), d(Z, Z1), p(x_neq_y_plus_z(X1, Y1, Z1))],
         m(any(X) #\= any(Y) - any(Z))        => [d(X, X1), d(Y, Y1), d(Z, Z1), p(x_neq_y_plus_z(Y1, X1, Z1))],
         m(any(X) #\= any(Y)) => [d(X, RX), d(Y, RY), g(neq(RX, RY))]
        ]).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   We again compile the committed-choice matching language to the
   intended auxiliary predicates. We now must take care not to
   unintentionally unify a variable with a complex term.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

make_matches(Clauses) :-
        matches(Ms),
        findall(F, (member(M=>_, Ms), arg(1, M, M1), functor(M1, F, _)), Fs0),
        sort(Fs0, Fs),
        maplist(prevent_cyclic_argument, Fs, PrevCyclicClauses),
        phrase(matchers(Ms), Clauses0),
        maplist(goals_goal, Clauses0, MatcherClauses),
        append(PrevCyclicClauses, MatcherClauses, Clauses1),
        sort_by_predicate(Clauses1, Clauses).

sort_by_predicate(Clauses, ByPred) :-
        map_list_to_pairs(predname, Clauses, Keyed),
        keysort(Keyed, KeyedByPred),
        pairs_values(KeyedByPred, ByPred).

predname((H:-_), Key)   :- !, predname(H, Key).
predname(M:H, M:Key)    :- !, predname(H, Key).
predname(H, Name/Arity) :- !, functor(H, Name, Arity).

prevent_cyclic_argument(F0, Clause) :-
        match_expand(F0, F),
        Head =.. [F,X,Y],
        Clause = (Head :- (   cyclic_term(X) ->
                              domain_error(clpfd_expression, X)
                          ;   cyclic_term(Y) ->
                              domain_error(clpfd_expression, Y)
                          ;   false
                          )).

matchers([]) --> [].
matchers([Condition => Goals|Ms]) -->
        matcher(Condition, Goals),
        matchers(Ms).

matcher(m(M), Gs) --> matcher(m_c(M,true), Gs).
matcher(m_c(Matcher,Cond), Gs) -->
        [(Head :- Goals0)],
        { Matcher =.. [F,A,B],
          match_expand(F, Expand),
          Head =.. [Expand,X,Y],
          phrase((match(A, X), match(B, Y)), Goals0, [Cond,!|Goals1]),
          phrase(match_goals(Gs, Expand), Goals1) },
        (   { symmetric(F), \+ (subsumes_term(A, B), subsumes_term(B, A)) } ->
            { Head1 =.. [Expand,Y,X] },
            [(Head1 :- Goals0)]
        ;   []
        ).

match(any(A), T)     --> [A = T].
match(var(V), T)     --> [( nonvar(T), T = ?(Var) ->
                            must_be_fd_integer(Var), V = Var
                          ; v_or_i(T), V = T
                          )].
match(integer(I), T) --> [integer(T), I = T].
match(-X, T)         --> [nonvar(T), T = -A], match(X, A).
match(abs(X), T)     --> [nonvar(T), T = abs(A)], match(X, A).
match(X+Y, T)        --> [nonvar(T), T = A + B], match(X, A), match(Y, B).
match(X-Y, T)        --> [nonvar(T), T = A - B], match(X, A), match(Y, B).
match(X*Y, T)        --> [nonvar(T), T = A * B], match(X, A), match(Y, B).

match_goals([], _)     --> [].
match_goals([G|Gs], F) --> match_goal(G, F), match_goals(Gs, F).

match_goal(r(X,Y), F)  --> { G =.. [F,X,Y] }, [G].
match_goal(d(X,Y), _)  --> [parse_clpfd(X, Y)].
match_goal(g(Goal), _) --> [Goal].
match_goal(p(Prop), _) -->
        [make_propagator(Prop, P)],
        { term_variables(Prop, Vs) },
        parse_init(Vs, P),
        [trigger_once(P)].


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% ?X #>= ?Y
%
% X is greater than or equal to Y.

X #>= Y :- clpfd_geq(X, Y).

clpfd_geq(X, Y) :- clpfd_geq_(X, Y), reinforce(X), reinforce(Y).

%% ?X #=< ?Y
%
% X is less than or equal to Y.

X #=< Y :- Y #>= X.

%% ?X #= ?Y
%
% X equals Y.

X #= Y :- clpfd_equal(X, Y).

clpfd_equal(X, Y) :- clpfd_equal_(X, Y), reinforce(X).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Conditions under which an equality can be compiled to built-in
   arithmetic. Their order is significant. (/)/2 becomes (//)/2.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

expr_conds(E, E)                 --> [integer(E)],
        { var(E), !, \+ current_prolog_flag(clpfd_monotonic, true) }.
expr_conds(E, E)                 --> { integer(E) }.
expr_conds(?(E), E)              --> [integer(E)].
expr_conds(-E0, -E)              --> expr_conds(E0, E).
expr_conds(abs(E0), abs(E))      --> expr_conds(E0, E).
expr_conds(A0+B0, A+B)           --> expr_conds(A0, A), expr_conds(B0, B).
expr_conds(A0*B0, A*B)           --> expr_conds(A0, A), expr_conds(B0, B).
expr_conds(A0-B0, A-B)           --> expr_conds(A0, A), expr_conds(B0, B).
expr_conds(A0//B0, A//B)         -->
        expr_conds(A0, A), expr_conds(B0, B),
        [B =\= 0].
expr_conds(A0/B0, AB)            --> expr_conds(A0//B0, AB).
expr_conds(min(A0,B0), min(A,B)) --> expr_conds(A0, A), expr_conds(B0, B).
expr_conds(max(A0,B0), max(A,B)) --> expr_conds(A0, A), expr_conds(B0, B).
expr_conds(A0 mod B0, A mod B)   -->
        expr_conds(A0, A), expr_conds(B0, B),
        [B =\= 0].
expr_conds(A0^B0, A^B)           -->
        expr_conds(A0, A), expr_conds(B0, B),
        [(B >= 0 ; A =:= -1)].

:- multifile
        user:goal_expansion/2.
:- dynamic
        user:goal_expansion/2.

user:goal_expansion(Var in Dom, In) :-
        \+ current_prolog_flag(clpfd_goal_expansion, false),
        (   ground(Dom), Dom = L..U, integer(L), integer(U) ->
            expansion_simpler(
                (   integer(Var) ->
                    between(L, U, Var)
                ;   clpfd:clpfd_in(Var, Dom)
                ), In)
        ;   In = clpfd:clpfd_in(Var, Dom)
        ).
user:goal_expansion(X0 #= Y0, Equal) :-
        \+ current_prolog_flag(clpfd_goal_expansion, false),
        phrase(expr_conds(X0, X), CsX),
        phrase(expr_conds(Y0, Y), CsY),
        list_goal(CsX, CondX),
        list_goal(CsY, CondY),
        expansion_simpler(
                (   CondX ->
                    (   var(Y) -> Y is X
                    ;   CondY -> X =:= Y
                    ;   T is X, clpfd:clpfd_equal(T, Y0)
                    )
                ;   CondY ->
                    (   var(X) -> X is Y
                    ;   T is Y, clpfd:clpfd_equal(X0, T)
                    )
                ;   clpfd:clpfd_equal(X0, Y0)
                ), Equal).
user:goal_expansion(X0 #>= Y0, Geq) :-
        \+ current_prolog_flag(clpfd_goal_expansion, false),
        phrase(expr_conds(X0, X), CsX),
        phrase(expr_conds(Y0, Y), CsY),
        list_goal(CsX, CondX),
        list_goal(CsY, CondY),
        expansion_simpler(
              (   CondX ->
                  (   CondY -> X >= Y
                  ;   T is X, clpfd:clpfd_geq(T, Y0)
                  )
              ;   CondY -> T is Y, clpfd:clpfd_geq(X0, T)
              ;   clpfd:clpfd_geq(X0, Y0)
              ), Geq).
user:goal_expansion(X #=< Y,  Leq) :- user:goal_expansion(Y #>= X, Leq).
user:goal_expansion(X #> Y, Gt)    :- user:goal_expansion(X #>= Y+1, Gt).
user:goal_expansion(X #< Y, Lt)    :- user:goal_expansion(Y #> X, Lt).

expansion_simpler((True->Then0;_), Then) :-
        is_true(True), !,
        expansion_simpler(Then0, Then).
expansion_simpler((False->_;Else0), Else) :-
        is_false(False), !,
        expansion_simpler(Else0, Else).
expansion_simpler((If->Then0;Else0), (If->Then;Else)) :- !,
        expansion_simpler(Then0, Then),
        expansion_simpler(Else0, Else).
expansion_simpler((Var is Expr,Goal), Goal) :-
        ground(Expr), !,
        Var is Expr.
expansion_simpler((Var is Expr,Goal), (Var = Expr,Goal)) :- var(Expr), !.
expansion_simpler(Var is Expr, Var = Expr) :- var(Expr), !.
expansion_simpler(between(L,U,V), Goal) :- maplist(integer, [L,U,V]), !,
        (   between(L,U,V) -> Goal = true
        ;   Goal = false
        ).
expansion_simpler(Goal, Goal).

is_true(true).
is_true(integer(I))  :- integer(I).
:- if(current_predicate(var_property/2)).
is_true(var(X))      :- var(X), var_property(X, fresh(true)).
is_false(integer(X)) :- var(X), var_property(X, fresh(true)).
is_false((A,B))      :- is_false(A) ; is_false(B).
:- endif.
is_false(var(X)) :- nonvar(X).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

linsum(X, S, S)    --> { var(X), !, non_monotonic(X) }, [vn(X,1)].
linsum(I, S0, S)   --> { integer(I), S is S0 + I }.
linsum(?(X), S, S) --> { must_be_fd_integer(X) }, [vn(X,1)].
linsum(-A, S0, S)  --> mulsum(A, -1, S0, S).
linsum(N*A, S0, S) --> { integer(N) }, !, mulsum(A, N, S0, S).
linsum(A*N, S0, S) --> { integer(N) }, !, mulsum(A, N, S0, S).
linsum(A+B, S0, S) --> linsum(A, S0, S1), linsum(B, S1, S).
linsum(A-B, S0, S) --> linsum(A, S0, S1), mulsum(B, -1, S1, S).

mulsum(A, M, S0, S) -->
        { phrase(linsum(A, 0, CA), As), S is S0 + M*CA },
        lin_mul(As, M).

lin_mul([], _)             --> [].
lin_mul([vn(X,N0)|VNs], M) --> { N is N0*M }, [vn(X,N)], lin_mul(VNs, M).

v_or_i(V) :- var(V), !, non_monotonic(V).
v_or_i(I) :- integer(I).

must_be_fd_integer(X) :-
        (   var(X) -> constrain_to_integer(X)
        ;   must_be(integer, X)
        ).

left_right_linsum_const(Left, Right, Cs, Vs, Const) :-
        phrase(linsum(Left, 0, CL), Lefts0, Rights),
        phrase(linsum(Right, 0, CR), Rights0),
        maplist(linterm_negate, Rights0, Rights),
        msort(Lefts0, Lefts),
        Lefts = [vn(First,N)|LeftsRest],
        vns_coeffs_variables(LeftsRest, N, First, Cs0, Vs0),
        filter_linsum(Cs0, Vs0, Cs, Vs),
        Const is CR - CL.
        %format("linear sum: ~w ~w ~w\n", [Cs,Vs,Const]).

linterm_negate(vn(V,N0), vn(V,N)) :- N is -N0.

vns_coeffs_variables([], N, V, [N], [V]).
vns_coeffs_variables([vn(V,N)|VNs], N0, V0, Ns, Vs) :-
        (   V == V0 ->
            N1 is N0 + N,
            vns_coeffs_variables(VNs, N1, V0, Ns, Vs)
        ;   Ns = [N0|NRest],
            Vs = [V0|VRest],
            vns_coeffs_variables(VNs, N, V, NRest, VRest)
        ).

filter_linsum([], [], [], []).
filter_linsum([C0|Cs0], [V0|Vs0], Cs, Vs) :-
        (   C0 =:= 0 ->
            constrain_to_integer(V0),
            filter_linsum(Cs0, Vs0, Cs, Vs)
        ;   Cs = [C0|Cs1], Vs = [V0|Vs1],
            filter_linsum(Cs0, Vs0, Cs1, Vs1)
        ).

gcd([], G, G).
gcd([N|Ns], G0, G) :-
        G1 is gcd(N, G0),
        gcd(Ns, G1, G).

even(N) :- N mod 2 =:= 0.

odd(N) :- \+ even(N).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   k-th root of N, if N is a k-th power.

   TODO: Replace this when the GMP function becomes available.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

integer_kth_root(N, K, R) :-
        (   even(K) ->
            N >= 0
        ;   true
        ),
        (   N < 0 ->
            odd(K),
            integer_kroot(N, 0, N, K, R)
        ;   integer_kroot(0, N, N, K, R)
        ).

integer_kroot(L, U, N, K, R) :-
        (   L =:= U -> N =:= L^K, R = L
        ;   L + 1 =:= U ->
            (   L^K =:= N -> R = L
            ;   U^K =:= N -> R = U
            ;   false
            )
        ;   Mid is (L + U)//2,
            (   Mid^K > N ->
                integer_kroot(L, Mid, N, K, R)
            ;   integer_kroot(Mid, U, N, K, R)
            )
        ).

integer_log_b(N, B, Log0, Log) :-
        T is B^Log0,
        (   T =:= N -> Log = Log0
        ;   T < N,
            Log1 is Log0 + 1,
            integer_log_b(N, B, Log1, Log)
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Largest R such that R^K =< N.

   TODO: Replace this when the GMP function becomes available.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

integer_kth_root_leq(N, K, R) :-
        (   even(K) ->
            N >= 0
        ;   true
        ),
        (   N < 0 ->
            odd(K),
            integer_kroot_leq(N, 0, N, K, R)
        ;   integer_kroot_leq(0, N, N, K, R)
        ).

integer_kroot_leq(L, U, N, K, R) :-
        (   L =:= U -> R = L
        ;   L + 1 =:= U ->
            (   U^K =< N -> R = U
            ;   R = L
            )
        ;   Mid is (L + U)//2,
            (   Mid^K > N ->
                integer_kroot_leq(L, Mid, N, K, R)
            ;   integer_kroot_leq(Mid, U, N, K, R)
            )
        ).

%% ?X #\= ?Y
%
% X is not Y.

X #\= Y :- clpfd_neq(X, Y), do_queue.

% X #\= Y + Z

x_neq_y_plus_z(X, Y, Z) :-
        propagator_init_trigger(x_neq_y_plus_z(X,Y,Z)).

% X is distinct from the number N. This is used internally, and does
% not reinforce other constraints.

neq_num(X, N) :-
        (   fd_get(X, XD, XPs) ->
            domain_remove(XD, N, XD1),
            fd_put(X, XD1, XPs)
        ;   X =\= N
        ).

%% ?X #> ?Y
%
% X is greater than Y.

X #> Y  :- X #>= Y + 1.

%% #<(?X, ?Y)
%
% X is less than Y. In addition to its regular use in problems that
% require it, this constraint can also be useful to eliminate
% uninteresting symmetries from a problem. For example, all possible
% matches between pairs built from four players in total:
%
% ==
% ?- Vs = [A,B,C,D], Vs ins 1..4,
%         all_different(Vs),
%         A #< B, C #< D, A #< C,
%    findall(pair(A,B)-pair(C,D), label(Vs), Ms).
% Ms = [ pair(1, 2)-pair(3, 4),
%        pair(1, 3)-pair(2, 4),
%        pair(1, 4)-pair(2, 3)].
% ==

X #< Y  :- Y #> X.

%% #\ +Q
%
% The reifiable constraint Q does _not_ hold. For example, to obtain
% the complement of a domain:
%
% ==
% ?- #\ X in -3..0\/10..80.
% X in inf.. -4\/1..9\/81..sup.
% ==

#\ Q       :- reify(Q, 0), do_queue.

%% ?P #<==> ?Q
%
% P and Q are equivalent. For example:
%
% ==
% ?- X #= 4 #<==> B, X #\= 4.
% B = 0,
% X in inf..3\/5..sup.
% ==
% The following example uses reified constraints to relate a list of
% finite domain variables to the number of occurrences of a given value:
%
% ==
% :- use_module(library(clpfd)).
%
% vs_n_num(Vs, N, Num) :-
%         maplist(eq_b(N), Vs, Bs),
%         sum(Bs, #=, Num).
%
% eq_b(X, Y, B) :- X #= Y #<==> B.
% ==
%
% Sample queries and their results:
%
% ==
% ?- Vs = [X,Y,Z], Vs ins 0..1, vs_n_num(Vs, 4, Num).
% Vs = [X, Y, Z],
% Num = 0,
% X in 0..1,
% Y in 0..1,
% Z in 0..1.
%
% ?- vs_n_num([X,Y,Z], 2, 3).
% X = 2,
% Y = 2,
% Z = 2.
% ==

L #<==> R  :- reify(L, B), reify(R, B), do_queue.

%% ?P #==> ?Q
%
% P implies Q.

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Implication is special in that created auxiliary constraints can be
   retracted when the implication becomes entailed, for example:

   %?- X + 1 #= Y #==> Z, Z #= 1.
   %@ Z = 1,
   %@ X in inf..sup,
   %@ Y in inf..sup.

   We cannot use propagator_init_trigger/1 here because the states of
   auxiliary propagators are themselves part of the propagator.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

L #==> R   :-
        reify(L, LB, LPs),
        reify(R, RB, RPs),
        append(LPs, RPs, Ps),
        propagator_init_trigger([LB,RB], pimpl(LB,RB,Ps)).

%% ?P #<== ?Q
%
% Q implies P.

L #<== R   :- R #==> L.

%% ?P #/\ ?Q
%
% P and Q hold.

L #/\ R    :- reify(L, 1), reify(R, 1), do_queue.

conjunctive_neqs_var_drep(Eqs, Var, Drep) :-
        conjunctive_neqs_var(Eqs, Var),
        phrase(conjunctive_neqs_vals(Eqs), Vals),
        list_to_domain(Vals, Dom),
        domain_complement(Dom, C),
        domain_to_drep(C, Drep).

conjunctive_neqs_var(V, _) :- var(V), !, false.
conjunctive_neqs_var(L #\= R, Var) :-
        (   var(L), integer(R) -> Var = L
        ;   integer(L), var(R) -> Var = R
        ;   false
        ).
conjunctive_neqs_var(A #/\ B, VA) :-
        conjunctive_neqs_var(A, VA),
        conjunctive_neqs_var(B, VB),
        VA == VB.

conjunctive_neqs_vals(L #\= R) --> ( { integer(L) } -> [L] ; [R] ).
conjunctive_neqs_vals(A #/\ B) -->
        conjunctive_neqs_vals(A),
        conjunctive_neqs_vals(B).

%% ?P #\/ ?Q
%
% P or Q holds. For example, the sum of natural numbers below 1000
% that are multiples of 3 or 5:
%
% ==
% ?- findall(N, (N mod 3 #= 0 #\/ N mod 5 #= 0, N in 0..999,
%                indomain(N)),
%            Ns),
%    sum(Ns, #=, Sum).
% Ns = [0, 3, 5, 6, 9, 10, 12, 15, 18|...],
% Sum = 233168.
% ==

L #\/ R :-
        (   disjunctive_eqs_var_drep(L #\/ R, Var, Drep) -> Var in Drep
        ;   reify(L, X, Ps1),
            reify(R, Y, Ps2),
            propagator_init_trigger([X,Y], reified_or(X,Ps1,Y,Ps2,1))
        ).

disjunctive_eqs_var_drep(Eqs, Var, Drep) :-
        disjunctive_eqs_var(Eqs, Var),
        phrase(disjunctive_eqs_vals(Eqs), Vals),
        list_to_drep(Vals, Drep).

disjunctive_eqs_var(V, _) :- var(V), !, false.
disjunctive_eqs_var(L #= R, Var) :-
        (   var(L), integer(R) -> Var = L
        ;   integer(L), var(R) -> Var = R
        ;   false
        ).
disjunctive_eqs_var(A #\/ B, VA) :-
        disjunctive_eqs_var(A, VA),
        disjunctive_eqs_var(B, VB),
        VA == VB.

disjunctive_eqs_vals(L #= R)  --> ( { integer(L) } -> [L] ; [R] ).
disjunctive_eqs_vals(A #\/ B) -->
        disjunctive_eqs_vals(A),
        disjunctive_eqs_vals(B).

%% ?P #\ ?Q
%
% Either P holds or Q holds, but not both.

L #\ R :- (L #\/ R) #/\ #\ (L #/\ R).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   A constraint that is being reified need not hold. Therefore, in
   X/Y, Y can as well be 0, for example. Note that it is OK to
   constrain the *result* of an expression (which does not appear
   explicitly in the expression and is not visible to the outside),
   but not the operands, except for requiring that they be integers.

   In contrast to parse_clpfd/2, the result of an expression can now
   also be undefined, in which case the constraint cannot hold.
   Therefore, the committed-choice language is extended by an element
   d(D) that states D is 1 iff all subexpressions are defined. a(V)
   means that V is an auxiliary variable that was introduced while
   parsing a compound expression. a(X,V) means V is auxiliary unless
   it is ==/2 X, and a(X,Y,V) means V is auxiliary unless it is ==/2 X
   or Y. l(L) means the literal L occurs in the described list.

   When a constraint becomes entailed or subexpressions become
   undefined, created auxiliary constraints are killed, and the
   "clpfd" attribute is removed from auxiliary variables.

   For (/)/2, mod/2 and rem/2, we create a skeleton propagator and
   remember it as an auxiliary constraint. The pskeleton propagator
   can use the skeleton when the constraint is defined.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

parse_reified(E, R, D,
              [g(cyclic_term(E)) => [g(domain_error(clpfd_expression, E))],
               g(var(E))     => [g(non_monotonic(E)),
                                 g(constrain_to_integer(E)), g(R = E), g(D=1)],
               g(integer(E)) => [g(R=E), g(D=1)],
               ?(E)          => [g(must_be_fd_integer(E)), g(R=E), g(D=1)],
               m(A+B)        => [d(D), p(pplus(A,B,R)), a(A,B,R)],
               m(A*B)        => [d(D), p(ptimes(A,B,R)), a(A,B,R)],
               m(A-B)        => [d(D), p(pplus(R,B,A)), a(A,B,R)],
               m(-A)         => [d(D), p(ptimes(-1,A,R)), a(R)],
               m(max(A,B))   => [d(D), p(pgeq(R, A)), p(pgeq(R, B)), p(pmax(A,B,R)), a(A,B,R)],
               m(min(A,B))   => [d(D), p(pgeq(A, R)), p(pgeq(B, R)), p(pmin(A,B,R)), a(A,B,R)],
               m(abs(A))     => [g(?(R)#>=0), d(D), p(pabs(A, R)), a(A,R)],
               m(A/B)        => [skeleton(A,B,D,R,ptzdiv)],
               m(A//B)       => [skeleton(A,B,D,R,ptzdiv)],
               m(A div B)    => [skeleton(A,B,D,R,pdiv)],
               m(A rdiv B)   => [skeleton(A,B,D,R,prdiv)],
               m(A mod B)    => [skeleton(A,B,D,R,pmod)],
               m(A rem B)    => [skeleton(A,B,D,R,prem)],
               m(A^B)        => [d(D), p(pexp(A,B,R)), a(A,B,R)],
               g(true)       => [g(domain_error(clpfd_expression, E))]]
             ).

% Again, we compile this to a predicate, parse_reified_clpfd//3. This
% time, it is a DCG that describes the list of auxiliary variables and
% propagators for the given expression, in addition to relating it to
% its reified (Boolean) finite domain variable and its Boolean
% definedness.

make_parse_reified(Clauses) :-
        parse_reified_clauses(Clauses0),
        maplist(goals_goal_dcg, Clauses0, Clauses).

goals_goal_dcg((Head --> Goals), Clause) :-
        list_goal(Goals, Body),
        expand_term((Head --> Body), Clause),write(5).

parse_reified_clauses(Clauses) :-
        parse_reified(E, R, D, Matchers),
        maplist(parse_reified(E, R, D), Matchers, Clauses).

parse_reified(E, R, D, Matcher, Clause) :-
        Matcher = (Condition0 => Goals0),
        phrase((reified_condition(Condition0, E, Head, Ds),
                reified_goals(Goals0, Ds)), Goals, [a(D)]),
        Clause = (parse_reified_clpfd(Head, R, D) --> Goals).

reified_condition(g(Goal), E, E, []) --> [{Goal}, !].
reified_condition(?(E), _, ?(E), []) --> [!].
reified_condition(m(Match), _, Match0, Ds) -->
        [!],
        { copy_term(Match, Match0),
          term_variables(Match0, Vs0),
          term_variables(Match, Vs)
        },
        reified_variables(Vs0, Vs, Ds).

reified_variables([], [], []) --> [].
reified_variables([V0|Vs0], [V|Vs], [D|Ds]) -->
        [parse_reified_clpfd(V0, V, D)],
        reified_variables(Vs0, Vs, Ds).

reified_goals([], _) --> [].
reified_goals([G|Gs], Ds) --> reified_goal(G, Ds), reified_goals(Gs, Ds).

reified_goal(d(D), Ds) -->
        (   { Ds = [X] } -> [{D=X}]
        ;   { Ds = [X,Y] } ->
            { phrase(reified_goal(p(reified_and(X,[],Y,[],D)), _), Gs),
              list_goal(Gs, Goal) },
            [( {X==1, Y==1} -> {D = 1} ; Goal )]
        ;   { domain_error(one_or_two_element_list, Ds) }
        ).
reified_goal(g(Goal), _) --> [{Goal}].
reified_goal(p(Vs, Prop), _) -->
        [{make_propagator(Prop, P)}],
        parse_init_dcg(Vs, P),
        [{trigger_once(P)}],
        [( { propagator_state(P, S), S == dead } -> [] ; [p(P)])].
reified_goal(p(Prop), Ds) -->
        { term_variables(Prop, Vs) },
        reified_goal(p(Vs,Prop), Ds).
reified_goal(skeleton(A,B,D,R,F), Ds) -->
        { Prop =.. [F,X,Y,Z],
          phrase(reified_goals([d(D1),l(p(P)),g(make_propagator(Prop, P)),
                                p([A,B,D2,R], pskeleton(A,B,D2,[X,Y,Z]-P,R,F)),
                                p(reified_and(D1,[],D2,[],D)),a(D2),a(A,B,R)],
                               Ds), Goals),
          list_goal(Goals, Goal) },
        [Goal].
reified_goal(a(V), _)     --> [a(V)].
reified_goal(a(X,V), _)   --> [a(X,V)].
reified_goal(a(X,Y,V), _) --> [a(X,Y,V)].
reified_goal(l(L), _)     --> [[L]].

parse_init_dcg([], _)     --> [].
parse_init_dcg([V|Vs], P) --> [{init_propagator(V, P)}], parse_init_dcg(Vs, P).

%?- set_prolog_flag(answer_write_options, [portray(true)]),
%   clpfd:parse_reified_clauses(Cs), maplist(portray_clause, Cs).

reify(E, B) :- reify(E, B, _).

reify(Expr, B, Ps) :-
        (   acyclic_term(Expr), reifiable(Expr) -> phrase(reify(Expr, B), Ps)
        ;   domain_error(clpfd_reifiable_expression, Expr)
        ).

reifiable(E)      :- var(E), non_monotonic(E).
reifiable(E)      :- integer(E), E in 0..1.
reifiable(?(E))   :- must_be_fd_integer(E).
reifiable(V in _) :- fd_variable(V).
reifiable(Expr)   :-
        Expr =.. [Op,Left,Right],
        (   memberchk(Op, [#>=,#>,#=<,#<,#=,#\=])
        ;   memberchk(Op, [#==>,#<==,#<==>,#/\,#\/,#\]),
            reifiable(Left),
            reifiable(Right)
        ).
reifiable(#\ E) :- reifiable(E).
reifiable(tuples_in(Tuples, Relation)) :-
        must_be(list(list), Tuples),
        maplist(maplist(fd_variable), Tuples),
        must_be(list(list(integer)), Relation).
reifiable(finite_domain(V)) :- fd_variable(V).

reify(E, B) --> { B in 0..1 }, reify_(E, B).

reify_(E, B) --> { var(E), !, E = B }.
reify_(E, B) --> { integer(E), E = B }.
reify_(?(B), B) --> [].
reify_(V in Drep, B) -->
        { drep_to_domain(Drep, Dom) },
        propagator_init_trigger(reified_in(V,Dom,B)),
        a(B).
reify_(tuples_in(Tuples, Relation), B) -->
        { maplist(relation_tuple_b_prop(Relation), Tuples, Bs, Ps),
          maplist(monotonic, Bs, Bs1),
          fold_statement(conjunction, Bs1, And),
          ?(B) #<==> And },
        propagator_init_trigger([B], tuples_not_in(Tuples, Relation, B)),
        kill_reified_tuples(Bs, Ps, Bs),
        list(Ps),
        as([B|Bs]).
reify_(finite_domain(V), B) -->
        propagator_init_trigger(reified_fd(V,B)),
        a(B).
reify_(L #>= R, B) --> arithmetic(L, R, B, reified_geq).
reify_(L #= R, B)  --> arithmetic(L, R, B, reified_eq).
reify_(L #\= R, B) --> arithmetic(L, R, B, reified_neq).
reify_(L #> R, B)  --> reify_(L #>= (R+1), B).
reify_(L #=< R, B) --> reify_(R #>= L, B).
reify_(L #< R, B)  --> reify_(R #>= (L+1), B).
reify_(L #==> R, B)  --> reify_((#\ L) #\/ R, B).
reify_(L #<== R, B)  --> reify_(R #==> L, B).
reify_(L #<==> R, B) --> reify_((L #==> R) #/\ (R #==> L), B).
reify_(L #\ R, B) --> reify_((L #\/ R) #/\ #\ (L #/\ R), B).
reify_(L #/\ R, B)   -->
        (   { conjunctive_neqs_var_drep(L #/\ R, V, D) } -> reify_(V in D, B)
        ;   boolean(L, R, B, reified_and)
        ).
reify_(L #\/ R, B) -->
        (   { disjunctive_eqs_var_drep(L #\/ R, V, D) } -> reify_(V in D, B)
        ;   boolean(L, R, B, reified_or)
        ).
reify_(#\ Q, B) -->
        reify(Q, QR),
        propagator_init_trigger(reified_not(QR,B)),
        a(B).

arithmetic(L, R, B, Functor) -->
        { phrase((parse_reified_clpfd(L, LR, LD),
                  parse_reified_clpfd(R, RR, RD)), Ps),
          Prop =.. [Functor,LD,LR,RD,RR,Ps,B] },
        list(Ps),
        propagator_init_trigger([LD,LR,RD,RR,B], Prop),
        a(B).

boolean(L, R, B, Functor) -->
        { reify(L, LR, Ps1), reify(R, RR, Ps2),
          Prop =.. [Functor,LR,Ps1,RR,Ps2,B] },
        list(Ps1), list(Ps2),
        propagator_init_trigger([LR,RR,B], Prop),
        a(LR, RR, B).

list([])     --> [].
list([L|Ls]) --> [L], list(Ls).

a(X,Y,B) -->
        (   { nonvar(X) } -> a(Y, B)
        ;   { nonvar(Y) } -> a(X, B)
        ;   [a(X,Y,B)]
        ).

a(X, B) -->
        (   { var(X) } -> [a(X, B)]
        ;   a(B)
        ).

a(B) -->
        (   { var(B) } -> [a(B)]
        ;   []
        ).

as([])     --> [].
as([B|Bs]) --> a(B), as(Bs).

kill_reified_tuples([], _, _) --> [].
kill_reified_tuples([B|Bs], Ps, All) -->
        propagator_init_trigger([B], kill_reified_tuples(B, Ps, All)),
        kill_reified_tuples(Bs, Ps, All).

relation_tuple_b_prop(Relation, Tuple, B, p(Prop)) :-
        put_attr(R, clpfd_relation, Relation),
        make_propagator(reified_tuple_in(Tuple, R, B), Prop),
        tuple_freeze_(Tuple, Prop),
        init_propagator(B, Prop).


tuples_in_conjunction(Tuples, Relation, Conj) :-
        maplist(tuple_in_disjunction(Relation), Tuples, Disjs),
        fold_statement(conjunction, Disjs, Conj).

tuple_in_disjunction(Relation, Tuple, Disj) :-
        maplist(tuple_in_conjunction(Tuple), Relation, Conjs),
        fold_statement(disjunction, Conjs, Disj).

tuple_in_conjunction(Tuple, Element, Conj) :-
        maplist(var_eq, Tuple, Element, Eqs),
        fold_statement(conjunction, Eqs, Conj).

fold_statement(Operation, List, Statement) :-
        (   List = [] -> Statement = 1
        ;   List = [First|Rest],
            foldl(Operation, Rest, First, Statement)
        ).

conjunction(E, Conj, Conj #/\ E).

disjunction(E, Disj, Disj #\/ E).

var_eq(V, N, ?(V) #= N).

% Match variables to created skeleton.

skeleton(Vs, Vs-Prop) :-
        maplist(prop_init(Prop), Vs),
        trigger_once(Prop).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   A drep is a user-accessible and visible domain representation. N,
   N..M, and D1 \/ D2 are dreps, if D1 and D2 are dreps.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

is_drep(N)      :- integer(N).
is_drep(N..M)   :- drep_bound(N), drep_bound(M), N \== sup, M \== inf.
is_drep(D1\/D2) :- is_drep(D1), is_drep(D2).
is_drep({AI})   :- is_and_integers(AI).
is_drep(\D)     :- is_drep(D).

is_and_integers(I)     :- integer(I).
is_and_integers((A,B)) :- is_and_integers(A), is_and_integers(B).

drep_bound(I)   :- integer(I).
drep_bound(sup).
drep_bound(inf).

drep_to_intervals(I)        --> { integer(I) }, [n(I)-n(I)].
drep_to_intervals(N..M)     -->
        (   { defaulty_to_bound(N, N1), defaulty_to_bound(M, M1),
              N1 cis_leq M1} -> [N1-M1]
        ;   []
        ).
drep_to_intervals(D1 \/ D2) -->
        drep_to_intervals(D1), drep_to_intervals(D2).
drep_to_intervals(\D0) -->
        { drep_to_domain(D0, D1),
          domain_complement(D1, D),
          domain_to_drep(D, Drep) },
        drep_to_intervals(Drep).
drep_to_intervals({AI}) -->
        and_integers_(AI).

and_integers_(I)     --> { integer(I) }, [n(I)-n(I)].
and_integers_((A,B)) --> and_integers_(A), and_integers_(B).

drep_to_domain(DR, D) :-
        must_be(ground, DR),
        (   is_drep(DR) -> true
        ;   domain_error(clpfd_domain, DR)
        ),
        phrase(drep_to_intervals(DR), Is0),
        merge_intervals(Is0, Is1),
        intervals_to_domain(Is1, D).

merge_intervals(Is0, Is) :-
        keysort(Is0, Is1),
        merge_overlapping(Is1, Is).

merge_overlapping([], []).
merge_overlapping([A-B0|ABs0], [A-B|ABs]) :-
        merge_remaining(ABs0, B0, B, Rest),
        merge_overlapping(Rest, ABs).

merge_remaining([], B, B, []).
merge_remaining([N-M|NMs], B0, B, Rest) :-
        Next cis B0 + n(1),
        (   N cis_gt Next -> B = B0, Rest = [N-M|NMs]
        ;   B1 cis max(B0,M),
            merge_remaining(NMs, B1, B, Rest)
        ).

domain(V, Dom) :-
        (   fd_get(V, Dom0, VPs) ->
            domains_intersection(Dom, Dom0, Dom1),
            %format("intersected\n: ~w\n ~w\n==> ~w\n\n", [Dom,Dom0,Dom1]),
            fd_put(V, Dom1, VPs),
            do_queue,
            reinforce(V)
        ;   domain_contains(Dom, V)
        ).

domains([], _).
domains([V|Vs], D) :- domain(V, D), domains(Vs, D).

props_number(fd_props(Gs,Bs,Os), N) :-
        length(Gs, N1),
        length(Bs, N2),
        length(Os, N3),
        N is N1 + N2 + N3.

fd_get(X, Dom, Ps) :-
        (   get_attr(X, clpfd, Attr) -> Attr = clpfd_attr(_,_,_,Dom,Ps)
        ;   var(X) -> default_domain(Dom), Ps = fd_props([],[],[])
        ).

fd_get(X, Dom, Inf, Sup, Ps) :-
        fd_get(X, Dom, Ps),
        domain_infimum(Dom, Inf),
        domain_supremum(Dom, Sup).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   By default, propagation always terminates. Currently, this is
   ensured by allowing the left and right boundaries, as well as the
   distance between the smallest and largest number occurring in the
   domain representation to be changed at most once after a constraint
   is posted, unless the domain is bounded. Set the experimental
   Prolog flag 'clpfd_propagation' to 'full' to make the solver
   propagate as much as possible. This can make queries
   non-terminating, like: X #> abs(X), or: X #> Y, Y #> X, X #> 0.
   Importantly, it can also make labeling non-terminating, as in:

   ?- B #==> X #> abs(X), indomain(B).
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

fd_put(X, Dom, Ps) :-
        (   current_prolog_flag(clpfd_propagation, full) ->
            put_full(X, Dom, Ps)
        ;   put_terminating(X, Dom, Ps)
        ).

put_terminating(X, Dom, Ps) :-
        Dom \== empty,
        (   Dom = from_to(F, F) -> F = n(X)
        ;   (   get_attr(X, clpfd, Attr) ->
                Attr = clpfd_attr(Left,Right,Spread,OldDom, _OldPs),
                put_attr(X, clpfd, clpfd_attr(Left,Right,Spread,Dom,Ps)),
                (   OldDom == Dom -> true
                ;   (   Left == (.) -> Bounded = yes
                    ;   domain_infimum(Dom, Inf), domain_supremum(Dom, Sup),
                        (   Inf = n(_), Sup = n(_) ->
                            Bounded = yes
                        ;   Bounded = no
                        )
                    ),
                    (   Bounded == yes ->
                        put_attr(X, clpfd, clpfd_attr(.,.,.,Dom,Ps)),
                        trigger_props(Ps, X, OldDom, Dom)
                    ;   % infinite domain; consider border and spread changes
                        domain_infimum(OldDom, OldInf),
                        (   Inf == OldInf -> LeftP = Left
                        ;   LeftP = yes
                        ),
                        domain_supremum(OldDom, OldSup),
                        (   Sup == OldSup -> RightP = Right
                        ;   RightP = yes
                        ),
                        domain_spread(OldDom, OldSpread),
                        domain_spread(Dom, NewSpread),
                        (   NewSpread == OldSpread -> SpreadP = Spread
                        ;   NewSpread cis_lt OldSpread -> SpreadP = no
                        ;   SpreadP = yes
                        ),
                        put_attr(X, clpfd, clpfd_attr(LeftP,RightP,SpreadP,Dom,Ps)),
                        (   RightP == yes, Right = yes -> true
                        ;   LeftP == yes, Left = yes -> true
                        ;   SpreadP == yes, Spread = yes -> true
                        ;   trigger_props(Ps, X, OldDom, Dom)
                        )
                    )
                )
            ;   var(X) ->
                put_attr(X, clpfd, clpfd_attr(no,no,no,Dom, Ps))
            ;   true
            )
        ).

domain_spread(Dom, Spread) :-
        domain_smallest_finite(Dom, S),
        domain_largest_finite(Dom, L),
        Spread cis L - S.

smallest_finite(inf, Y, Y).
smallest_finite(n(N), _, n(N)).

domain_smallest_finite(from_to(F,T), S)   :- smallest_finite(F, T, S).
domain_smallest_finite(split(_, L, _), S) :- domain_smallest_finite(L, S).

largest_finite(sup, Y, Y).
largest_finite(n(N), _, n(N)).

domain_largest_finite(from_to(F,T), L)   :- largest_finite(T, F, L).
domain_largest_finite(split(_, _, R), L) :- domain_largest_finite(R, L).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   With terminating propagation, all relevant constraints get a
   propagation opportunity whenever a new constraint is posted.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

reinforce(X) :-
        (   current_prolog_flag(clpfd_propagation, full) ->
            % full propagation propagates everything in any case
            true
        ;   term_variables(X, Vs),
            maplist(reinforce_, Vs),
            do_queue
        ).

reinforce_(X) :-
        (   fd_var(X), fd_get(X, Dom, Ps) ->
            put_full(X, Dom, Ps)
        ;   true
        ).

put_full(X, Dom, Ps) :-
        Dom \== empty,
        (   Dom = from_to(F, F) -> F = n(X)
        ;   (   get_attr(X, clpfd, Attr) ->
                Attr = clpfd_attr(_,_,_,OldDom, _OldPs),
                put_attr(X, clpfd, clpfd_attr(no,no,no,Dom, Ps)),
                %format("putting dom: ~w\n", [Dom]),
                (   OldDom == Dom -> true
                ;   trigger_props(Ps, X, OldDom, Dom)
                )
            ;   var(X) -> %format('\t~w in ~w .. ~w\n',[X,L,U]),
                put_attr(X, clpfd, clpfd_attr(no,no,no,Dom, Ps))
            ;   true
            )
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   A propagator is a term of the form propagator(C, State), where C
   represents a constraint, and State is a free variable that can be
   used to destructively change the state of the propagator via
   attributes. This can be used to avoid redundant invocation of the
   same propagator, or to disable the propagator.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

make_propagator(C, propagator(C, _)).

propagator_state(propagator(_,S), S).

trigger_props(fd_props(Gs,Bs,Os), X, D0, D) :-
        (   ground(X) ->
            trigger_props_(Gs),
            trigger_props_(Bs)
        ;   Bs \== [] ->
            domain_infimum(D0, I0),
            domain_infimum(D, I),
            (   I == I0 ->
                domain_supremum(D0, S0),
                domain_supremum(D, S),
                (   S == S0 -> true
                ;   trigger_props_(Bs)
                )
            ;   trigger_props_(Bs)
            )
        ;   true
        ),
        trigger_props_(Os).

trigger_props(fd_props(Gs,Bs,Os), X) :-
        trigger_props_(Os),
        trigger_props_(Bs),
        (   ground(X) ->
            trigger_props_(Gs)
        ;   true
        ).

trigger_props(fd_props(Gs,Bs,Os)) :-
        trigger_props_(Gs),
        trigger_props_(Bs),
        trigger_props_(Os).

trigger_props_([]).
trigger_props_([P|Ps]) :- trigger_prop(P), trigger_props_(Ps).

trigger_prop(Propagator) :-
        propagator_state(Propagator, State),
        (   State == dead -> true
        ;   get_attr(State, clpfd_aux, queued) -> true
        ;   b_getval('$clpfd_current_propagator', C), C == State -> true
        ;   % passive
            % format("triggering: ~w\n", [Propagator]),
            put_attr(State, clpfd_aux, queued),
            (   arg(1, Propagator, C), functor(C, F, _), global_constraint(F) ->
                push_queue(Propagator, 2)
            ;   push_queue(Propagator, 1)
            )
        ).

kill(State) :- del_attr(State, clpfd_aux), State = dead.

kill(State, Ps) :-
        kill(State),
        maplist(kill_entailed, Ps).

kill_entailed(p(Prop)) :-
        propagator_state(Prop, State),
        kill(State).
kill_entailed(a(V)) :-
        del_attr(V, clpfd).
kill_entailed(a(X,B)) :-
        (   X == B -> true
        ;   del_attr(B, clpfd)
        ).
kill_entailed(a(X,Y,B)) :-
        (   X == B -> true
        ;   Y == B -> true
        ;   del_attr(B, clpfd)
        ).

no_reactivation(rel_tuple(_,_)).
no_reactivation(pdistinct(_)).
no_reactivation(pgcc(_,_,_)).
no_reactivation(pgcc_single(_,_)).
%no_reactivation(scalar_product(_,_,_,_)).

activate_propagator(propagator(P,State)) :-
        % format("running: ~w\n", [P]),
        del_attr(State, clpfd_aux),
        (   no_reactivation(P) ->
            b_setval('$clpfd_current_propagator', State),
            run_propagator(P, State),
            b_setval('$clpfd_current_propagator', [])
        ;   run_propagator(P, State)
        ).

disable_queue :- b_setval('$clpfd_queue_status', disabled).
enable_queue  :- b_setval('$clpfd_queue_status', enabled).

portray_propagator(propagator(P,_), F) :- functor(P, F, _).

portray_queue(V, []) :- var(V), !.
portray_queue([P|Ps], [F|Fs]) :-
        portray_propagator(P, F),
        portray_queue(Ps, Fs).

do_queue :-
        % b_getval('$clpfd_queue', H-_),
        % portray_queue(H, Port),
        % format("queue: ~w\n", [Port]),
        (   b_getval('$clpfd_queue_status', enabled) ->
            (   fetch_propagator(Propagator) ->
                activate_propagator(Propagator),
                do_queue
            ;   true
            )
        ;   true
        ).

init_propagator(Var, Prop) :-
        (   fd_get(Var, Dom, Ps0) ->
            insert_propagator(Prop, Ps0, Ps),
            fd_put(Var, Dom, Ps)
        ;   true
        ).

constraint_wake(pneq, ground).
constraint_wake(x_neq_y_plus_z, ground).
constraint_wake(absdiff_neq, ground).
constraint_wake(pdifferent, ground).
constraint_wake(pexclude, ground).
constraint_wake(scalar_product_neq, ground).

constraint_wake(x_leq_y_plus_c, bounds).
constraint_wake(scalar_product_eq, bounds).
constraint_wake(scalar_product_leq, bounds).
constraint_wake(pplus, bounds).
constraint_wake(pgeq, bounds).
constraint_wake(pgcc_single, bounds).
constraint_wake(pgcc_check_single, bounds).

global_constraint(pdistinct).
global_constraint(pgcc).
global_constraint(pgcc_single).
global_constraint(pcircuit).
%global_constraint(rel_tuple).
%global_constraint(scalar_product_eq).

insert_propagator(Prop, Ps0, Ps) :-
        Ps0 = fd_props(Gs,Bs,Os),
        arg(1, Prop, Constraint),
        functor(Constraint, F, _),
        (   constraint_wake(F, ground) ->
            Ps = fd_props([Prop|Gs], Bs, Os)
        ;   constraint_wake(F, bounds) ->
            Ps = fd_props(Gs, [Prop|Bs], Os)
        ;   Ps = fd_props(Gs, Bs, [Prop|Os])
        ).

term_expansion(make_parse_clpfd, Clauses)   :- make_parse_clpfd(Clauses).
make_parse_clpfd.
