:- module(units,
          [
              op(700, xfx, '==='),
	      op(700, xfx, '=#='),
	      op(700, xfx, (#=\=)),
	      op(700, xfx, '#<'),
	      op(700, xfx, '#>'),
	      op(700, xfx, '#>='),
	      op(700, xfx, '#=<'),
	      op(700, xfx, (¤=\=)),
	      op(700, xfx, '¤<'),
	      op(700, xfx, '¤>'),
	      op(700, xfx, '¤>='),
	      op(700, xfx, '¤=<'),
	      op(500, yfx, '#+'),
	      op(500, yfx, '#-'),
	      op(200, fy,  '#+'),
	      op(200, fy,  '#-'),
	      op(400, yfx, '#*'),
	      op(400, yfx, '#/'),
	      op(200, xfx, '#**'),
	      ('==='/2),
	      ('=#='/2),
              pr/1,
              pr/2,
              pr/3,
              pr0/1,
              pr0/2,
              equal/2,
              hasUnit/2,
              equalUnits/2,
	      constants/2
          ]).

:- nb_setval(ppunits,[m,kg,s,'A','K',cd,mol]).
:- nb_setval(si,[m,kg,s,'A','K',cd,mol]).
:- nb_setval(cgs,[ft,lb,s,'A','F°',cd,mol]).

domain(X) :- (number(X);rational(X)).

getUnits(PP,X,Y) :-
    (
        atom(PP) ->
          b_getval(PP,A);
        A=PP
    ),
    (
        atom(X) ->
          b_getval(X,XX);
        XX=X
    ),
    getUnits0(A,XX,Y).

getUnits0([],_,[]).
getUnits0(AA,X,Y) :-
    getUnits0(AA,X,X,Y).

getUnits0([A|AA],X,[],[A|YY]) :- !,
    getUnits0(AA,X,YY).

getUnits0([A|AA],X,[XX|_],[XX|YY]) :-
    equalUnits(1:A,1:XX),!,
    getUnits0(AA,X,YY).

getUnits0(AA,X,[_|XXX],Y) :-
    getUnits0(AA,X,XXX,Y).

%use_module(library(clpr)).
%use_module(library(clpq)).

:- dynamic(dyn0/1).

dyn :- dyn0(X),!,X==true.

(X === Y) :- asserta(dyn0(false)), '=x='(X,Y,false).
(X =#= Y) :- asserta(dyn0(true)) , '=x='(X,Y,true ).

:- op(700, xfx, ===).
:- op(700, xfx, =#=).
:- op(700, xfx, (#=\=)).
:- op(700, xfx, '#<').
:- op(700, xfx, '#>').
:- op(700, xfx, '#>=').
:- op(700, xfx, '#=<').
:- op(700, xfx, (¤=\=)).
:- op(700, xfx, '¤<').
:- op(700, xfx, '¤>').
:- op(700, xfx, '¤>=').
:- op(700, xfx, '¤=<').
:- op(700, xfx, '#=').

#=(X,Y) :- {X = Y}.


'=x='(X1:X2,Y1:Y2,F) :-
    tr(1:X2,U),!,
    U=A:[B1,B2,B3,B4,B5,B6,B7],

    tr(1:Y2,V),!,
    V=C:[D1,D2,D3,D4,D5,D6,D7],

    (
	var(X1) ->
	(
	    var(Y1) ->
	       (F==true -> {X1 = Y1*C/A} ; true);
	    X1 is Y1*C/A
	);
	Y1 is X1*A/C
    ),
    B1 #= D1,
    B2 #= D2,
    B3 #= D3,
    B4 #= D4,
    B5 #= D5,
    B6 #= D6,
    B7 #= D7.

'=x='(X1:X2,Y,F) :-
    tr(1:X2,U),!,
    U=A:[B1,B2,B3,B4,B5,B6,B7],

    tr(Y,V),!,
    V=C:[D1,D2,D3,D4,D5,D6,D7],

    (
	var(X1) ->
	(
	    var(C) ->
	    (F==true -> {X1 = C/A} ; true);
	    X1 is C/A
	);
	C is X1/A
    ),
    
    B1 #= D1,
    B2 #= D2,
    B3 #= D3,
    B4 #= D4,
    B5 #= D5,
    B6 #= D6,
    B7 #= D7.

'=x='(X,Y1:Y2,F) :- '=x='(Y1:Y2,X,F).
    

'=x='(X,Y,F) :-
    tr(X,U),!,
    U=A:[B1,B2,B3,B4,B5,B6,B7],

    tr(Y,V),!,
    V=C:[D1,D2,D3,D4,D5,D6,D7],

    (F==true -> {A = C} ; A = C),
    B1 #= D1,
    B2 #= D2,
    B3 #= D3,
    B4 #= D4,
    B5 #= D5,
    B6 #= D6,
    B7 #= D7.
    
    
e_mul(X,N,Z,Exp):-
    tr(X,(U:[A,B,C,D,E,F,G])),
    AA #= A*N,
    BB #= B*N,
    CC #= C*N,
    DD #= D*N,
    EE #= E*N,
    FF #= F*N,
    GG #= G*N,
    ((domain(U),rational(N)) -> UU is U**N; call(Exp,X,N,UU)),
    Z=(UU:[AA,BB,CC,DD,EE,FF,GG]).

e_add(X,Y,Z,Mul):-
    tr(X,(U1:[A1,B1,C1,D1,E1,F1,G1])),
    tr(Y,(U2:[A2,B2,C2,D2,E2,F2,G2])),
    AA #= A1+A2,
    BB #= B1+B2,
    CC #= C1+C2,
    DD #= D1+D2,
    EE #= E1+E2,
    FF #= F1+F2,
    GG #= G1+G2,
    ((domain(U1),domain(U2)) -> UU is U1*U2; call(Mul,U1,U2,UU)),
    Z=(UU:[AA,BB,CC,DD,EE,FF,GG]).

 e_sub(X,Y,Z,Div):-
    tr(X,(U1:[A1,B1,C1,D1,E1,F1,G1])),
    tr(Y,(U2:[A2,B2,C2,D2,E2,F2,G2])),
    AA #= A1-A2,
    BB #= B1-B2,
    CC #= C1-C2,
    DD #= D1-D2,
    EE #= E1-E2,
    FF #= F1-F2,
    GG #= G1-G2,
    ((domain(U1),domain(U2)) -> UU is U1/U2; call(Div,U1,U2,UU)),
    Z=(UU:[AA,BB,CC,DD,EE,FF,GG]).

e_eq([A1,B1,C1,D1,E1,F1,G1],[A2,B2,C2,D2,E2,F2,G2]) :-
    A1 #= A2,
    B1 #= B2,
    C1 #= C2,
    D1 #= D2,
    E1 #= E2,
    F1 #= F2,
    G1 #= G2.


prefix('h',100).
prefix('k',1000).
prefix('M',1000000).
prefix('G',1000000000).
prefix('T',1000000000000).
prefix('P',1000000000000000).
prefix('E',1000000000000000000).
prefix('Z',1000000000000000000000).
prefix('Y',1000000000000000000000000).

prefix('d',1 rdiv 10).
prefix('c',1 rdiv 100).
prefix('m',1 rdiv 1000).
prefix('μ',1 rdiv 1000000).
prefix('n',1 rdiv 1000000000).
prefix('p',1 rdiv 1000000000000).
prefix('f',1 rdiv 1000000000000000).
prefix('a',1 rdiv 1000000000000000000).
prefix('z',1 rdiv 1000000000000000000000).
prefix('y',1 rdiv 1000000000000000000000000).

g(B,A,E) :- number(A) -> call(B is rationalize(E)) ; true.

exp1(_,_,_).
exp2(X,Y,Z) :- {Z=X^Y}.

sum2(X,Y,Z) :- {Z = X*Y}.

sub2(X,Y,Z) :- {Z = X/Y}.

tr(X,Y) :-
    var(X),!,X=Y,
    Y=(_:[_,_,_,_,_,_,_]).

tr(X,Y) :-
    number(X),!,
    Y=(X:[A,B,C,D,E,F,G]),
    A #= 0,
    B #= 0,
    C #= 0,
    D #= 0,
    E #= 0,
    F #= 0,
    G #= 0.

tr(X,X) :- X=(_:[_,_,_,_,_,_,_]),!.

tr(A:m  ,(A : [1,0,0,0,0,0,0])) :- !.

tr(A:s  ,(A   : [0,0,1,0,0,0,0])) :- !.
tr(A:min,(B   : [0,0,1,0,0,0,0])) :- !, B is A*60.
tr(A:h  ,(B   : [0,0,1,0,0,0,0])) :- !, B is A*3600.

tr(A:g  ,(B   : [0,1,0,0,0,0,0])) :- !, B is A*(1/1000).

tr(A:'A', A   : [0,0,0,1,0,0,0]) :- !.

tr(A:'K', A   : [0,0,0,0,1,0,0]) :- !.
tr(A:'cd', A  : [0,0,0,0,0,1,0]) :- !.
tr(A:'mol', A : [0,0,0,0,0,0,1]) :- !.

tr(A:in,Z)   :- !,g(B,A, 0.0254*A),tr(B:m,Z).
tr(A:ft,Z)   :- !,g(B,A, 0.3048*A),tr(B:m,Z).
tr(A:yd,Z)   :- !,g(B,A, 0.9144*A),tr(B:m,Z).
tr(A:mile,Z) :- !,g(B,A, 1609.344*A),tr(B:m,Z).

tr(A:lb,Z) :- !,g(B,A, 0.453592*A),tr(B:kg,Z).
tr(A:gr,Z) :- !,g(B,A, 64.79891*A),tr(B:mg,Z).
tr(A:oz,Z) :- !,g(B,A, 28.34952*A),tr(B:g,Z).

tr(A:'C°', Z) :- !,g(B,A, A + 273.15), tr(B:'K',Z).
tr(A:'F°', Z)  :- !,g(B,A, (A + 459.67) * 5.0 / 9.0), tr(B:'K',Z).

tr(A:'N',Z)   :- !,tr(A:(kg*m*s**(-2)),Z).
tr(A:'kp',Z)  :- !,g(B,A, A*9.80665), tr(B:'N',Z).
tr(A:'dyn',Z) :- !,g(B,A, A*1e-5)   , tr(B:'N',Z).

tr(A:'Pa',Z)  :- !,tr(A:('N'*m**(-2)),Z).
tr(A:atm,Z)   :- !,g(B,A,A*101325)  , tr(B:'Pa',Z).
tr(A:at,Z)    :- !,g(B,A,A*98065.5) , tr(B:'Pa',Z).
tr(A:bar,Z)   :- !,g(B,A,A*100000)  , tr(B:'Pa',Z).
tr(A:torr,Z)  :- !,g(B,A,A*133.322) , tr(B:'Pa',Z).
tr(A:psi,Z)   :- !,g(B,A,A*6894)    , tr(B:'Pa',Z).

tr(A:'J',Z)   :- !,tr(A:'N'*m,Z).
tr(A:'Ws',Z)  :- !,tr(A:'N'*m,Z).
tr(A:'Wh',Z)  :- !,g(B,A,3600*A), tr(B:'N'*m,Z).
tr(A:'eV',Z)  :- !,g(B,A,1.60217662e-19*A), tr(B:'N'*m,Z).

tr(A:'W',Z)   :- !,tr(A:'N'*m*s**(-1),Z).

tr(A:'V',Z)   :- !,tr(A:'J'*'A'**(-1)*s**(-1),Z).
tr(A:'C',Z)   :- !,tr(A:'A'*s,Z).
tr(A:'F',Z)   :- !,tr(A:'C'*'V'**(-1),Z).
tr(A:'Ω',Z)   :- !,tr(A:'V'*'A'**(-1),Z).
tr(A:'S',Z)   :- !,tr(A:'Ω'**(-1),Z).
tr(A:'Hz',Z)  :- !,tr(A:s**(-1),Z).
tr(A:'H',Z)   :- !,tr(A:'Ω'*s,Z).
tr(A:'Wb',Z)  :- !,tr(A:'J'*'A'**(-1),Z).
tr(A:'T',Z)   :- !,tr(A:'Wb'*m**(-2),Z).


tr(A:S,Z) :-
    atom(S),!,
    atom_chars(S,[C|L]),
    atom_chars(R,L),
    prefix(C,V),
    (domain(A)-> AA is A*V ; (dyn -> {AA = A*V} ; true)),
    tr(AA:R,Z).

    
tr(X ** (M/N),Y) :-    
    !,
    integer(M),
    integer(N),
    O is M rdiv N,
    e_mul(X,O,Y,exp1).

tr(X #** (M/N),Y) :-    
    !,
    integer(M),
    integer(N),
    O is M rdiv N,
    e_mul(X,O,Y,exp2).

tr(X ** N,Y) :-
    !,
    tr(X ** (N/1),Y).

tr(X #** N,Y) :-
    !,
    tr(X #** (N/1),Y).

tr(A : (X ** (N / M)),Y) :- !,
    integer(N),
    integer(M),
    O is N rdiv M,
    tr(A:(X ** O),Y).

tr(A : (X #** (N / M)),Y) :- !,
    integer(N),
    integer(M),
    O is N rdiv M,
    tr(A:(X #** O),Y).
    
tr(A : (X ** R),Y) :- !,
    domain(R),
    tr((1:X),U),
    tr(U**R,V:VV),
    ((domain(A), domain(V)) -> W is A*V ; true),
    Y = W:VV.

tr(A : (X #** R),Y) :- !,
    domain(R),
    tr((1:X),U),
    tr(U**R,V:VV),
    ((domain(A), domain(V)) -> W is A*V ; exp2(A,V,W)),
    Y = W:VV.

tr(X * Y,Z) :-
    !,
    e_add(X, Y, Z, exp1).

tr(X #* Y,Z) :-
    !,
    e_add(X, Y, Z, sum2).


tr(A : (X * Y),Z) :-    
    !,
    tr(1:X,U),
    tr(1:Y,V),
    tr(U*V,B:W),
    ((domain(B),domain(A)) -> C is A*B; true),
    Z=(C:W).

tr(A : (X #* Y),Z) :-    
    !,
    tr(1:X,U),
    tr(1:Y,V),
    tr(U*V,B:W),
    ((domain(B),domain(A)) -> C is A*B; sum2(A,B,C)),
    Z=(C:W).

tr(A : (X / Y),Z) :-
    !,
    tr(1:X,U),
    tr(1:Y,V),
    tr(U/V,B:W),
    ((domain(B),domain(A))->C is A*B;true),
    Z=(C:W).

tr(A : (X #/ Y),Z) :-
    !,
    tr(1:X,U),
    tr(1:Y,V),
    tr(U/V,B:W),
    ((domain(B),domain(A)) -> C is A*B ; sum2(A,B,C)),
    Z=(C:W).

tr(X / Y,Z) :-
    !,
    e_sub(X,Y,Z,exp1).

tr(X #/ Y,Z) :-
    !,
    e_sub(X,Y,Z,sub2).

tr(X + Y,Z) :-
    !,
    tr(X,(A:XX)),
    tr(Y,(B:YY)),
    write(f(XX,YY)),nl,
    ((domain(A),domain(B)) -> C is A + B ; true),
    write(1),
    e_eq(XX,YY),
    write(2),
    Z = (C:XX).

tr(X #+ Y,Z) :-
    !,
    tr(X,(A:XX)),
    tr(Y,(B:YY)),
    write(a),nl,
    {C = A + B},
    write(b),nl,
    e_eq(XX,YY),
    write(c),nl,
    Z = (C:XX).

tr(X - Y,Z) :-
    !,
    tr(X,(A:XX)),
    tr(Y,(B:YY)),
    C is A - B,
    e_eq(XX,YY),
    Z = (C:XX).

tr(X #- Y,Z) :-
    !,
    tr(X,(A:XX)),
    tr(Y,(B:YY)),
    {C = A - B},
    e_eq(XX,YY),
    Z = (C:XX).

tr(-X,Z) :-
    !,
    tr(X,(C:XX)),
    CC is -C,
    Z = (CC:XX).

tr(exp(X),Z) :-
    tr(X,A:V),
    equalUnits(A:V,1),
    (var(A) -> {B = exp(A)} ; B is exp(A)),
    Z = B:V.

tr(sin(X),Z) :-
    tr(X,A:V),
    equalUnits(A:V,1),
    (var(A) -> {B = sin(A)} ; B is sin(A)),
    Z = B:V.

tr(cos(X),Z) :-
    tr(X,A:V),
    equalUnits(A:V,1),
    (var(A) -> {B = cos(A)} ; B is cos(A)),
    Z = B:V.

tr(tan(X),Z) :-
    tr(X,A:V),
    equalUnits(A:V,1),
    (var(A) -> {B = tan(A)} ; B is tan(A)),
    Z = B:V.

tr(abs(X),Z) :-
    tr(X,A:V),
    (var(A) -> {B = abs(A)} ; B is abs(A)),
    Z = B:V.

tr(min(X,Y),Z) :-
    equalUnits(X,Y),
    tr(X,A:Q),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> C is max(A,B) ; {C = max(A,B)}),
    Z = C:Q.

tr(log(X),Z) :-
    tr(X,A:V),
    equalUnits(A:V,1),
    B is log(A),
    Z = B:V.

tr(+X,Z) :-
    !,
    tr(X,Z).

tr(#+X,Z) :-
    !,
    tr(X,Z).

tr(-X,Z) :-
    !,
    tr(X,A:V),
    B is -A,
    Z = B:V.

tr(#-X,Z) :-
    !,
    tr(X,A:V),
    (var(A) -> {B = -A} ; B is -A),
    Z = B:V.

tr(max(X,Y),Z) :-
    equalUnits(X,Y),
    tr(X,A:Q),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> C is max(A,B) ; {C = max(A,B)}),
    Z = C:Q.

#<(X,Y) :-
    equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A < B ; {A < B}).
    
#>(X,Y) :-
    equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A > B ; {A > B}).
    
#=<(X,Y) :-
    equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A =< B ; {A =< B}).

#>=(X,Y) :-
	equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A =< B ; {A =< B}).


¤<(X,Y) :-
    equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A < B ; true).
    
¤>(X,Y) :-
    equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A > B  ; true).
    
¤=<(X,Y) :-
    equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A =< B ; true).

¤>=(X,Y) :-
	equalUnits(X,Y),
    tr(X,A:_),
    tr(Y,B:_),
    ((domain(A),domain(B)) -> A =< B ; true).

equalUnits(X,Y) :-
    tr(X,(_:[A1,B1,C1,D1,E1,F1,G1])),!,
    tr(Y,(_:[A2,B2,C2,D2,E2,F2,G2])),!,
    A1 #= A2,
    B1 #= B2,
    C1 #= C2,
    D1 #= D2,
    E1 #= E2,
    F1 #= F2,
    G1 #= G2.

hasUnit(X,U) :- equalUnits(X,(1:U)).
    
equal(X,Y) :-
    equalUnits(X,Y),
    tr(X,(A1:_)),!,
    tr(X,(A2:_)),!,
    A1 = A2.

pr0(X) :-
    pr0(X,[]).

writew(X rdiv Y) :- !,write('('),write(X/Y),write(')').
writew(X       ) :- write(X).

pr0(X,U) :-
    getUnits(ppunits,U,[BB,DD,CC,EE,FF,GG,HH]),
    tr(X,(A:[B,D,C,E,F,G,H])),!,
    tr(1:BB,BBB:_),
    tr(1:DD,DDD:_),
    tr(1:CC,CCC:_),
    tr(1:EE,EEE:_),
    tr(1:FF,FFF:_),
    tr(1:GG,GGG:_),
    tr(1:HH,HHH:_),

    K is (BBB^B)*(DDD^D)*(CCC^C)*(EEE^E)*(FFF^F)*(GGG^G)*(HHH^H),
    AA is A / K,
    
    write(AA),
    write(:),
    (
	var(B) -> write(BB),write('**?');
	B==0   -> (true,UB=1);
	(write(BB),write('**'),writew(B),write(' '))
    ),
    (
	C == 0 ->
	  UC = UB;
	(UB == 1 -> (write(' '),UC=1); true)
    ),
    (
	var(C) -> write(CC),write('**?');
	C==0   -> true;
	(write(CC),write('**'),writew(C),write(' '))
    ),
    (
	D == 0 ->
	  UD = UC;
	(UC == 1 -> (write(' '),UD=1); true)
    ),
    (
	var(D) -> write(DD),write('**?');
	D==0   -> true;
	(write(DD),write('**'),writew(D))
    ),
    (
	E == 0 ->
	  UE = UD;
	(UD == 1 -> (write(' '),UE=1); true)
    ),
    (
	var(E) -> write(EE),write('**?');
	E==0   -> true;
	(write(EE),write('**'),writew(E))
    ),
    (
	F == 0 ->
	  UF = UE;
	(UE == 1 -> (write(' '),UF=1); true)
    ),
    (
	var(F) -> write(FF),write('**?');
	F==0   -> true;
	(write(FF),write('**'),writew(F))
    ),
    (
	G == 0 ->
	  UG = UF;
	(UF == 1 -> (write(' '),UG=1); true)
    ),
    (
	var(G) -> write(GG),write('**?');
	G==0   -> true;
	write(GG),(write('cd**'),writew(G))
    ),
    (
	H == 0 ->
	  true;
	(UG == 1 -> (write(' ')); true)
    ),
    (
	var(H) -> write(HH),write('**?');
	H==0   -> true;
	(write(HH),write('**'),writew(H))
    ).

:- nb_setval(ppcmplx,[m,s,kg,'A','K',cd,mol,'N','Pa','J','W','V','C','F','Ω','H','Wb','T']).


gen(S,L) :-
    gen1(L,S).

gen0(S,L) :-
    gen1(L,S).

gen1([S|XX],SS) :-
    S=SS;
    gen1(XX,SS).

t(X) :- catch(label(X),_,fail).

sum(L,Z) :- sum(L,0,Z).
sum([],X,X).
sum([N|L],Y,X) :-
    YY is Y + abs(N),
    sum(L,YY,X).

smalest([N|NN],L,V) :-
    N=[A,U,F],
    sum(U,K),
    smalest(NN,K,[A,U],L,F,V).

smalest([],_,N,N,V,V).

smalest([N|NN],K,M,L,VV,V) :-
    N=[A,U,W],
    sum(U,KK),
    (
        KK < K ->
          smalest(NN,KK,[A,U],L,W,V);
        smalest(NN,K,M,L,VV,V)
    ).

try1(X,S,N,V,C,_) :-
    tr(X,_:Y),!,
    findall([[SS1],[NN1],V],
            (
                gen(SS1,C),
                tr(1:SS1**NN1,_:Y),
                t([NN1]),
                tr(1:SS1**NN1,V:Y)
            ),
            L),
    smalest(L,[[S],[N]],V).

try2(X,S1,N1,S2,N2,V,C,_) :-
    tr(X,_:Y),!,
    findall([[SS1,SS2],[NN1,NN2],V],
            (
                gen(SS1,C),
                gen(SS2,C),
                SS2 \= SS1,
                once(tr(1:SS1**NN1*SS2**NN2,_:Y)),
                t([NN1,NN2]),
                tr(1:SS1**NN1*SS2**NN2,V:Y)
            ),L),
    smalest(L,[[S1,S2],[N1,N2]],V).

try10(X,S,N,V,_,U) :-
    tr(X,_:Y),!,
    gen0(S,U),
    tr(1:S**N,V:Y),
    t(N).

try20(X,S1,N1,S2,N2,V,_,U) :-
    tr(X,_:Y),!,
    gen0(S1,U),
    gen0(S2,U),
    S2 \= S1,
    tr(1:S1**N1*S2**N2,V:Y),
    t([N1,N2]).

try3(X,S1,N1,S2,N2,S3,N3,V,C,U) :-
    tr(X,_:Y),!,
    findall([[SS1,SS2,SS3],[NN1,NN2,NN3],V],
            (
                gen(SS1,C),
                gen0(SS2,U),
                SS1 \= SS2,
                gen0(SS3,U),
                SS3 \= SS1,
                SS3 \= SS2,
                tr(1:SS1**NN1*SS2**NN2*SS3**NN3,V:Y),
                once(t([NN1,NN2,NN3])),
                tr(1:SS1**NN1*SS2**NN2*SS3**NN3,V:Y)
            ),L),
    smalest(L,[[S1,S2,S3],[N1,N2,N3]],V).

try7(X,m,N1,kg,N2,s,N3,'A',N4,'K',N5,cd,N6,mol,N7,V,_,_) :-    
    tr(X,V:[N1,N2,N3,N4,N5,N6,N7]),!.
    

prx(X,L,V,C,U) :-
    try1(X,S1,N1,V,C,U)
    -> L=[[S1,N1]];
    try2(X,S1,N1,S2,N2,V,C,U)
    -> L=[[S1,N1],[S2,N2]];
    try3(X,S1,N1,S2,N2,S3,N3,V,C,U)
    -> L=[[S1,N1],[S2,N2],[S3,N3]];
    try7(X,S1,N1,S2,N2,S3,N3,S4,N4,S5,N5,S6,N6,S7,N7,V,C,U)
    ->L=[[S1,N1],[S2,N2],[S3,N3],[S4,N4],[S5,N5],[S6,N6],[S7,N7]].

pry([]).
pry([[S,N]]) :-
    N == 1 -> write(S);
    (write(S),write('**'),writew(N)).

pry([[S,N]|X]) :-
    (
        N == 1 -> (write(S),write(' '));
        (write(S),write('**'),writew(N),write(' '))
    ),
    pry(X).

prz([],[]).
prz([[_,0]|L],LL) :- !,prz(L,LL).
prz([[S,X]|L],[[S,X]|LL]) :- !, prz(L,LL).

pr(X) :-
    pr(X,[],[]).
pr(X,U) :-
    pr(X,U,[]).

pr(A:X,U,C) :-
    getUnits(ppunits,U,UU),
    getUnits(ppcmplx,UU,UC),
    getUnits(UC,C,CC),
    prx(A:X,L,V,CC,UU), !,
    AA is A / V,
    write(AA),write(':'),
    prz(L,LL),
    pry(LL).

constantss('Na' , (6.02214076e23       : mol**(-1))).
constantss('kb' , (1.380649e-23        : 'J'/'K')).
constantss(e    , 1.602176634e-19     : 'C').
constantss('G'  , 6.6743015e-11       : m**3*kg**(-1)*s**(-2)).
constantss(h    , 6.62607015e-34      : 'J'*s).
constantss(c    , 299792458           : km/s).
constantss(mu0  , 1.2566370621219e-6  : 'N'*'A'**(-2)).
constantss(me   , 9.109383701528e-31  : kg).
constantss(mp   , 1.6726219236951e-27 : kg).
constantss(pi   , 3.1415).

constants(X,Z) :-
    constantss(X,U:V),
    W is rationalize(U),
    Z === W:V.

constants(e0   , X) :-
    constants(mu0,M),
    constants(c,C),
    X === 1/M/C**2.

constants(alpha, X) :-
    constants(e,E),
    constants(h,H),
    constants(c,C),
    constants(e0,E0),
    X === E**2/2/E0/H/C.
    
constants('Kj' , X) :-
    constants(e,E),
    constants(h,H),
    X === 2*E/H.

constants('R', X) :-
    constants('Na',N),
    constants('kb',K),
    X === N*K.

constants('Ryd',X) :-
    constants(alpha,A),
    constants(me,ME),
    constants(c,C),
    constants(h,H),
    X === A**2*ME*C/2/H.

constants('Rk',X) :-
    constants(h,H),
    constants(e,E),
    X  === H/E**2.
    
limit(Min,Max,X,Y) :- Y === max(Min:_,min(Max:_,X)).
