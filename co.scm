;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(use-modules (logic guile-log))
(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log guile-prolog coroutine))
(use-modules (logic guile-log guile-prolog attribute))


(compile-prolog-string
"
  f(1).
")
